FROM node:16 as base

COPY . /app/
WORKDIR /app/
RUN yarn install
RUN yarn build
from nginx 
COPY nginx/nginx.conf /etc/nginx/nginx.conf
COPY --from=base /app/build/ /app/site/
COPY startup.sh /app/startup.sh
CMD ["bash /app/startup.sh"]
