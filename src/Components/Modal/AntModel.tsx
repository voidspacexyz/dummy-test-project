import React, { useState } from 'react';
import { Button, Modal, Tooltip, Typography } from 'antd';
import TagButton from '../Button/TagButton';
import TimeAgo from 'react-timeago'
import AntImage from '../Image/AntImage';
import { RiTimerFlashLine } from 'react-icons/ri';
const { Text } = Typography;


const AntModel = ({isButton, buttonSize, buttonType, buttonText, modelData, modelTitle, placement, modelOpen,startTime,endTime,updateModel}:{isButton?:any, buttonSize?:any, buttonType?:any, buttonText?:any, modelData?:any, modelTitle:any, placement?:any, modelOpen?:boolean,startTime?:any,endTime?:any,updateModel?:any}) => {
  const [open, setOpen] = useState(modelOpen);
  const [ellipsis, setEllipsis] = useState(true);

  return (
    <>
        {/* {isButton && 
            <Button type={buttonType} onClick={() => setOpen(true)} size={buttonSize} className={`float-${placement}`}>
                {buttonText}
            </Button>
        } */}
        <Modal
            title={
                <div className='sm:flex justify-between'>
                    <div className="hidden sm:block">
                        <Text
                            ellipsis={ellipsis ? { tooltip: {modelTitle} } : false}
                        >
                        {modelTitle}
                        </Text>
                    </div>
                    <div className="flex gap-2 sm:mt-0 mt-2">
                            <TagButton 
                                color="#108ee9" 
                                tagName={
                                    <div className="flex">
                                        <span className="self-center">
                                            <RiTimerFlashLine size={15} className="mr-2" />
                                        </span>
                                        <span> {startTime + " - " + endTime}</span>
                                    </div>
                                }
                            />
                            <TagButton 
                                color="#108ee9" 
                                tagName={<TimeAgo date={modelData?.createdDate?.split("T").reverse().pop()} />}
                            />
                    </div>
                </div>
            }
            centered
            open={open}
            onOk={() => { setOpen(false); updateModel()}}
            onCancel={() => { setOpen(false); updateModel()}}
            closable={false}
            width={1000}
        >
            <div>
                <div className='sm:flex justify-between bg-[#eef6ff] rounded-md pl-2'>
                    <div className='rounded w-fit p-1 sm:p-0 self-center text-sm mb-1'>
                        {modelTitle && 
                            <div className="sm:hidden justify-between">
                                {modelTitle}
                            </div>
                        }
                        <div className="flex justify-between w-fit p-1 pl-0">
                            <span className="font-semibold">
                                By:
                            </span>
                            <span className="pl-1">
                                {modelData?.createdBy?.first_name ? modelData?.createdBy?.first_name + " " + modelData?.createdBy?.last_name : "N/A"}
                            </span>   
                        </div>
                    </div>
                    {modelData?.tags && 
                        <div className="flex  sm:w-[20rem] rounded-md overflow-x-scroll h-12  sm:justify-end">
                            {modelData?.tags.map((tag, i) => tag.name !== "" && tag.count !== 0 && ( <TagButton tagName={tag.name} color="#55acee"/>))}
                        </div>
                    }
                </div>
                
                <div className="overflow-hidden hover:overflow-y-scroll text-justify pr-[10px] h-[20rem] overflow-y-scroll mt-2 border-[#d9d9d9] border-solid border rounded-md sh">
                        <p className='p-2'>{modelData?.text}</p>
                </div>
                <div className='mt-4'>
                {modelData?.annoImage && 
                    <AntImage width={200} src={modelData?.annoImage}/>
                }
                </div>
                
                <div>
                
                </div>
                
            </div>
        </Modal>
    </>
  );
};

export default AntModel;