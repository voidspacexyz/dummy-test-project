import React, { useEffect, useRef, useState } from "react";
import { MdClose } from "react-icons/md";
import { useNavigate } from "react-router-dom";
import Button from "../Button/Button";
import IconButton from "../Button/IconButton";

const MultiGroupModal = ({
  onClose,
  groupData,
  onSelectMulti,
}: {
  onClose: () => void;
  groupData: any[];
  onSelectMulti;
}) => {
  const [loading, setLoading] = useState(false);
  const [searchWord, setSearchWord] = useState<string>("");
  const [curVal, setCurVal] = useState<any>([]);
  const [multi, setMulti] = useState<string>("");
  const [display, setDisplay] = useState(true);
  const [checked, setChecked] = useState(true);
  const [group, setGroup] = useState<any[]>(groupData);

  const searchInput = useRef<HTMLInputElement>(null);

  const handleFocus = () => {
    if (searchInput && searchInput.current) {
      searchInput.current.focus();
    }
  };

  const handleSearch = ({
    target,
  }: React.ChangeEvent<HTMLInputElement>): void => {
    const searchWord: string = target.value;
    setDisplay(false);
    setSearchWord(searchWord);
  };

  const handleMultiSearch = () => {
    onSelectMulti(multi);
    onClose();
  };

  const SearchResults = (e) => {
    return (
      <>
        <label className="block text-gray-500 font-bold text-center mb-1 md:mb-0 pr-4 pt-5">
          Search Results
        </label>
        <div className="flex flex-col p-5 w-full gap-2 h-auto max-h-96 rounded overflow-hidden shadow-lg mb-2 overflow-y-auto">
          {group
            ?.filter((grp, i) => grp.name && grp.name.includes(searchWord))
            .map((data, i) => (
              <div
                className="text-white w-full bg-blue-600 hover:bg-blue-700 active:bg-blue-800 font-bold rounded uppercase shadow hover:shadow-lg py-3 px-10 text-base place-items-center justify-center outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 disabled:opacity-50 cursor-pointer items-center text-center"
                onClick={() => {
                  setCurVal((prev) => [...prev, data.name]);
                  setMulti(
                    multi === "" ? multi + data.id : multi + "," + data.id
                  );
                }}
              >
                {data.name}
              </div>
            ))}
        </div>
      </>
    );
  };

  const handleDel = (data) => {
    const index = group.findIndex((x) => x.id === data);
    group.splice(index, 1);
  };

  const AllGroups = () => {
    return (
      <>
        <label className="block text-gray-500 font-bold text-center mb-1 md:mb-0 pr-4 pt-5">
          Your Groups
        </label>
        <div className="flex flex-col p-5 w-full gap-2 h-auto max-h-96 rounded overflow-hidden shadow-lg mb-2 overflow-y-auto">
          {group?.map((grp, i) => (
            <>
              <div
                onClick={() => {
                  setChecked(!checked);

                  setCurVal((prev) => [...prev, grp.name]);
                  setMulti(
                    multi === "" ? multi + grp.id : multi + "," + grp.id
                  );
                  handleDel(grp.id);
                }}
                className="text-white w-full bg-blue-600 hover:bg-blue-700 active:bg-blue-800 font-bold rounded uppercase shadow hover:shadow-lg py-3 px-10 text-base place-items-center justify-center outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 disabled:opacity-50 cursor-pointer items-center text-center"
                key={i}
              >
                {grp.name}
              </div>
            </>
          ))}
        </div>
      </>
    );
  };

  const DisplayRes = () => {
    return (
      <>
        <label className="block text-gray-500 font-bold text-center mb-1 md:mb-0 pr-4">
          Selected Collections
        </label>

        <div className="grid grid-cols-2 p-1 w-full h-auto items-center place-items-center justify-center rounded overflow-hidden shadow-lg mb-2 overflow-y-auto">
          {curVal.map((addedGrp, i) => (
            <div className="text-white w-auto h-auto p-2 bg-blue-600 hover:bg-blue-700 active:bg-blue-800 font-bold rounded uppercase shadow hover:shadow-lg text-base place-items-center justify-center outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 disabled:opacity-50 cursor-pointer items-center text-center">
              {addedGrp}
            </div>
          ))}
        </div>
      </>
    );
  };

  return (
    <div className="lg:p-1 sm:p-1 rounded-md h-auto overflow-y-auto">
      {curVal?.length !== 0 && <DisplayRes />}

      <div className="flex flex-col md:flex md:items-center m-1 mb-6 mt-5">
        <label className="flex text-gray-500 font-bold text-center mb-1 md:mb-0 pr-4">
          Search Collections
          <p className="text-xs pt-1 px-2">(Case Sensitive)</p>
        </label>
        <div className="flex">
          <input
            className="bg-gray-200 border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
            type="text"
            placeholder={`Search users here   ...`}
            value={searchWord}
            ref={searchInput}
            autoFocus={true}
            onChange={(e) => {
              handleSearch(e);
            }}
          />
          <IconButton
            size="xs"
            type="tertiary"
            onClick={() => {}}
            className="-ml-9 mt-1 cursor-pointer"
          >
            <MdClose
              size={25}
              onClick={() => {
                setSearchWord("");
                setDisplay(true);
                handleFocus();
              }}
            />
          </IconButton>
        </div>
      </div>

      {display ? (
        group.length !== 0 ? (
          <AllGroups />
        ) : (
          <label className="block text-gray-500 font-bold text-center mb-1 md:mb-0 pr-4">
            There are no collections to choose
          </label>
        )
      ) : (
        searchWord !== "" && <SearchResults />
      )}

      <div className="p-5 flex flex-row place-items-center justify-center border-t-2 mt-5">
        <Button
          size="sm"
          type="secondary"
          onClick={() => {
            handleMultiSearch();
            setLoading(true);
          }}
        >
          Confirm
        </Button>
        <Button size="sm" type="danger" onClick={onClose}>
          Cancel
        </Button>
      </div>
    </div>
  );
};

export default MultiGroupModal;
