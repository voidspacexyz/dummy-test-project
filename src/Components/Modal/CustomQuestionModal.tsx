import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import { MdArrowBack, MdClose } from "react-icons/md";
import { useNavigate } from "react-router-dom";
import { useRecoilValue } from "recoil";
import { prodState, tokenState } from "../../Modules/AuthState/AuthState";
import Button from "../Button/Button";
import IconButton from "../Button/IconButton";

const CustomQuestionModal = ({
  onClose,
  groupData,
  onSelectCQ,
}: {
  onClose: () => void;
  groupData: any[];
  onSelectCQ;
}) => {
  const [loading, setLoading] = useState(false);
  const [searchWord, setSearchWord] = useState<string>("");
  const [curVal, setCurVal] = useState<any>([]);
  const [multi, setMulti] = useState<string>("");
  const [disSelectedCustQues, setDisSelectedCustQues] = useState(false);
  const [customQuestion, setCustomQuestion] = useState<any>([]);
  const [customId, setCustomId] = useState("");

  const searchInput = useRef<HTMLInputElement>(null);
  const navigate = useNavigate();
  const auth = localStorage.getItem("token");
  const baseURL = localStorage.getItem("prod_url");

  useEffect(() => {
    getGroupById();
  }, [multi]);

  const getGroupById = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/group/${multi}/`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setCustomQuestion(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const handleFocus = () => {
    if (searchInput && searchInput.current) {
      searchInput.current.focus();
    }
  };

  const handleSearch = ({
    target,
  }: React.ChangeEvent<HTMLInputElement>): void => {
    const searchWord: string = target.value;
    setSearchWord(searchWord);
  };

  const handleCQ = () => {
    onSelectCQ(multi);
    onClose();
  };

  const SearchResults = (e) => {
    return (
      <>
        <label className="block text-gray-500 font-bold text-center mb-1 md:mb-0 pr-4">
          Search Results
        </label>
        <div className="grid sm:space-y-0 sm:grid sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-4 p-5 gap-5 w-full h-auto items-center place-items-center justify-center rounded overflow-hidden shadow-lg mb-2">
          {groupData
            ?.filter((grp, i) => grp.name && grp.name.includes(searchWord))
            .map((data, i) => (
              <div
                className="p-5 inline-block text-sm  h-full rounded-sm font-semibold border-b-2 border-gray-400 shadow cursor-pointer focus:outline-none focus:ring focus-ring:bg-blue-300"
                onClick={() => {
                  setCurVal((prev) => [...prev, data.name]);
                  setMulti(
                    multi === "" ? multi + data.id : multi + "," + data.id
                  );
                  setDisSelectedCustQues(true);
                }}
              >
                {data.name}
              </div>
            ))}
        </div>
      </>
    );
  };

  const AllGroups = () => {
    return (
      <>
        <label className="block text-gray-500 font-bold text-center mb-1 md:mb-0 pr-4 pt-5">
          Your Groups
        </label>
        <div className="grid sm:space-y-0 sm:grid sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-4 p-5 gap-5 w-full h-auto items-center place-items-center justify-center rounded overflow-hidden shadow-lg mb-2">
          {groupData?.map((grp, i) => (
            <div
              onClick={() => {
                setCurVal((prev) => [...prev, grp.name]);
                setMulti(multi === "" ? multi + grp.id : multi + "," + grp.id);
                setDisSelectedCustQues(true);
              }}
              className="p-5 inline-block text-sm  h-full rounded-sm font-semibold border-b-2 border-gray-400 shadow cursor-pointer focus:outline-none focus:ring focus-ring:bg-blue-300"
              key={i}
            >
              {grp.name}
            </div>
          ))}
        </div>
      </>
    );
  };

  const DisplayRes = () => {
    return (
      <>
        <label className="block text-gray-500 font-bold text-center mb-1 md:mb-0 pr-4 pt-5">
          Selected Groups
        </label>
        <div className="grid sm:space-y-0 sm:grid sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-4 p-5 gap-5 w-full h-auto items-center place-items-center justify-center rounded overflow-hidden shadow-lg mb-2">
          {curVal.map((addedGrp, i) => (
            <div className="flex p-5 text-sm  h-full rounded-sm font-semibold border-b-2 border-gray-400 shadow cursor-pointer focus:outline-none focus:ring focus-ring:bg-blue-300">
              {addedGrp}
              {/* <IconButton
                size="xs"
                type="tertiary"
                color="red"
                onClick={() => {}}
              >
                <MdClose size={15} />
              </IconButton> */}
            </div>
          ))}
        </div>
        {customQuestion.length !== 0 && (
          <>
            <label className="block text-gray-500 font-bold text-center mb-1 md:mb-0 pr-4 pt-5">
              Select Custom Question below
            </label>
            <div className="grid sm:space-y-0 sm:grid sm:grid-cols-2 md:grid-cols-2 lg:grid-cols-4 p-5 gap-5 w-full h-auto items-center place-items-center justify-center rounded overflow-hidden shadow-lg mb-2">
              {customQuestion.extra_group_questions.map((cust, key) => (
                <div
                  className="lowercase inline-block rounded-md font-medium border border-gray-400 shadow cursor-pointer p-2 px-2 bg-blue-500 text-white focus:outline-none focus:ring focus-ring:bg-blue-300"
                  key={key}
                  onClick={() => setCustomId(cust.id)}
                >
                  {cust.question}
                </div>
              ))}
            </div>
          </>
        )}
      </>
    );
  };

  return (
    <div className="lg:p-5 sm:p-1 rounded-t">
      <div
        className="col-span-1"
        onClick={() => {
          setDisSelectedCustQues(false);
          setCurVal("");
          setMulti("");
        }}
      >
        <MdArrowBack size={30} />
      </div>
      {!disSelectedCustQues && (
        <>
          <div className="md:flex md:items-center m-5 mb-6">
            <div className="md:w-1/3">
              <label className="block text-gray-500 font-bold text-center mb-1 md:mb-0 pr-4">
                Search Groups<p className="text-xs">(Case Sensitive)</p>
              </label>
            </div>
            <div className="md:w-2/3 flex">
              <input
                className="bg-gray-200 border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
                type="text"
                placeholder={`Search users here   ...`}
                value={searchWord}
                ref={searchInput}
                autoFocus={true}
                onChange={(e) => {
                  handleSearch(e);
                }}
              />
              <IconButton
                size="xs"
                type="tertiary"
                onClick={() => {}}
                className="-ml-9 mt-1 cursor-pointer"
              >
                <MdClose
                  size={25}
                  onClick={() => {
                    setSearchWord("");
                    handleFocus();
                  }}
                />
              </IconButton>
            </div>
          </div>
          {searchWord !== "" && <SearchResults />}
          {groupData.length !== 0 ? (
            <AllGroups />
          ) : (
            <label
              className="block text-gray-500 font-bold text-center mb-1 md:mb-0 pr-4"
              onClick={() => navigate("/group")}
            >
              Create a Collection
            </label>
          )}
        </>
      )}
      {curVal?.length !== 0 && <DisplayRes />}
      <div className="p-5 flex flex-row place-items-center justify-center border-t-2">
        <Button
          size="sm"
          type="secondary"
          onClick={() => {
            handleCQ();
            setLoading(true);
          }}
        >
          Confirm
        </Button>
        <Button size="sm" type="danger" onClick={onClose}>
          Cancel
        </Button>
      </div>
    </div>
  );
};

export default CustomQuestionModal;
