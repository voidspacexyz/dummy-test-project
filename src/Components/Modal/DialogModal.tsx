import React, { Fragment } from "react";
import { MdClose } from "react-icons/md";
import IconButton from "../Button/IconButton";
import { Dialog, Transition } from "@headlessui/react";

const DialogModal = ({
  open,
  onClose,
  children,
  title,
  footer,
  className,
}: {
  open: boolean;
  onClose: () => void;
  children: React.ReactNode;
  title: string;
  footer?: boolean;
  className?: string;
}) => {
  return (
    <>
      <Transition appear show={open} as={Fragment}>
        <Dialog as="div" className="relative z-10 w-full" onClose={onClose}>
          <Transition.Child
            as={Fragment}
            enter="ease-out duration-300"
            enterFrom="opacity-0"
            enterTo="opacity-100"
            leave="ease-in duration-200"
            leaveFrom="opacity-100"
            leaveTo="opacity-0"
          >
            <div className="fixed inset-0 bg-black bg-opacity-25" />
          </Transition.Child>

          <div className="fixed inset-0 overflow-y-auto top-0 left-0 right-0 bottom-0 flex items-center justify-center">
            <div className="flex min-h-fit items-center justify-center p-4 text-center">
              <Transition.Child
                as={Fragment}
                enter="ease-out duration-300"
                enterFrom="opacity-0 scale-95"
                enterTo="opacity-100 scale-100"
                leave="ease-in duration-200"
                leaveFrom="opacity-100 scale-100"
                leaveTo="opacity-0 scale-95"
              >
                <Dialog.Panel className="w-full max-w-max max-h-screen transform overflow-auto rounded-2xl bg-white p-5 py-6 text-left align-middle shadow-xl transition-all">
                  <div className="place-items-end justify-end items-end">
                    <IconButton
                      type="tertiary"
                      size="xs"
                      className="float-right font-extrabold"
                      onClick={onClose}
                      color="black"
                    >
                      <MdClose size={20} />
                    </IconButton>
                  </div>
                  <div className="flex items-center justify-center pb-5">
                    <Dialog.Title
                      as="h3"
                      className="text-2xl font-medium leading-6 text-gray-900"
                    >
                      {title}
                    </Dialog.Title>
                  </div>

                  <div className="mt-2 p-5">{children}</div>
                </Dialog.Panel>
              </Transition.Child>
            </div>
          </div>
        </Dialog>
      </Transition>
    </>
  );
};

export default DialogModal;
