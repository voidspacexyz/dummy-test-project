import axios from "axios";
import React, { useState } from "react";
import { MdOutlineFileUpload } from "react-icons/md";
import { useNavigate } from "react-router-dom";
import { useRecoilValue } from "recoil";
import { prodState, tokenState } from "../../Modules/AuthState/AuthState";
import Button from "../Button/Button";
import Loader from "../Loader/Loader";

const ImportModal = ({
  onClose,
  type,
  groupId,
  mediaId,
  updateGroups,
}: {
  onClose: () => void;
  type: string;
  groupId?: string;
  mediaId?: string;
  updateGroups?: () => void;
}) => {
  const [error, setError] = useState<any>([]);
  const [upload, setUpload] = useState<any>([]);
  const [uploaded, setUploaded] = useState(false);
  const [loading, setLoading] = useState(false);

  const auth = localStorage.getItem("token");
  const baseURL = localStorage.getItem("prod_url");

  const handleImport = () => {
    const form = new FormData();
    form.append("upload", upload);
    type === "group" && form.append("ie_metadata", '{"type":"group"}');
    type === "archive" &&
      form.append(
        "ie_metadata",
        `{"type":"archive","import_into_group":${groupId}}`
      );

    const options = {
      method: "POST",
      url: `${baseURL}api/v1/import/`,
      headers: {
        Authorization: `Token ${auth}`,
        "Content-Type": "multipart/form-data",
      },
      data: form,
    };

    axios
      .request(options)
      .then((response) => {
        console.log(response.data);
        updateGroups && updateGroups();
        onClose();
        setLoading(false);
      })
      .catch((error) => {
        console.error(error);
        setError(error.response.data);
      });
  };

  const ErrorFeedback = () => {
    return (
      <>
        {error.length !== 0 &&
          error.map((x) => (
            <div className="flex items-center justify-center p-5 border-b border-solid border-slate-200 rounded-t">
              <h5 className="text-xl font-medium text-red-600">{x.detail}</h5>
            </div>
          ))}
      </>
    );
  };

  const handleUpload = (e) => {
    const file = e.target.files;
    setUpload(file[0]);
  };

  const Upload = () => {
    return (
      <div className="flex items-center justify-center w-full mb-5">
        <div className="flex flex-col w-full h-full mx-auto border-4 border-dashed hover:bg-gray-100 hover:border-gray-300">
          {!uploaded ? (
            <div className="flex flex-col items-center justify-center pt-1">
              <MdOutlineFileUpload size={30} />
              <input
                type="file"
                placeholder="Select a media"
                className="form-control block w-full px-5 py-5 text-xl font-normal text-gray-700 m-0 focus:text-gray-700 fous:bg-white focus:bg-none focus:b-none focus:outline-none"
                onChange={(e) => {
                  handleUpload(e);
                  setUploaded(true);
                }}
                required
              />
            </div>
          ) : (
            upload && (
              <div className="flex flex-col items-center justify-center p-2 max-w-lg pl-5">
                <div>{upload.name}</div>
              </div>
            )
          )}
        </div>
      </div>
    );
  };

  if (loading) {
    return <Loader />;
  }

  return (
    <div className="p-1 pt-2">
      {error && error !== "" && <ErrorFeedback />}
      <Upload />

      <div className="p-5 flex flex-row place-items-center justify-center border-t-2">
        <Button
          size="sm"
          type="secondary"
          onClick={() => {
            handleImport();
            setLoading(true);
          }}
        >
          Confirm
        </Button>
        <Button size="sm" type="danger" onClick={onClose}>
          Cancel
        </Button>
      </div>
    </div>
  );
};

export default ImportModal;
