import React from "react";
import Button from "../Button/Button";

const DeleteModal = ({
  onClick,
  onClose,
  disabled,
}: {
  onClick?: () => void;
  onClose: () => void;
  disabled?;
}) => {
  return (
    <div className="p-5 flex flex-row place-items-center justify-center">
      <Button size="sm" type="secondary" onClick={onClick} disabled={disabled}>
        Confirm
      </Button>
      <Button size="sm" type="danger" onClick={onClose}>
        Cancel
      </Button>
    </div>
  );
};

export default DeleteModal;
