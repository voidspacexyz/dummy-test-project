import axios from "axios";
import React, { useEffect, useState } from "react";
import { MdOutlineDelete } from "react-icons/md";
import { useNavigate } from "react-router-dom";
import { useRecoilValue } from "recoil";
import { prodState, tokenState } from "../../Modules/AuthState/AuthState";
import Button from "../Button/Button";
import IconButton from "../Button/IconButton";
import Toggle from "../Button/Toggle";

const CopyModal = ({
  onClick,
  onClose,
  mediaId,
  groupId,
  groups,
  extra,
}: {
  onClick?: () => void;
  onClose: () => void;
  mediaId: string;
  groupId?: string;
  groups: any;
  extra?: any;
}) => {
  const [curMedia, setCurMedia] = useState("");
  const auth = localStorage.getItem("token");
  const [searchMedia, setSearchMedia] = useState<any>([]);
  const [toGroup, setToGroup] = useState("");
  const [addedGroup, setAddedGroup] = useState(null);
  const [disGrp, setDisGrp] = useState(true);
  const [error, setError] = useState<any>([]);
  const [copyTags, setCopyTags] = useState(false);
  const [copyAnnotations, setCopyAnnotations] = useState(false);
  const [extraRes, setExtraRes] = useState<any>([]);

  const navigate = useNavigate();
  const baseURL = localStorage.getItem("prod_url");

  useEffect(() => {
    extra.length !== 0 &&
      extra?.map((ext) => {
        return setExtraRes((prev) => [
          ...prev,
          `{"question_id": "${ext.question_id}", "response": "${ext.response}"}`,
        ]);
      });
  }, []);

  useEffect(() => {
    curMedia !== "" && searchBy();
  }, [curMedia]);

  const searchBy = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/archive/copy/${mediaId}`,
      params: { search: curMedia },
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setSearchMedia(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleCopy = () => {
    const copyAnno = copyAnnotations ? "True" : "False";
    const copyTag = copyTags ? "True" : "False";

    const formData = new FormData();
    groupId && formData.append("from_group", groupId);
    formData.append("to_group", toGroup);
    formData.append("copy_annotations", copyAnno);
    formData.append("copy_tags", copyTag);
    extra.length !== 0 &&
      formData.append("extra_group_response", `{"answers":[${extraRes}]}`);

    const options = {
      method: "PUT",
      url: `${baseURL}api/v1/archive/copy/${mediaId}/`,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Token ${auth}`,
      },
      data: formData,
    };

    axios
      .request(options)
      .then((response) => {
        console.log(response);
        onClose();
        alert("Successfully Copied");
      })
      .catch((error) => {
        console.log(error);
        setError((prev) => [...prev, error.response.data]);
      });
  };

  const handleSearch = ({
    target,
  }: React.ChangeEvent<HTMLInputElement>): void => {
    const searchWord: string = target.value;
    setCurMedia(searchWord);
  };

  const Display = () => {
    return (
      <>
        <div className="p-5 flex place-items-center justify-center items-center">
          <Button
            size="sm"
            type="tertiary"
            className={
              "w-fit justify-center flex gap-5 px-5 py-3 shadow-lg rounded leading-normal text-emerald-600 bg-white hover:bg-emerald-600 hover:text-white"
            }
          >
            {addedGroup}
          </Button>
          <IconButton
            size="xs"
            className="p-1 bg-red-100 text-red-500 active:bg-red-600 rounded shadow hover:shadow-lg outline-none focus:outline-none w-auto"
            type="danger"
            onClick={() => {
              setDisGrp(true);
              setAddedGroup(null);
              setToGroup("");
            }}
          >
            <MdOutlineDelete size={15} />
          </IconButton>
        </div>
        <div className="p-5 flex place-items-center justify-center items-center">
          <Toggle
            onClick={() => setCopyTags(!copyTags)}
            title="Copy Tags"
            checked={copyTags}
          />
          <Toggle
            onClick={() => setCopyAnnotations(!copyAnnotations)}
            title="Copy Annotations"
            checked={copyAnnotations}
          />
        </div>
      </>
    );
  };

  console.log(groups);

  const UserGroups = () => {
    return (
      <>
        <div className="grid lg:grid-cols-4 gap-3 p-1 h-auto overflow-hidden">
          {groups?.map((grp, i) => (
            <div
              className="group grid grid-cols-6 rounded-lg shadow-xl bg-white max-w-sm text-justify "
              key={i}
            >
              <div className="col-span-5 p-3 w-max-content">
                <h5 className="text-gray-900 text-xl font-medium mb-2">
                  {grp.name}
                </h5>
                <p className="text-gray-700 text-base mb-4 line-clamp-3 hover:line-clamp-none">
                  {grp.description}
                </p>
                <div className="center place-items-center justify-center">
                  <Button
                    size="sm"
                    type="secondary"
                    onClick={() => {
                      setAddedGroup(grp.name);
                      setToGroup(grp.id);
                      setDisGrp(false);
                    }}
                  >
                    select
                  </Button>
                </div>
              </div>
            </div>
          ))}
        </div>
      </>
    );
  };

  const ErrorFeedback = () => {
    return (
      <>
        {error.length !== 0 &&
          error.map((x) => (
            <div className="flex items-center justify-center p-5 border-b border-solid border-slate-200 rounded-t">
              <h5 className="text-xl font-medium text-red-600">{x.detail}</h5>
            </div>
          ))}
      </>
    );
  };
  return (
    <div className="p-1 pt-2">
      {addedGroup && <Display />}
      {disGrp && <UserGroups />}
      {error && error !== "" && <ErrorFeedback />}
      <div className="p-5 flex flex-row place-items-center justify-center border-t-2">
        <Button size="sm" type="secondary" onClick={handleCopy}>
          Confirm
        </Button>
        <Button size="sm" type="danger" onClick={onClose}>
          Cancel
        </Button>
      </div>
    </div>
  );
};

export default CopyModal;
