import React, { useState, useEffect } from "react";
import { Card, Col, Row, Space, Typography, Button } from "antd";
import { useNavigate, useParams } from "react-router-dom";
import IconButton from "../Button/IconButton";
import { MdLock, MdPermIdentity } from "react-icons/md";

const { Paragraph, Text } = Typography;

const CardLayout = ({
  cardData,
  collection,
  media,
  dropdown,
  id,
  noDataText,
}: {
  cardData?: any;
  collection?: any;
  media?: any;
  dropdown?: any;
  id?: any;
  noDataText?: any;
}) => {
  const [iconColor, setIconColor] = useState("");
  const navigate = useNavigate();

  useEffect(() => {
    collection && collection.is_public
      ? setIconColor("bg-emerald-500")
      : setIconColor("bg-blue-500");
  }, []);

  return (
    <>
      {media && (
        <Card style={{ borderColor: "rgb(179, 222, 236)" }}>
          <div className="flex justify-between mb-2 items-center">
            <div className="font-semibold">
              <Text ellipsis={false}>{media.name}</Text>
            </div>
            <div>{dropdown}</div>
          </div>
          <hr />
          <div className="text-justify pr-[10px] mt-2 font-normal">
            <Paragraph ellipsis={false}>{media.description}</Paragraph>
          </div>
          <div className="justify-end float-right">
            <Button
              type="text"
              className="border-blue-500 border-2 rounded-full"
              onClick={() => navigate(`/collection/${id}/media/${media.uuid}`)}
            >
              {" "}
              View{" "}
            </Button>
          </div>
        </Card>
      )}
      {collection && (
        <Card style={{ borderColor: "rgb(179, 222, 236)" }}>
          <div className="flex justify-between mb-2 place-content-center items-center">
            <div className="font-semibold">
              <Text ellipsis={false}>{collection.name}</Text>
            </div>
            <div
              className={
                "text-white text-center inline-flex items-center justify-center w-8 h-8 shadow-lg rounded-full " +
                iconColor
              }
            >
              {collection.is_public ? (
                <IconButton
                  size="xs"
                  // className="p-1 text-white rounded shadow hover:shadow-lg outline-none focus:outline-none w-auto"
                  type="tertiary"
                  onClick={() => {}}
                >
                  <MdPermIdentity size={25} />
                </IconButton>
              ) : (
                <IconButton
                  size="xs"
                  // className="p-1 text-white rounded shadow hover:shadow-lg outline-none focus:outline-none w-auto"
                  type="tertiary"
                  onClick={() => {}}
                >
                  <MdLock size={15} />
                </IconButton>
              )}
            </div>
          </div>
          <hr />
          <div className="h-[7rem] overflow-y-scroll  text-justify pr-[10px] mt-2 font-normal">
            <Paragraph ellipsis={false}>{collection.description}</Paragraph>
          </div>
          <div className="justify-end float-right mt-4">
            <Button
              type="text"
              className="border-blue-500 border-2 rounded-full"
              onClick={() => navigate(`/collection/${collection.id}`)}
            >
              {" "}
              Open{" "}
            </Button>
          </div>
        </Card>
      )}
      {noDataText && (
        <Card style={{ borderColor: "rgb(179, 222, 236)" }}>{noDataText}</Card>
      )}
      {cardData && <Card style={{ borderColor: "rgb(179, 222, 236)" }}></Card>}
    </>
  );
};

export default CardLayout;
