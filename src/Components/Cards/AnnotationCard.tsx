import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { MdEdit, MdOutlineDelete, MdOutlineShare } from "react-icons/md";
import Dropdown from "../Dropdown/Dropdown";
import Button from "../Button/Button";
import { BiExport } from "react-icons/bi";
import { RiTimerFlashLine } from "react-icons/ri";
import { useRecoilState, useRecoilValue } from "recoil";
import {
  cardTimeState,
  editAnnoIDState,
} from "../../Modules/AuthState/AuthState";
import { Badge, Card, Space } from "antd";
import TimeAgo from "react-timeago";
import parse from "html-react-parser";
import TagButton from "../Button/TagButton";
import AntModel from "../Modal/AntModel";
import Pragraphs from "../Paragraph/Pragraphs";
import { CgArrowsExpandRight } from "react-icons/cg";
import { FaUser } from "react-icons/fa";

const AnnotationCard = ({
  annoText,
  createdDate,
  annotationId,
  tags,
  startTime,
  endTime,
  createdBy,
  updatedDate,
  annoImage,
  shareModal,
  deleteModal,
  exportAnno,
  mediaTitle,
}: {
  annoText?: string;
  createdDate?: string;
  annotationId?: string;
  tags?: any;
  startTime?: string;
  endTime?: string;
  createdBy?: any;
  updatedDate?: string;
  annoImage?: string;
  mediaTitle?: string;
  shareModal?;
  deleteModal?;
  exportAnno?;
}) => {
  const auth = localStorage.getItem("token");
  const [cardTime, setCardTime] = useRecoilState<string>(cardTimeState);
  const [editAnno, setEditAnno] = useRecoilState<string>(editAnnoIDState);

  const handleDeleteAnno = () => deleteModal(annotationId);

  const handleExportAnno = () => exportAnno(annotationId);

  const [modelOpen, setModelOpen] = useState(false);
  useEffect(() => {}, [modelOpen]);

  const closeModel = () => {
    setModelOpen(false);
  };
  const CardHeader = ({ startTime, endTime }) => {
    return (
      <>
        <div
          className="flex text-white text-sm font-bold cursor-pointer"
          onClick={() => {
            console.log(startTime);
            setCardTime(startTime);
          }}
        >
          <span className="self-center">
            <RiTimerFlashLine size={15} className="mr-2" />
          </span>
          <span> {startTime + " - " + endTime}</span>
        </div>
      </>
    );
  };

  const CardProfileHeader = ({ user }) => {
    return (
      <>
        <div className="flex text-white text-sm font-bold cursor-pointer">
          <TagButton
            color="rgb(22, 119, 255)"
            tagName={
              <div className="flex">
                <span className="self-center">
                  <FaUser size={10} className="mr-2" />
                </span>
                <span> {user}</span>
              </div>
            }
          />
        </div>
      </>
    );
  };

  const CardDropDown = () => {
    return (
      <>
        <div className="-ml-16 items-end justify-end">
          <Dropdown>
            <div className="pt-2 px-1 py-0 flex flex-col gap-2 overflow-auto">
              <Button
                className="w-fit h-full rounded-full px-1 flex gap-2"
                type="tertiary"
                size="xs"
                color="emerald"
                onClick={() => {
                  setEditAnno(annotationId);
                }}
              >
                <MdEdit size={15} /> Edit
              </Button>
              <Button
                className="w-fit h-full rounded-full px-1 flex gap-2"
                type="tertiary"
                size="xs"
                color="emerald"
                onClick={shareModal}
              >
                <MdOutlineShare size={15} /> Share
              </Button>
              <Button
                className="w-fit h-full rounded-full px-1 flex gap-2"
                type="tertiary"
                size="xs"
                color="red"
                onClick={handleDeleteAnno}
              >
                <MdOutlineDelete size={15} /> Delete
              </Button>
              <Button
                size="xs"
                type="tertiary"
                className="w-fit h-full rounded-full px-1 flex gap-2"
                onClick={handleExportAnno}
                color="emerald"
              >
                <BiExport size={15} /> Export
              </Button>
            </div>
          </Dropdown>
        </div>
      </>
    );
  };

  return (
    <>
      <Space
        direction="vertical"
        size="middle"
        style={{ width: "100%" }}
        className="border-[1.5px] border-[#b3deec] rounded-lg"
      >
        <Badge.Ribbon
          placement="start"
          text={<CardHeader startTime={startTime} endTime={endTime} />}
        >
          <Card
            title={
              <div className="flex sm:flex-col lg:flex-row ml-[8rem]">
                <CardProfileHeader user={createdBy.username} />
                <TagButton
                  color="rgb(22, 119, 255)"
                  tagName={
                    <TimeAgo date={createdDate?.split("T").reverse().pop()} />
                  }
                />
              </div>
            }
            size="small"
            extra={
              auth && (
                <div className="flex gap-2">
                  <CardDropDown />
                  <div
                    className="self-center cursor-pointer"
                    onClick={() => setModelOpen(true)}
                  >
                    <CgArrowsExpandRight />
                  </div>
                </div>
              )
            }
          >
            <div>
              <div className="max-h-[7rem] overflow-hidden hover:overflow-y-scroll text-justify pr-[10px]">
                <Pragraphs isExpand={false} rows={4} text={parse(annoText)} />
              </div>
              <div className="flex w-full">
                <div className="flex overflow-hidden hover:overflow-x-scroll h-12 ">
                  {tags.map(
                    (tag, i) =>
                      tag.name !== "" &&
                      tag.count !== 0 && (
                        <TagButton
                          tagName={tag.name}
                          color="#55acee"
                          isTooltip={true}
                        />
                      )
                  )}
                </div>
              </div>
            </div>
            {modelOpen && (
              <AntModel
                modelTitle={mediaTitle}
                isButton={true}
                modelOpen={modelOpen}
                updateModel={closeModel}
                buttonSize="small"
                buttonType="primary"
                placement={"right"}
                buttonText="View"
                startTime={startTime}
                endTime={endTime}
                modelData={{
                  text: parse(annoText),
                  tags: tags,
                  createdDate: createdDate,
                  createdBy: createdBy,
                  annoImage: annoImage,
                }}
              />
            )}
            {/* <hr className="border border-t-[1px] border-solid border-slate-200 my-2"/>
            <div className="flex justify-between">
              <div className="cursor-pointer">
                <TagButton 
                  color="#108ee9" 
                  tagName={<TimeAgo date={createdDate?.split("T").reverse().pop()} />}
                />
              </div>
            </div> */}
          </Card>
        </Badge.Ribbon>
      </Space>
    </>
  );
};

export default AnnotationCard;

AnnotationCard.defaultProps = {
  annoText: "Annotation",
  createdDate: "",
  annotionId: "",
  tags: [],
  startTime: "",
  endTime: "",
  createdBy: "",
  updatedDate: "",
  annoImage: "",
};

AnnotationCard.propTypes = {
  annoText: PropTypes.string,
  createdDate: PropTypes.string,
  annotionId: PropTypes.number,
  tags: PropTypes.array,
  startTime: PropTypes.string,
  endTime: PropTypes.string,
  updatedDate: PropTypes.string,
  annoImage: PropTypes.any,
};
