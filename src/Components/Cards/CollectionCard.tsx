import React, { useEffect, useState } from "react";
import PropTypes from "prop-types";
import IconButton from "../Button/IconButton";
import { MdPermIdentity, MdLock } from "react-icons/md";

const CollectionCard = ({
  title,
  description,
  createdDate,
  pubCollection,
  collectionId,
}) => {
  const [iconColor, setIconColor] = useState("");

  useEffect(() => {
    pubCollection
      ? setIconColor("bg-emerald-500")
      : setIconColor("bg-blue-500");
  }, []);
  return (
    <>
      <div className="relative flex flex-col min-w-0 break-words bg-white rounded mb-10 xl:mb-0 shadow-xl shadow-gray-500">
        <div className="flex-auto p-4">
          <div className="flex flex-wrap">
            <div className="relative w-full pr-4 max-w-full flex-grow flex-1">
              <span className="font-semibold text-xl text-blueGray-700 h-auto min-h-24 max-h-full line-clamp-5 hover:line-clamp-none">
                {title}
              </span>
              <p className="text-blueGray-400 font-bold text-xs h-auto min-h-24 max-h-full line-clamp-5 hover:line-clamp-none">
                {description}
              </p>
            </div>
            <div className="relative w-auto pl-4 flex-initial">
              <div
                className={
                  "text-white p-3 text-center inline-flex items-center justify-center w-12 h-12 shadow-lg rounded-full " +
                  iconColor
                }
              >
                {pubCollection ? (
                  <IconButton
                    size="xs"
                    className="p-1 text-white rounded shadow hover:shadow-lg outline-none focus:outline-none w-auto"
                    type="tertiary"
                    onClick={() => {}}
                  >
                    <MdPermIdentity size={25} />
                  </IconButton>
                ) : (
                  <IconButton
                    size="xs"
                    className="p-1 text-white rounded shadow hover:shadow-lg outline-none focus:outline-none w-auto"
                    type="tertiary"
                    onClick={() => {}}
                  >
                    <MdLock size={25} />
                  </IconButton>
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="p-4 text-sm text-blueGray-400 mt-auto">
          <span className="mr-2">{createdDate.split("T").reverse().pop()}</span>
          <div className="items-end justify-end text-end mt-auto">
            <span>
              <a
                href={`/collection/${collectionId}`}
                className="px-2 text-sm font-bold text-black hover:bg-opacity-30 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75"
              >
                Open
              </a>
            </span>
          </div>
        </div>
      </div>
    </>
  );
};

export default CollectionCard;

CollectionCard.defaultProps = {
  title: "Collection",
  description: "",
  createdDate: "",
  pubCollection: false,
  collectionId: "",
};

CollectionCard.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  createdDate: PropTypes.string,
  pubCollection: PropTypes.bool,
  collectionId: PropTypes.number,
};
