import React from "react";
import { FloatButton } from "antd";

const Float = ({
  icon,
  onClick,
  tooltip,
  href,
}: {
  icon?: React.ReactNode;
  onClick?: (event) => void;
  tooltip?: string;
  href?: string;
}) => {
  return (
    <div>
      <FloatButton
        shape="circle"
        className="absolute left-2 top-2"
        icon={icon}
        onClick={onClick}
        tooltip={<div>{tooltip}</div>}
        href={href}
      />
    </div>
  );
};

export default Float;
