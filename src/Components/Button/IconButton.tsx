import buttonText from "@material-tailwind/react/theme/components/button/buttonText";
import React from "react";
import { ButtonSize } from "./Button";

export const IconType = {
  primary:
    "bg-blue-200 hover:bg-blue-200 active:bg-blue-400 text-blue-600 font-bold rounded shadow hover:shadow-lg ",
  secondary:
    "bg-emerald-100 hover:bg-emerald-300 active:bg-emerald-400 text-emerald-600 font-bold rounded shadow hover:shadow-lg ",
  danger:
    "bg-red-100 hover:bg-red-200 active:bg-red-400 text-red-600 font-bold rounded shadow hover:shadow-lg ",
  tertiary: "font-bold rounded",
};

const IconButton = ({
  size,
  onClick,
  children,
  type,
  className,
  color,
  disabled,
  buttonType,
}: {
  size: string;
  onClick?: (e: any) => void;
  children: React.ReactNode;
  type: string;
  className?: string;
  color?: string;
  disabled?: boolean;
  buttonType?: any;
}) => {
  return (
    <button
      type={buttonType ? buttonType : "button"}
      className={`${className} w-fit h-fit text-${color}-600 active:text-${color}-700 ${IconType[type]} ${ButtonSize[size]} outline-none focus:outline-none ease-linear transition-all duration-150`}
      onClick={onClick}
      disabled={disabled}
    >
      {children}
    </button>
  );
};

export default IconButton;
