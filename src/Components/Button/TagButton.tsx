import React from 'react'
import { Space, Tag, Tooltip } from 'antd'
import { EditFilled } from '@ant-design/icons';

const TagButton = ({tagName,isEdit,color,isAdd, isTooltip}: { tagName?:any, isEdit?:boolean, color?:any, isAdd?: boolean, isTooltip?: boolean }) => {
    return (
        // <span className="text-xs self-center font-semibold inline-block py-1 px-2 rounded-full text-blue-500 bg-blue-200 uppercase last:mr-0 mr-1">
        //     {tagName.name}
        // </span>
        <>
        { isAdd ? (
            <>
                {/* <Space size={[0, 8]} wrap>
                    <Tag icon={isEdit && <EditFilled />} color={color} onClick={()=> { isEdit && console.log(isEdit) }}>{tagName}</Tag>
                    <Tag color={color}>{tagName}</Tag>
                    <Tag color={color}>{tagName}</Tag>
                </Space> */}
            </>
        ) : (
            <Space size={[0, 8]} wrap >
                <Tooltip title={isTooltip && tagName}>
                    <Tag className="px-3 text-sm font-semibold rounded-md" icon={isEdit && <EditFilled />} color={color} onClick={()=> { isEdit && console.log(isEdit) }}>{tagName}</Tag>
                </Tooltip>
                
            </Space>
        )}
        </>
    )
}

export default TagButton
