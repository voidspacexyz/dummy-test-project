import React, { useRef } from "react";
import videojs from "video.js";
import VideoJS from "./VideoJS";

const Player = ({ upload, type = null }: { upload: any; type?: string }) => {
  const playerRef = useRef(null);

  const videoType = (src) => {
    switch (src) {
      case "mp4":
        return "video/mp4";
      case "mpeg4":
        return "video/mpeg4";
      case "mkv":
        return "video/mkv";
      case "m4v":
        return "video/m4v";
      case "avi":
        return "video/avi";
      case "flv":
        return "video/flv";

      case "mp3":
        return "audio/mp3";
      case "amr":
        return "audio/amr";
      case "aac":
        return "audio/aac";
      case "m4a":
        return "audio/m4a";
      case "wav":
        return "audio/wav";
      case "ogg":
        return "audio/ogg";
      case "wma":
        return "wma";
    }
  };

  const videoJsOptions = {
    autoplay: false,
    controls: true,
    responsive: true,
    fluid: true,
    preload: true,
    sources: [
      {
        src: upload,
        type: type !== null ? type : videoType(upload),
      },
    ],
  };

  const handlePlayerReady = (player) => {
    playerRef.current = player;

    // You can handle player events here, for example:
    player.on("waiting", () => {
      videojs.log("player is waiting");
    });

    player.on("dispose", () => {
      videojs.log("player will dispose");
    });
  };

  return (
    <div className="sm:w-[60%] border-2 border-[#202020] border-solid rounded-lg">
      <VideoJS options={videoJsOptions} onReady={handlePlayerReady} />
    </div>
  );
};
export default Player;
