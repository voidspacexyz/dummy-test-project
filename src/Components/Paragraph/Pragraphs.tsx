import React, { useState } from 'react';
import { Switch, Typography } from 'antd';

const { Paragraph, Text } = Typography;

const Pragraphs = ({isExpand, rows, text}: {isExpand?:boolean, rows?:number, text?:any}) => {
  const [ellipsis, setEllipsis] = useState(isExpand);

  return (
    <>
        <Paragraph ellipsis={ellipsis ? { rows: rows ? rows : 2, expandable: true, symbol: 'more' } : false}>
            {text}
        </Paragraph>
    </>
  );
};

export default Pragraphs