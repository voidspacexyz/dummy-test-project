import React from "react";
import { FiHome, FiLink, FiPhone } from "react-icons/fi";

const Footer = () => {
  return (
    <footer className="text-center lg:text-left bg-gray-100 text-gray-600 bottom-0">
      <div className="mx-6 py-10 text-center md:text-left">
        <div className="container grid sm:grid-cols-1 lg:grid-cols-5 gap-20 p-20">
          <div className="col-span-1"></div>
          <div className="col-span-1">
            <h6 className=" font-semibold mb-4 flex items-center justify-center md:justify-start">
              <a href="https://open.janastu.org/projects/cowmesh">PAPAD</a>
            </h6>
            <p className="pb-2">
              A Janastu collective project - an inclusive Free and Open Source
              (FOSS) project - to Annotate, Discover and Navigate stories, ideas
              and knowledge of our communities.
            </p>
            <p>
              We are looking for creative people or developers to contribute to
              the project.
            </p>
          </div>
          <div className="col-span-1">
            <h6 className=" font-normal mb-4 flex justify-center md:justify-start">
              Organisations
            </h6>
            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://open.janastu.org">open.janastu.org</a>
            </p>
            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="http://servelots.com/">Servelots</a>
            </p>
          </div>
          <div className="col-span-1">
            <h6 className=" font-normal mb-4 flex justify-center md:justify-start">
              Code
            </h6>
            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://gitlab.com/servelots/papad/">Papad Repository</a>
            </p>
            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://stories.janastu.org">Papad Demo</a>
            </p>
            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://gitlab.com/servelots/papad/papad-docker">
                Papad Installation
              </a>
            </p>
            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://gitlab.com/servelots/papad/papad-issues/-/issues">
                File an issue
              </a>
            </p>
            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://gitlab.com/servelots/papad/papad-api">
                Papad API
              </a>
            </p>
            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://gitlab.com/servelots/papad/papad-ui">Papad UI</a>
            </p>
            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://gitlab.com/servelots/papad/papad-docs">
                Papad Docs
              </a>
            </p>
          </div>
          {/* <div className="col-span-1">
            <h6 className=" font-normal mb-4 flex justify-center md:justify-start">
              Import Links
            </h6>
            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://open.janastu.org">open.janastu.org</a>
            </p>
            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://stories.janastu.org">Papad Demo</a>
            </p>

            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://gitlab.com/servelots/papad/papad-docker">
                Papad Installation
              </a>
            </p>

            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://gitlab.com/servelots/papad/papad-issues/-/issues">
                File an issue
              </a>
            </p>
            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://gitlab.com/servelots/papad">Papad Repo</a>
            </p>
          </div>{" "}
          <div className="col-span-1">
            <h6 className=" font-normal mb-4 flex justify-center md:justify-start">
              Import Links
            </h6>
            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://open.janastu.org">open.janastu.org</a>
            </p>
            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://stories.janastu.org">Papad Demo</a>
            </p>

            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://gitlab.com/servelots/papad/papad-docker">
                Papad Installation
              </a>
            </p>

            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://gitlab.com/servelots/papad/papad-issues/-/issues">
                File an issue
              </a>
            </p>
            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://gitlab.com/servelots/papad">Papad Repo</a>
            </p>
          </div>{" "}
          <div className="col-span-1">
            <h6 className=" font-normal mb-4 flex justify-center md:justify-start">
              Import Links
            </h6>
            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://open.janastu.org">open.janastu.org</a>
            </p>
            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://stories.janastu.org">Papad Demo</a>
            </p>

            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://gitlab.com/servelots/papad/papad-docker">
                Papad Installation
              </a>
            </p>

            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://gitlab.com/servelots/papad/papad-issues/-/issues">
                File an issue
              </a>
            </p>
            <p className="flex font-semibold items-center justify-center md:justify-start mb-4 gap-5">
              <FiLink />
              <a href="https://gitlab.com/servelots/papad">Papad Repo</a>
            </p>
          </div> */}
        </div>
      </div>
      <div className="flex justify-between p-6 bg-gray-200 ">
        <div className="gap-5 text-center">
          <span>© 2023 </span>
          <a
            className="text-gray-600 font-normal"
            href="https://open.janastu.org"
          >
            janastu.org
          </a>
          <span>, Inc. </span>
          <a
            className="text-gray-600 font-normal"
            href="https://gitlab.com/servelots/papad/papad-api/-/blob/prod/LICENSE"
          >
            GNU AGPLv3
          </a>
        </div>
        <div className="text-start">
          <p>Language: English Mature content</p>
          <p>Filter: None </p>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
