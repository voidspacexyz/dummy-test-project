import React, { Children, ReactChild, ReactChildren } from "react";
import { Disclosure } from "@headlessui/react";
import { MdKeyboardArrowDown } from "react-icons/md";

const Disclosures = ({
  text,
  title,
  children,
}: {
  text?: string;
  title?: string;
  children?: React.ReactNode;
}) => {
  return (
    <Disclosure>
      {({ open }) => (
        <>
          <Disclosure.Button className="flex w-full justify-between rounded-lg bg-emerald-100 px-4 py-2 text-left text-sm font-medium text-emerald-900 hover:bg-emerald-200 focus:outline-none focus-visible:ring focus-visible:ring-emerald-500 focus-visible:ring-opacity-75">
            <span>{title}</span>
            <MdKeyboardArrowDown
              className={`${
                open ? "rotate-180 transform" : ""
              } h-5 w-5 text-emerald-500`}
            />
          </Disclosure.Button>
          <Disclosure.Panel className="px-4 pt-4 pb-2 text-sm text-gray-500">
            {children}
          </Disclosure.Panel>
        </>
      )}
    </Disclosure>
  );
};

export default Disclosures;
