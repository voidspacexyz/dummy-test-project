import React, { useEffect, useRef, useState } from "react";
import { IoClose, IoHome, IoMenu } from "react-icons/io5";
import {
  MdArrowForwardIos,
  MdGroups,
  MdSpaceDashboard,
  MdViewList,
} from "react-icons/md";
import { IoMdLogIn, IoMdLogOut } from "react-icons/io";
import { FaUser, FaUserEdit, FaHandsHelping } from "react-icons/fa";
import {
  MdKeyboardArrowDown,
  MdSearch,
  MdOutlineAdminPanelSettings,
  MdOutlineCollectionsBookmark,
} from "react-icons/md";
import { VscSettings, VscRequestChanges,VscThreeBars } from "react-icons/vsc";
import axios from "axios";
import { CgProfile } from "react-icons/cg";
import { GoSettings } from "react-icons/go";
import { RiDraftLine } from "react-icons/ri";
import { RxCross2 } from "react-icons/rx";
import IconButton from "../Button/IconButton";
import { ImStatsDots } from "react-icons/im";
import {
  loginState,
  prodState,
  tokenState,
  usernameState,
} from "../../Modules/AuthState/AuthState";
import { useRecoilState, useRecoilValue } from "recoil";
import { Fragment } from "react";
import { Disclosure, Menu, Transition } from "@headlessui/react";
import { Badge } from "antd";

const Nav = () => {
  const [isOpen, setIsOpen] = useState(false);
  const navRef = useRef(null);
  const [me, setMe] = useRecoilState(usernameState);
  const [userName, setUserName] = useState("");
  const [fullName, setFullName] = useState("");
  const [firstName, setFirstName] = useState("");
  const [curMedia, setCurMedia] = useState<any>("");
  const [imgSrc, setImgSrc] = useState<any>("");
  const [superUser, setSuperUser] = useState<any>(null);
  const [login, setLogin] = useRecoilState(loginState);
  const auth = localStorage.getItem("token");
  const baseURL = localStorage.getItem("prod_url");

  const navigation = [
    {
      name: "Home",
      href: "/",
      current: true,
      icon: <IoHome className="mr-2" size={18} />,
      isAllowed: true,
    },
    {
      name: "About",
      href: "/about",
      current: false,
      icon: <MdSpaceDashboard className="mr-2" size={20} />,
      isAllowed: true,
    },
    // { name: 'My Requests', href: '/requests', current: false, icon: <MdViewList className="mr-2"  size={20} />, isAllowed: localStorage.getItem("token") ? true : false },
    {
      name: "Stats",
      href: "/auth/stats",
      current: false,
      icon: <ImStatsDots className="mr-2" size={16} />,
      isAllowed: superUser ? true : false,
    },
  ];

  useEffect(() => {
    auth && getUser();
  }, []);
  useEffect(() => {
    login.isSuccess && getUser();
  }, [login.isSuccess === true]);
  const getUser = () => {
    const options = {
      method: "GET",
      url: `${baseURL}auth/users/me/`,
      headers: {
        Authorization: `Token ${localStorage.getItem("token")}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setUserName(response.data.username);
        setFirstName(response.data.first_name);
        setFullName(response.data.first_name + " " + response.data.last_name);
        localStorage.setItem(
          "fullName",
          `${response.data.first_name + " " + response.data.last_name}`
        );
        localStorage.setItem("username", `${response.data.username}`);
        localStorage.setItem("userUUID", `${response.data.id}`);

        setSuperUser(response.data.is_superuser);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleLogout = () => {
    setLogin({ isLoading: true, isError: false, isSuccess: false });

    try {
      setLogin({ isLoading: false, isError: false, isSuccess: true });
    } catch (error) {
      setLogin({ isLoading: false, isError: true, isSuccess: false });
    }
  };

  const logout = () => {
    const options = {
      method: "POST",
      url: `${baseURL}auth/token/logout`,
      headers: {
        Authorization: `Token ${localStorage.getItem("token")}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        handleLogout();
        successLogout();
        window.location.href = "/auth/login";
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const successLogout = () => {
    localStorage.clear();
    setUserName("");
  };
  function classNames(...classes) {
    return classes.filter(Boolean).join(" ");
  }
  const BadgeComponent = () => {
    return (
      <Badge count={"Beta"} showZero color='#faad14' />
    )
  } 
  return (
    <>
      <Disclosure as="nav" className="bg-white shadow-lg sticky top-0 z-10">
        {({ open }) => (
          <>
            <div className="mx-auto max-w-[1536px] px-2 sm:px-6">
              <div className="relative flex h-16 items-center justify-between">
                <div
                  className={`absolute inset-y-0 right-0 justify-end flex items-center sm:hidden md:hidden`}
                >
                  {/* Mobile menu button*/}
                  <Disclosure.Button className="inline-flex items-center justify-center rounded-md p-2 text-gray-400 hover:bg-gray-700 hover:text-white focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                    <span className="sr-only">Open main menu</span>
                    {open ? (
                      <RxCross2
                        className="block h-6 w-6"
                        color="black"
                        aria-hidden="true"
                      />
                    ) : (
                      <VscThreeBars
                        className="block h-6 w-6"
                        color="black"
                        aria-hidden="true"
                      />
                    )}
                  </Disclosure.Button>
                </div>
                <div
                  className={`absolute right-0 justify-end items-center hidden mr-[12rem] sm:block`}
                >
                  <div className={`justify-end flex items-center `}>
                    <input
                      type="text"
                      placeholder={`Search here  ...`}
                      value={curMedia}
                      onChange={(e) => {
                        setCurMedia(e.target.value);
                      }}
                      className="bg-[#dde2e8] border-2 rounded-3xl border-gray-200 w-[30rem] h-[2.5rem] text-gray-700 focus:outline-none focus:border-purple-500"
                    />
                    <IconButton size="xs" type="tertiary" className="-ml-9">
                      <a href={`/search/${curMedia}`} className="w-full">
                        <MdSearch size={20} />
                      </a>
                    </IconButton>
                  </div>
                </div>
                <div className="flex flex-1 items-center inset-y-0 right-0 justify-start sm:items-stretch">
                  <div
                    ref={navRef}
                    className="flex flex-shrink-0 items-center place-items-center"
                  >
                    <img
                      className="block h-10 w-auto lg:hidden"
                      src={process.env.PUBLIC_URL + "/images/papad.png"}
                      alt="Papad Logo"
                    />
                    <img
                      className="hidden h-10 w-auto lg:block"
                      src={process.env.PUBLIC_URL + "/images/papad.png"}
                      alt="Papad Logo"
                    />
                    <div className="px-3 text-xl font-semibold ">
                      <a href="/">Papad</a>
                    </div>
                  </div>
                  <div className="hidden sm:ml-6 sm:block">
                    <div className="flex space-x-4 ">
                      {navigation.map((item) => (
                        <>
                          {item?.isAllowed && (
                            <a
                              key={item.name}
                              href={item.href}
                              className={classNames(
                                "text-black hover:bg-gray-700 hover:text-white",
                                "flex w-auto px-3 py-2 rounded-md text-md font-medium place-items-center"
                              )}
                            >
                              {item.icon}
                              {item.name}
                            </a>
                          )}
                        </>
                      ))}
                    </div>
                  </div>
                </div>

                <div className="absolute inset-y-0 right-0 flex items-center mr-[2rem] lg:mr-0 pr-2 sm:static sm:inset-auto sm:ml-6 sm:pr-0 cursor-pointer">
                  {localStorage.getItem("token") || login?.isSuccess ? (
                    <Menu as="div" className="flex ml-3 ">
                      <Menu.Button className="flex text-sm focus:outline-none focus:ring-none focus:ring-none">
                        <div className="items-center flex rounded-xl p-2 focus:outline-none ">
                          {imgSrc ? (
                            <img
                              className="h-[2rem] w-[2rem] rounded-full"
                              src={imgSrc}
                              alt=""
                            />
                          ) : (
                            <div className=" flex gap-2 p-2 text-center items-center justify-center flex-shrink-0 self-center flex-row relative border hover:bg-[#d6ebfd] rounded-full h-8 ">
                              <FaUser
                                className="h-[1rem] w-[1rem] rounded-full"
                              />
                              {firstName}
                                <MdKeyboardArrowDown className="ml-1" size={25} />
                            </div>
                          )}
                        </div>
                      </Menu.Button>
                      <Transition
                        as={Fragment}
                        enter="transition ease-out duration-100"
                        enterFrom="transform opacity-0 scale-95"
                        enterTo="transform opacity-100 scale-100"
                        leave="transition ease-in duration-75"
                        leaveFrom="transform opacity-100 scale-100"
                        leaveTo="transform opacity-0 scale-95"
                      >
                        <div className="text-md font-semibold absolute right-0 px-2 z-10 mt-[3.6rem] min-w-[16rem] max-w-[100%] origin-top-right rounded-md text-black bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none ">
                          <Menu.Items className="divide-y divide-[#e5e7eb] w-full">
                            <Menu.Item>
                              <div className="text-sm font-normal text-dark">
                                <div className="flex flex-row justify-between items-end w-full">
                                  {imgSrc ? (
                                    <div className="mr-[12px] flex-shrink-0 self-center inline-flex flex-row items-center relative border border-gray-400 bg-gray-100 rounded-full h-8 w-8">
                                      <img
                                        className="rounded-full"
                                        src={imgSrc}
                                        width="30"
                                        height="30"
                                      />
                                    </div>
                                  ) : (
                                    <div className=" p-2 mr-[12px] text-center items-center justify-center flex-shrink-0 self-center inline-flex flex-row relative border border-gray-400 bg-gray-100 rounded-full h-8 w-8">
                                      <FaUser
                                        className="h-[1.5rem] w-[1.5rem] rounded-full"
                                        size={10}
                                      />
                                    </div>
                                  )}

                                  <div className="flex flex-col w-full">
                                    <h5 className="text-sm font-normal mb-[4px] h-[20px]">
                                      {fullName}
                                    </h5>
                                    <span className="text-xs text-gray-700 font-normal mb-[4px]">
                                      @{userName}
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </Menu.Item>
                            <Menu.Item>
                              <div className="my-2">
                                <span className="my-5 text-xs text-zinc-500">
                                  Personal
                                </span>
                                <div className="flex flex-row w-full my-2 text-sm font-normal">
                                  <div className="flex flex-col w-full">
                                    <div
                                      className="p-2 hover:bg-[#acd3ee] cursor-pointer flex  justify-between"
                                      onClick={() => {
                                        window.location.href = "/home";
                                      }}
                                    >
                                      <span className="flex place-items-center">
                                        <MdOutlineCollectionsBookmark
                                          className="pr-2"
                                          size={25}
                                        />{" "}
                                        <span>My Collections </span>
                                      </span>{" "}
                                    </div>
                                    <div
                                      className="p-2 hover:bg-[#acd3ee] cursor-pointer flex justify-between"
                                      onClick={() => {
                                        window.location.href = "/requests";
                                      }}
                                    >
                                      <span className="flex place-items-center">
                                        <VscRequestChanges
                                          className="pr-2"
                                          size={25}
                                        />{" "}
                                        <span>My Requests</span>{" "}
                                      </span>
                                    </div>
                                    <div className="p-2 hover:bg-[#acd3ee] cursor-not-allowed flex justify-between">
                                      <span className="flex place-items-center">
                                        <RiDraftLine
                                          className="pr-2"
                                          size={25}
                                        />{" "}
                                        <span>Drafts</span>{" "}
                                      </span>
                                      <BadgeComponent/>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </Menu.Item>
                            <Menu.Item>
                              <div className="my-2">
                                <span className="my-5 text-xs text-zinc-500">
                                  Settings
                                </span>
                                <div className="flex flex-row w-full my-2 text-sm font-normal">
                                  <div className="flex flex-col w-full">
                                    <div className="p-2 hover:bg-[#acd3ee] cursor-not-allowed flex justify-between">
                                      <span className="flex place-items-center">
                                        <FaUserEdit
                                          className="pr-2"
                                          size={25}
                                        />{" "}
                                        <span>Edit Profile </span>
                                      </span>
                                      <BadgeComponent/>
                                    </div>
                                    {superUser && (
                                      <div className="p-2 hover:bg-[#acd3ee] cursor-not-allowed flex justify-between">
                                        <span className="flex place-items-center">
                                          <MdOutlineAdminPanelSettings
                                            className="pr-2"
                                            size={25}
                                          />{" "}
                                          <span>Admin Settings</span>
                                        </span>
                                        <span className="rounded-md bg-green-200 place-items-center p-1 text-xs">
                                          Beta
                                        </span>
                                      </div>
                                    )}
                                    <div className="p-2 hover:bg-[#acd3ee] cursor-not-allowed flex justify-between">
                                      <span className="flex place-items-center">
                                        <GoSettings
                                          className="pr-2"
                                          size={25}
                                        />{" "}
                                        <span>Settings</span>
                                      </span>
                                      <BadgeComponent/>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </Menu.Item>
                            <Menu.Item>
                              <div className="my-2">
                                <span className="my-5 text-xs text-zinc-500 ">
                                  Support
                                </span>
                                <div className="flex flex-row w-full my-2 text-sm font-normal">
                                  <div className="flex flex-col w-full">
                                    <div className="p-2 hover:bg-[#acd3ee] cursor-not-allowed flex justify-between">
                                      <span className="flex place-items-center">
                                        <FaHandsHelping
                                          className="pr-2"
                                          size={25}
                                        />
                                        <span> Help</span>
                                      </span>
                                      <BadgeComponent/>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </Menu.Item>
                            <Menu.Item>
                              <div className="py-3 text-sm font-medium place-items-center">
                                <div
                                  className={
                                    "flex rounded-lg text-center justify-center px-4 py-2 text-sm bg-red-600 hover:bg-[#f33e3e] items-center cursor-pointer "
                                  }
                                  onClick={() => {
                                    logout();
                                  }}
                                >
                                  <IoMdLogOut className="mr-1" size={15} />LOGOUT
                                </div>
                              </div>
                            </Menu.Item>
                          </Menu.Items>
                        </div>
                      </Transition>
                    </Menu>
                  ) : (
                    <div className={`sm:ml-6 sm:block`}>
                      <div className="flex space-x-4 ">
                        <a
                          href="/auth/login"
                          className={classNames(
                            "text-black bg-[#d6ebfd] hover:bg-[#afddaf] hover:text-black cursor-pointer",
                            "flex w-auto px-3 py-2 rounded-md text-md font-semibold place-items-center"
                          )}
                        >
                          <IoMdLogIn className="pr-1" size={20} /> Login /
                          Sign-Up
                        </a>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
            <Disclosure.Panel className="sm:hidden">
              <div className="space-y-1 px-2 pt-2 pb-3 divide-y divide-solid">
                <div className="justify-end flex items-center">
                  <input
                    type="text"
                    placeholder={`Search here  ...`}
                    value={curMedia}
                    onChange={(e) => {
                      setCurMedia(e.target.value);
                    }}
                    className="bg-gray-200 border-2 border-gray-200 rounded w-full text-gray-700 focus:outline-none focus:border-purple-500"
                  />
                  <IconButton size="xs" type="tertiary" className="-ml-9">
                    <a href={`/search/${curMedia}`} className="w-full">
                      <MdSearch size={20} />
                    </a>
                  </IconButton>
                </div>
                {navigation.map((item) => (
                  <>
                    {item?.isAllowed && (
                      <Disclosure.Button
                        key={item.name}
                        as="a"
                        href={item.href}
                        className={classNames(
                          "text-black hover:bg-gray-700 hover:text-white",
                          "flex w-auto px-3 py-2 rounded-md text-md font-medium place-items-center"
                        )}
                      >
                        {item.icon}
                        {item.name}
                      </Disclosure.Button>
                    )}
                  </>
                ))}
              </div>
            </Disclosure.Panel>
          </>
        )}
      </Disclosure>
    </>
  );
};

export default Nav;
