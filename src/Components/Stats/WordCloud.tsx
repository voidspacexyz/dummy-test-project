import React from "react";
import { TagCloud } from "react-tagcloud";

const WordCloud = ({ tag }) => {
  const customRenderer = (tag, size, color) => (
    <span
      key={tag.name}
      style={{
        animation: "blinker 5s linear infinite",
        animationDelay: `${Math.random() * 2}s`,
        fontSize: `1em`,
        border: `5px solid ${color}`,
        margin: "5px",
        padding: "5px",
        display: "inline-block",
        color: "black",
      }}
    >
      {tag.name} - {tag.count}
    </span>
  );

  return (
    <TagCloud
      tags={tag}
      minSize={1}
      maxSize={5}
      renderer={customRenderer}
      style={{ width: 300, textAlign: "left", textColor: "black" }}
      className=""
    />
  );
};

export default WordCloud;
