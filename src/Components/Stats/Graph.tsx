import React from "react";
import ReactEcharts from "echarts-for-react";

interface GraphNode {
  symbolSize: number;
  label?: {
    show?: boolean;
  };
}

const Graph = ({ data, layout, title }) => {
  data.nodes.forEach((node: GraphNode) => {
    node.label = {
      show: node.symbolSize > 0,
    };
  });

  const option = {
    title: {
      text: title,
      subtext: "Circular graph",
      top: "bottom",
      left: "center",
    },
    tooltip: {},
    legend: [
      {
        data: data?.categories.map((a: { name: string }) => {
          return a.name;
        }),
      },
    ],
    animationDurationUpdate: 1500,
    animationEasingUpdate: "quinticInOut",
    series: [
      {
        name: title,
        type: "graph",
        layout: layout,
        circular: {
          rotateLabel: true,
        },
        data: data?.nodes,
        links: data?.links,
        categories: data?.categories,
        roam: true,
        label: {
          position: "right",
          formatter: "{b}",
        },
        lineStyle: {
          color: "source",
          curveness: 0.3,
        },
        emphasis: {
          focus: "adjacency",
          lineStyle: {
            width: 10,
          },
        },
      },
    ],
  };

  return (
    <>
      {data.length !== 0 && (
        <ReactEcharts
          option={option}
          style={{ height: "1000px", width: "100%" }}
        />
      )}
    </>
  );
};

export default Graph;
