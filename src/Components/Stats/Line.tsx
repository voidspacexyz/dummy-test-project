import React, { useEffect, useState } from "react";
import ReactEcharts from "echarts-for-react";

const Line = ({ stats, label }: { stats: any; label: string }) => {
  const [xValue, setXValue] = useState<any>([]);
  const [yValue, setYValue] = useState<any>([]);

  useEffect(() => {
    setXValue(
      stats.map((data) => {
        return data.created_date;
      })
    );
    setYValue(
      stats.map((data) => {
        return data.total;
      })
    );
  }, []);

  const option = {
    title: {
      text: `${label} Stats`,
    },
    tooltip: {
      trigger: "axis",
    },
    legend: {
      data: [label],
    },
    grid: {
      left: "3%",
      right: "4%",
      bottom: "3%",
      containLabel: true,
    },
    toolbox: {
      feature: {
        saveAsImage: {},
      },
    },
    xAxis: {
      type: "category",
      boundaryGap: false,
      data: xValue,
    },
    yAxis: {
      type: "value",
    },
    series: [
      {
        name: label,
        type: "line",
        stack: "Total",
        color: "#FB8833",
        data: yValue,
      },
    ],
  };

  return (
    <>
      <ReactEcharts
        option={option}
        style={{ height: "400px", width: "100%" }}
      />
    </>
  );
};

export default Line;
