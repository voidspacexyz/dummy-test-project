import { atom } from "recoil";

const loginState = atom({
  key: "loginState",
  default: {
    isLoading: false,
    isError: false,
    isSuccess: localStorage.getItem("token") ? true : false,
  },
});
const startTimer = atom({
  key: "startTimer",
  default: "00:00:00",
});

const endTimer = atom({
  key: "endTimer",
  default: "00:00:00",
});

const tokenState = atom({
  key: "tokenState",
  default: null,
});

const prodState = atom({
  key: "prodState",
  default: null,
});

const usernameState = atom({
  key: "usernameState",
  default: null,
});

const fullnameState = atom({
  key: "fullnameState",
  default: "",
});

const cardTimeState = atom({
  key: "cardTimeState",
  default: "",
});

const editAnnoIDState = atom({
  key: "editAnnoIDState",
  default: "",
});

export {
  loginState,
  tokenState,
  prodState,
  usernameState,
  startTimer,
  endTimer,
  fullnameState,
  cardTimeState,
  editAnnoIDState,
};
