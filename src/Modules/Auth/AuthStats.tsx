import axios from "axios";
import React, { useEffect, useState } from "react";
import { useRecoilValue } from "recoil";
import { Line } from "../../Components";
import { prodState, tokenState } from "../AuthState/AuthState";

const AuthStats = () => {
  const [superUser, setSuperUser] = useState<any>(null);
  const [grpStats, setGrpStats] = useState<any>([]);
  const [userStats, setUserStats] = useState<any>([]);
  const [mediaStats, setMediaStats] = useState<any>([]);
  const [annoStats, setAnnoStats] = useState<any>([]);

  const auth = localStorage.getItem("token");
  const baseURL = localStorage.getItem("prod_url");

  useEffect(() => {
    getUser();
    getGrpStats();
    getUserStats();
    getMediaStats();
    getAnnoStats();
  }, []);

  const getUser = () => {
    const options = {
      method: "GET",
      url: `${baseURL}auth/users/me/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setSuperUser(response.data.is_superuser);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getGrpStats = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/instancegroupstats/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setGrpStats(response.data);
      })
      .catch(function (error) {
        console.error(error);
      });
  };

  const getUserStats = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/instanceuserstats/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setUserStats(response.data);
      })
      .catch(function (error) {
        console.error(error);
      });
  };

  const getMediaStats = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/instancemediastats/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setMediaStats(response.data);
      })
      .catch(function (error) {
        console.error(error);
      });
  };

  const getAnnoStats = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/instanceannotationstats/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setAnnoStats(response.data);
      })
      .catch(function (error) {
        console.error(error);
      });
  };

  return (
    <div className="container gap-10">
      {superUser && (
        <>
          <div className="items-center justify-center text-center place-items-center">
            <div className="col-md-12">
              <div className="font-bold text-3xl">Overall Statistics</div>
            </div>
          </div>
          <Line stats={grpStats} label="Groups" />
          <Line stats={userStats} label="Users" />
          <Line stats={mediaStats} label="Medias" />
          <Line stats={annoStats} label="Annotations" />
        </>
      )}
    </div>
  );
};

export default AuthStats;
