import axios from "axios";
import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useRecoilState, useRecoilValue } from "recoil";
import { getBaseURL, getData } from "../../api";
import { Button } from "../../Components";
import {
  loginState,
  prodState,
  tokenState,
  usernameState,
} from "../AuthState/AuthState";

export const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [token, setToken] = useRecoilState(tokenState);
  const [user, setUser] = useState<any>();
  const [error, setError] = useState<any[]>([]);
  const [login, setLogin] = useRecoilState(loginState);
  const [me, setMe] = useRecoilState(usernameState);

  const navigate = useNavigate();
  const baseURL = localStorage.getItem("prod_url");

  const handleLogin = () => {
    setLogin({ isLoading: true, isError: false, isSuccess: false });

    try {
      setLogin({ isLoading: false, isSuccess: true, isError: false });
    } catch (error) {
      setLogin({ isLoading: false, isError: true, isSuccess: false });
    }
  };

  const postRecord = async (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append("username", username);
    formData.append("password", password);

    const options = {
      method: "POST",
      url: `${baseURL}auth/token/login/`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
      data: formData,
    };

    axios
      .request(options)
      .then((data) => {
        setToken(data.data.auth_token);
        localStorage.setItem("token", data.data.auth_token);
        console.log(data.data);

        handleLogin();
        navigate("/");
      })
      .catch((error) => {
        setError((prev) => [...prev, error.response.data]);
        console.log(`ERROR: \n${error}`);
      });
  };

  const newRecord = () => {
    alert("Contact the Developers");
    setUsername("");
    setPassword("");
  };

  return (
    <div>
      <div className="item-align-center">
        <div className="flex justify-center">
          <div className="rounded-lg shadow-xl bg-gray-50 lg:w-1/2">
            <div className="m-4">
              <form onSubmit={(e) => postRecord(e)}>
                <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                  <h3 className="text-3xl font-semibold">LOG IN</h3>
                </div>
                <div className="p-5 border-b border-solid border-slate-200 rounded-t">
                  <div className="md:flex md:items-center m-5 mb-6">
                    <div className="md:w-1/3">
                      <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                        Username
                      </label>
                    </div>
                    <div className="md:w-2/3">
                      <input
                        className="bg-gray-200 border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
                        type="username"
                        maxLength={300}
                        value={username}
                        onChange={(e) => {
                          setUsername(e.target.value);
                        }}
                        required
                      />
                    </div>
                  </div>
                  <div className="md:flex md:items-center m-5 mb-6">
                    <div className="md:w-1/3">
                      <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                        Password
                      </label>
                    </div>
                    <div className="md:w-2/3">
                      <input
                        className="current-password bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                        type="password"
                        minLength={8}
                        value={password}
                        onChange={(e) => {
                          setPassword(e.target.value);
                        }}
                        required
                      />
                    </div>
                  </div>
                </div>
                {error?.map((err, i) => (
                  <div
                    className="flex items-center justify-center p-5 border-b border-solid border-slate-200 rounded-t"
                    key={i}
                  >
                    <h5 className="text-xl font-medium text-red-600">
                      {err.non_field_errors}
                    </h5>
                  </div>
                ))}
                <div className="text-center mt-6 p-5">
                  <Button className="w-full" type="secondary" size="sm">
                    Sign In
                  </Button>
                  <Button
                    type="tertiary"
                    size="sm"
                    color="red"
                    className="uppercase"
                    onClick={newRecord}
                  >
                    Forgot Password
                  </Button>
                </div>
                <hr className="mt-6 border-b-1 border-gray-200 pt-5" />

                <div className="flex p-2 space-x-4 justify-center mb-10">
                  <Button
                    type="primary"
                    size="sm"
                    onClick={() => navigate("/auth/register")}
                    className="w-fit bg-blue-500 text-white active:bg-blue-700 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                  >
                    Create new account
                  </Button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
