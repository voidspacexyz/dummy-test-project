import axios from "axios";
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { useRecoilValue } from "recoil";
import { Button } from "../../Components";
import { prodState } from "../AuthState/AuthState";

export const Register = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [first, setFirst] = useState("");
  const [last, setLast] = useState("");
  const [error, setError] = useState<any[]>([]);

  const navigate = useNavigate();
  const baseURL = localStorage.getItem("prod_url");

  const newRecord = () => {
    setUsername("");
    setPassword("");
    setEmail("");
    setFirst("");
    setLast("");
  };

  const postRecord = (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append("username", username);
    formData.append("password", password);
    formData.append("email", email);
    formData.append("first_name", first);
    formData.append("last_name", last);

    const options = {
      method: "POST",
      url: `${baseURL}auth/users/`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
      data: formData,
    };

    axios
      .request(options)
      .then((data) => {
        console.log(data);
        navigate("/auth/login");
      })
      .catch((error) => {
        setError(error.response.data);
        console.error(error);
      });
  };

  return (
    <div>
      <div className="item-align-center">
        <div className="flex justify-center">
          <div className="rounded-lg shadow-xl bg-gray-50 lg:w-1/2">
            <div className="m-4">
              <form onSubmit={(e) => postRecord(e)}>
                <div className="flex items-start justify-between p-5 border-b border-solid border-slate-200 rounded-t">
                  <h3 className="text-3xl font-semibold uppercase">
                    Create New Account
                  </h3>
                </div>
                <div className="p-5 border-b border-solid border-slate-200 rounded-t">
                  <div className="md:flex md:items-center m-5 mb-6">
                    <div className="md:w-1/3">
                      <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                        Username
                      </label>
                    </div>
                    <div className="md:w-2/3">
                      <input
                        className="bg-gray-200 border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
                        type="text"
                        maxLength={300}
                        value={username}
                        onChange={(e) => {
                          setUsername(e.target.value);
                        }}
                        required
                      />
                    </div>
                  </div>
                  <div className="md:flex md:items-center m-5 mb-6">
                    <div className="md:w-1/3">
                      <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                        Password
                      </label>
                    </div>
                    <div className="md:w-2/3">
                      <input
                        className="new-password bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                        type="password"
                        minLength={8}
                        value={password}
                        onChange={(e) => {
                          setPassword(e.target.value);
                        }}
                        required
                      />
                    </div>
                  </div>

                  <div className="md:flex md:items-center m-5 mb-6">
                    <div className="md:w-1/3">
                      <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                        Email ID
                      </label>
                    </div>
                    <div className="md:w-2/3">
                      <input
                        className="bg-gray-200 border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
                        type="email"
                        maxLength={300}
                        value={email}
                        onChange={(e) => {
                          setEmail(e.target.value);
                        }}
                        required
                      />
                    </div>
                  </div>

                  <div className="md:flex md:items-center m-5 mb-6">
                    <div className="md:w-1/3">
                      <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                        First Name
                      </label>
                    </div>
                    <div className="md:w-2/3">
                      <input
                        className="bg-gray-200 border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
                        type="text"
                        maxLength={300}
                        value={first}
                        onChange={(e) => {
                          setFirst(e.target.value);
                        }}
                      />
                    </div>
                  </div>

                  <div className="md:flex md:items-center m-5 mb-6">
                    <div className="md:w-1/3">
                      <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                        Last Name
                      </label>
                    </div>
                    <div className="md:w-2/3">
                      <input
                        className="bg-gray-200 border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
                        type="text"
                        maxLength={300}
                        value={last}
                        onChange={(e) => {
                          setLast(e.target.value);
                        }}
                      />
                    </div>
                  </div>
                </div>

                {Object.values(error).map((err, key) => (
                  <div
                    className="flex items-center justify-center p-5 border-b border-solid border-slate-200 rounded-t"
                    key={key}
                  >
                    <h5 className="text-xl font-medium text-red-600">{err}</h5>
                  </div>
                ))}

                <div className="text-center mt-6 p-5">
                  <Button size="sm" className="w-full" type="secondary">
                    Register
                  </Button>
                  <Button
                    size="sm"
                    type="tertiary"
                    color="red"
                    className="uppercase"
                    onClick={newRecord}
                  >
                    Cancel
                  </Button>
                </div>
                <hr className="mt-6 border-b-1 border-gray-200 pt-5" />

                <div className="flex flex-row p-2 space-x-4 justify-center mb-10">
                  <Button
                    type="primary"
                    size="sm"
                    onClick={() => navigate("/auth/login")}
                    className="w-fit bg-blue-500 text-white active:bg-blue-700 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
                  >
                    LOGIN
                  </Button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
