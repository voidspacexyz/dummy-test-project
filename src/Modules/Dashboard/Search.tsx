import { Cascader, Col, Input, Row, Select, Space } from "antd";
import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import {
  MdClose,
  MdOutlineReportProblem,
  MdCheckCircle,
  MdSearch,
} from "react-icons/md";
import { useNavigate, useParams } from "react-router-dom";
import { useRecoilValue } from "recoil";
import {
  AnnotationCard,
  Button,
  CustomQuestionModal,
  DialogModal,
  IconButton,
  Loader,
  MultiGroupModal,
} from "../../Components";
import CardLayout from "../../Components/Cards/CardLayout";
import { prodState, tokenState } from "../AuthState/AuthState";
import { secToTimestamp } from "../Media/Annotation";

const Search = () => {
  const searchInput = useRef<HTMLInputElement>(null);
  const [isShown, setIsShown] = useState(false);
  const [groupToggle, setGroupToggle] = useState(false);
  const [curMedia, setCurMedia] = useState<string>("");
  const [mediaVal, setMediaVal] = useState("search");
  const [annoVal, setAnnoVal] = useState("name");
  const [disMedia, setDisMedia] = useState(true);
  const [multiSearch, setMultiSearch] = useState(false);
  const [multiGrpVal, setMultiGrpVal] = useState("");
  const [multiGrpData, setMultiGrpData] = useState("");
  const [multi, setMulti] = useState<string>("");
  const [me, setMe] = useState<any[]>([]);
  const [searchMedia, setSearchMedia] = useState<any[]>([]);
  const [searchAnno, setSearchAnno] = useState<any[]>([]);
  const [customQuestionModal, setCustomQuestionModal] = useState(false);
  const [customQues, setCustomQues] = useState(false);
  const [extraId, setExtraId] = useState("");
  const [openTab, setOpenTab] = useState(1);
  const [openKey, setOpenKey] = useState(null);
  const [openGroup, setOpenGroup] = useState(3);
  const [multiGroup, setMultiGroup] = useState<any>([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState<string>("");

  const toggle = () => setIsShown(!isShown);
  const { val } = useParams();
  const baseURL = localStorage.getItem("prod_url");
  const auth = localStorage.getItem("token");

  useEffect(() => {
    if (val !== undefined && val !== "") {
      const cur = val?.split("=");
      if (cur[0] !== "tag") {
        setCurMedia(val);
      } else if (cur[0] === "tag") {
        setAnnoVal("tag");
        setCurMedia(cur[1]);
      }
    }
    getUser();
  }, []);

  useEffect(() => {
    if (curMedia !== "") {
      mediaVal === "annotations" ? searchAnnotation() : searchBy();
    }
  }, [curMedia]);

  useEffect(() => {
    me.map((grp) => multiGroup.push({ value: grp.id, label: grp.name }));
  }, [me]);

  const searchAnnotation = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/annotate/?${annoVal}=${curMedia}`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };

    axios
      .request(options)
      .then((response) => {
        setSearchAnno(response.data.results);
      })
      .catch((e) => {
        console.log(e);
        setError(e?.message);
      });
  };

  console.log(
    `${baseURL}api/v1/archive/?${mediaVal}=${curMedia}${multiGrpVal}${multiGrpData}`
  );

  const searchBy = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/archive/?${mediaVal}=${curMedia}${multiGrpVal}${multiGrpData}`,
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
    };

    const AuthOptions = {
      method: "GET",
      url: `${baseURL}api/v1/archive/?${mediaVal}=${curMedia}${multiGrpVal}${multiGrpData}`,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: `Token ${auth}`,
      },
    };

    const searchCustomQuesOptions = {
      method: "GET",
      url: `${baseURL}api/v1/archive/`,
      params: {
        extra_question_id: extraId,
        extra_question_response: curMedia,
      },
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(
        auth ? (customQues ? searchCustomQuesOptions : AuthOptions) : options
      )
      .then((response) => {
        setSearchMedia(response.data.results);
      })
      .catch((e) => {
        console.log(e);
        setError(e?.message);
      });
  };

  const getUser = () => {
    const options = {
      method: "GET",
      url: `${baseURL}auth/users/me/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setMe(response.data.groups);
      })
      .catch((e) => {
        console.log(e);
        setError(e?.message);
      });
  };

  const handleSearch = ({
    target,
  }: React.ChangeEvent<HTMLInputElement>): void => {
    const searchWord: string = target.value;

    searchWord === "*" ? setCurMedia("") : setCurMedia(searchWord);
  };

  const handleFocus = () => {
    if (searchInput && searchInput.current) {
      searchInput.current.focus();
    }
  };

  const handleDropdown = (e) => {
    if (e) {
      switch (e) {
        case "publicGroup":
          setMultiGrpVal("");
          setMultiGrpData("");
          setMultiSearch(false);
          setGroupToggle(!groupToggle);
          break;

        case "myGroup":
          setMultiGrpVal("");
          setMultiGrpData("");
          setMultiSearch(false);
          setGroupToggle(!groupToggle);
          break;

        case "multipleGroup":
          setMultiGrpVal("&group=");
          setMultiSearch(!multiSearch);
          break;

        case "name":
          setMediaVal("name");
          setCurMedia("");
          setDisMedia(true);
          handleFocus();
          break;

        case "description":
          setMediaVal("description");
          setCurMedia("");
          setDisMedia(true);
          handleFocus();
          break;

        case "tag":
          setMediaVal("tag");
          setCurMedia("");
          setDisMedia(true);
          handleFocus();
          break;

        case "annotationName":
          setMediaVal("annotations");
          setAnnoVal("name");
          setCurMedia("");
          setDisMedia(false);
          handleFocus();
          break;

        case "annotationTag":
          setMediaVal("annotations");
          setAnnoVal("tag");
          setCurMedia("");
          setDisMedia(false);
          handleFocus();
          break;

        case "Search by Custom Question/s":
          setCustomQuestionModal(!customQuestionModal);
          setCustomQues(true);
          setMediaVal("");
          setDisMedia(false);
          handleFocus();
          break;

        default:
          setMultiGrpVal("");
          setMediaVal("search");
          setCurMedia("");
          setDisMedia(true);
          handleFocus();
          break;
      }
    }
  };

  const handleMulti = (multi) => {
    setMultiGrpData(multi.join(","));
  };

  const handleSelectAnnotation = (anno) => {
    handleDropdown(anno[1]);
  };

  const handleCustom = (id) => {
    setExtraId(id);
  };

  const DisplayCards = ({ media }: { media: any }) => {
    const navigate = useNavigate();

    return (
      <div className="grid lg:grid-cols-3 sm:grid-cols-1 md:grid-cols-2 py-5 px-5 gap-10 h-auto overflow-hidden">
        {media.results &&
          media.results.map((y, i) => (
            <>
              <div
                className="grid grid-cols-6 rounded-lg bg-white max-w-sm cursor-pointer h-full shadow-xl shadow-gray-500"
                key={i}
              >
                <div className="col-span-5">
                  <div className="relative flex flex-col min-w-0 break-words bg-white rounded mb-10 xl:mb-0 ">
                    <div className="flex-auto p-4">
                      <span className="font-semibold text-xl text-blueGray-700">
                        {y.name}
                      </span>

                      <p className="text-blueGray-400 uppercase font-bold text-xs h-auto min-h-24 max-h-28 line-clamp-5 hover:line-clamp-none mb-2">
                        {y.description}
                      </p>

                      {y.tags &&
                        y.tags.map((tag, key) => (
                          <div className="inline-flex gap-2 min-h-5" key={key}>
                            <div className="bg-gray-200 rounded-full px-2 py-1 text-sm font-semibold text-gray-700 mr-2">
                              {tag.name}
                            </div>
                          </div>
                        ))}
                    </div>
                  </div>
                </div>
                <div
                  className="col-span-6 p-3 w-max-content items-center px-2 text-sm font-bold min-h-fit max-h-16 h-12 mt-auto justify-center text-center bg-emerald-500 text-white hover:bg-opacity-70 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75"
                  onClick={() => {
                    navigate(`/collection/${y.group.id}/media/${y.uuid}`);
                  }}
                >
                  Open
                </div>
              </div>
            </>
          ))}
      </div>
    );
  };

  const searchOf = ["Name", "Description", "Tag"];
  const searchAcross = ["Public Group", "My Group", "Multiple Group"];
  const searchIn = ["Media", "Annotation"];

  const [selectedSearchOf, setSelectedSearchOf] = useState<string[]>([]);
  const [selectedSearchAcross, setSelectedSearchAcross] = useState<string[]>(
    []
  );
  const [selectedSearchIn, setSelectedSearchIn] = useState<string[]>([]);

  const displayRender = (labels: string[]) => labels[labels.length - 1];

  const filteredSearchOf = searchOf.filter(
    (o) => !selectedSearchOf.includes(o)
  );
  const filteredSearchAcross = searchAcross.filter(
    (o) => !selectedSearchAcross.includes(o)
  );
  const filteredSearchIn = searchIn.filter(
    (o) => !selectedSearchIn.includes(o)
  );

  if (error) {
    return <Loader error={error} />;
  }

  if (loading) {
    return <Loader />;
  }

  return (
    <>
      <div className="container ">
        <div className="grid sm:space-y-0 sm:grid sm:grid-cols-1 lg:grid-cols-8 pt-10">
          <div></div>
          <div className="col-span-6">
            <div className="md:w-auto w-full p-3 bg-white rounded-full">
              <div className="grid lg:grid-cols-6 sm:grid-cols-1 md:grid-cols-2">
                <div className="col-span-2 flex">
                  <IconButton
                    size="xs"
                    type="tertiary"
                    className="pt-2"
                    color="purple"
                  >
                    <MdSearch size={30} />
                  </IconButton>
                  <Input
                    type="text"
                    // ref={searchInput}
                    placeholder={`Search here ${
                      mediaVal === "search" ? "" : mediaVal
                    } ...`}
                    value={curMedia === "*" ? "*" : curMedia}
                    onChange={(e) => {
                      handleSearch(e);
                    }}
                    size="small"
                    autoFocus={true}
                    className="border-2 border-gray-200 rounded text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
                  />
                  <MdClose
                    size={35}
                    className="pt-4 cursor-pointer"
                    onClick={() => {
                      setMediaVal(disMedia ? "search" : "annotations");
                      setCurMedia("");
                      setDisMedia(disMedia ? true : false);
                      setIsShown(false);
                      setGroupToggle(false);
                      setMultiSearch(false);
                      setCustomQuestionModal(false);
                      handleFocus();
                    }}
                  />
                </div>
                <Space style={{ width: "100%" }} direction="vertical">
                  <Space wrap>
                    <Select
                      defaultValue={"all"}
                      className="py-2"
                      placeholder="Name / Description / Tag"
                      style={{ width: 180 }}
                      onChange={(e) => {
                        handleDropdown(e);
                      }}
                      options={[
                            { label: "All", value: "all" },
                            { label: "Name", value: "name" },
                            { label: "Description", value: "description" },
                            { label: "Tag", value: "tag" },
                      ]}
                    />
                  </Space>
                </Space>
                <Space style={{ width: "100%" }} direction="vertical">
                  <Space wrap>
                    <Select
                      defaultValue={auth ? "myGroup" : "publicGroup"}
                      style={{ width: 180 }}
                      className="py-2"
                      placeholder="Select Group"
                      onChange={(e) => {
                        handleDropdown(e);
                      }}
                      options={[
                            { label: "Public Group", value: "publicGroup" },
                            { label: "My Group", value: "myGroup" },
                            { label: "Multiple Group", value: "multipleGroup" },
                      ]}
                    />
                    {multiSearch && (
                      <Cascader
                        options={multiGroup}
                        defaultOpen={true}
                        placeholder="Select type"
                        className="py-2"
                        expandTrigger="hover"
                        displayRender={displayRender}
                        onChange={handleMulti}
                        multiple
                        maxTagCount="responsive"
                      />
                    )}
                  </Space>
                </Space>

                <Space style={{ width: "100%" }} direction="vertical">
                  <Cascader
                    options={[
                      {
                        value: "media",
                        label: "Media",
                      },
                      {
                        label: "Annotation",
                        value: "annotation",
                        children: [
                          {
                            value: "annotationName",
                            label: "Annotation Name",
                          },
                          {
                            value: "annotationTag",
                            label: "Annotation Tag",
                          },
                        ],
                      },
                    ]}
                    defaultValue={["media"]}
                    placeholder="Select type"
                    className="py-2"
                    expandTrigger="hover"
                    displayRender={displayRender}
                    onChange={handleSelectAnnotation}
                  />
                </Space>
                <IconButton
                  type="tertiary"
                  size="xs"
                  className="p-0 px-3 py-3"
                  onClick={() => {
                    toggle();
                    handleFocus();
                  }}
                >
                  <MdOutlineReportProblem size={20} />
                </IconButton>
              </div>

              {isShown && (
                <div className="col-span-3">
                  <a href="https://gitlab.com/servelots/papad/papad-api/-/issues/2">
                    <li>Put * to search any</li>
                    <li>
                      Custom Question will work only with users of POSTGREL DB
                      Users
                    </li>
                    <li>Follow the link for Search based development</li>
                  </a>
                </div>
              )}
            </div>
          </div>

          <div className="col-span-8 p-5">
            <Row gutter={[24, 16]}>
              {disMedia
                ? searchMedia.map((media, i) => (
                    <Col xs={{ span: 24 }} lg={{ span: 8 }} key={i}>
                      <CardLayout collection={media} />
                    </Col>
                  ))
                : searchAnno.map((anno, i) => (
                    <Col xs={{ span: 24 }} lg={{ span: 8 }} key={i}>
                      <AnnotationCard
                        annotationId={anno.uuid}
                        annoText={anno.annotation_text}
                        tags={anno.tags}
                        startTime={secToTimestamp(
                          anno.media_target.split("=").join().split(",")[1]
                        )}
                        endTime={secToTimestamp(
                          anno.media_target.split("=").join().split(",")[2]
                        )}
                        annoImage={anno.annotation_image}
                        createdDate={anno.created_at}
                        createdBy={anno.created_by}
                        updatedDate={anno.updated_at}
                      />
                    </Col>
                  ))}
            </Row>
          </div>

          {customQuestionModal && (
            <DialogModal
              open={customQuestionModal}
              onClose={() => setCustomQuestionModal(!customQuestionModal)}
              title={"Select custom question to be searched"}
            >
              <CustomQuestionModal
                onClose={() => setCustomQuestionModal(!customQuestionModal)}
                groupData={me}
                onSelectCQ={handleCustom}
              />
            </DialogModal>
          )}
        </div>
      </div>
    </>
  );
};

export default Search;
