import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useRecoilValue } from "recoil";
import { Button } from "../../Components";
import { prodState, tokenState } from "../AuthState/AuthState";
import { useNavigate } from "react-router-dom";

const Requests = () => {
  const [requests, setRequests] = useState<any>([]);
  const [page, setPage] = useState({ previous: null, next: null });
  const [url, setUrl] = useState<any>("");

  const auth = localStorage.getItem("token");
  const { id } = useParams();
  const baseURL = localStorage.getItem("prod_url");
  const navigate = useNavigate();
  useEffect(() => {
    getEI();
  }, [url]);

  useEffect(() => {
    if (!localStorage.getItem("token")) {
      navigate("/");
    }
  }, []);
  useEffect(() => {
    id && getEIbyId(id);
  }, [id]);

  const getEI = () => {
    const options = {
      method: "GET",
      url: url ? url : `${baseURL}api/v1/myierequests/?page_size=30`,

      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setRequests(response.data.results);
        setPage({ next: response.data.next, previous: response.data.previous });
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const getEIbyId = (id: string) => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/export/${id}/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setRequests(response.data);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const handleTime = (time: any) => {
    if (time !== null) {
      const dateTime = time.split("T");
      const reqTime = dateTime[1].split("+")[0];

      return `${dateTime[0]} at ${reqTime}`;
    }
  };

  const Pagination = () => {
    return (
      <div className="flex justify-center pb-10">
        {page.next === null && page.previous === null ? null : (
          <>
            <Button
              size="sm"
              type="tertiary"
              className="m-5 font-semibold border-2 w-fit rounded-lg shadow-lg border-blue-600 -blue-600 disabled:opacity-25 focus:outline-none"
              onClick={(e) => {
                setUrl(page.previous);
              }}
              disabled={page.previous === null ? true : false}
            >
              Previous
            </Button>
            <Button
              size="sm"
              type="tertiary"
              className="p-2 m-5 font-semibold border-2 w-min rounded-lg shadow-lg border-blue-600 -blue-600 disabled:opacity-25 focus:outline-none"
              onClick={(e) => {
                setUrl(page.next);
              }}
              disabled={page.next === null ? true : false}
            >
              Next
            </Button>
          </>
        )}
      </div>
    );
  };

  console.log("====================================");
  console.log(requests.length);
  console.log("====================================");

  const Table = () => {
    return (
      <table className="min-w-full border-collapse block md:table">
        <thead className="block md:table-header-group">
          <tr className="border border-grey-500 md:border-none block md:table-row absolute -top-full md:top-auto -left-full md:left-auto  md:relative ">
            <th className="bg-blue-500 p-2 text-white font-bold md:border md:border-grey-500 text-left block md:table-cell">
              Request ID
            </th>
            <th className="bg-blue-500 p-2 text-white font-bold md:border md:border-grey-500 text-left block md:table-cell">
              Request Type
            </th>
            <th className="bg-blue-500 p-2 text-white font-bold md:border md:border-grey-500 text-left block md:table-cell">
              Requested At
            </th>
            <th className="bg-blue-500 p-2 text-white font-bold md:border md:border-grey-500 text-left block md:table-cell">
              Authorised
            </th>
            <th className="bg-blue-500 p-2 text-white font-bold md:border md:border-grey-500 text-left block md:table-cell">
              Detail
            </th>
            <th className="bg-blue-500 p-2 text-white font-bold md:border md:border-grey-500 text-left block md:table-cell">
              Completed
            </th>
            <th className="bg-blue-500 p-2 text-white font-bold md:border md:border-grey-500 text-left block md:table-cell">
              Completed At
            </th>
            <th className="bg-blue-500 p-2 text-white font-bold md:border md:border-grey-500 text-left block md:table-cell">
              Requested File
            </th>
          </tr>
        </thead>
        {requests?.map((x, i) => (
          <tbody className="block md:table-row-group" key={i}>
            <tr className="bg-gray-300 border border-grey-500 md:border-none block md:table-row">
              <td className="p-2 md:border md:border-grey-500 text-left block md:table-cell">
                <span className="inline-block w-1/3 md:hidden font-bold">
                  Request ID
                </span>
                {x.request_id}
              </td>
              <td className="p-2 md:border md:border-grey-500 text-left block md:table-cell">
                <span className="inline-block w-1/3 md:hidden font-bold capitalize">
                  Request Type
                </span>
                {x.ie_metadata.type} {x.request_type}
              </td>
              <td className="p-2 md:border md:border-grey-500 text-left block md:table-cell">
                <span className="inline-block w-1/3 md:hidden font-bold">
                  Requested At
                </span>
                {handleTime(x.requested_at)}
              </td>
              <td className="p-2 md:border md:border-grey-500 text-left block md:table-cell">
                <span className="inline-block w-1/3 md:hidden font-bold">
                  Authorised
                </span>
                {x.is_authorized === true ? "Yes" : "No"}
              </td>
              <td className="p-2 md:border md:border-grey-500 text-left block md:table-cell">
                <span className="inline-block w-1/3 md:hidden font-bold">
                  Detail
                </span>
                {x.detail}
              </td>
              <td className="p-2 md:border md:border-grey-500 text-left block md:table-cell">
                <span className="inline-block w-1/3 md:hidden font-bold">
                  Completed
                </span>
                {x.is_complete === true ? "Yes" : "No"}
              </td>
              <td className="p-2 md:border md:border-grey-500 text-left block md:table-cell">
                <span className="inline-block w-1/3 md:hidden font-bold">
                  Completed At
                </span>
                {handleTime(x.completed_at)}
              </td>
              <td className="p-2 md:border md:border-grey-500 text-left block md:table-cell">
                <span className="inline-block w-1/3 md:hidden font-bold">
                  Download
                </span>
                <a href={x.requested_file} className="text-blue-600">
                  {x.requested_file}
                </a>
              </td>
            </tr>
          </tbody>
        ))}
      </table>
    );
  };

  const NoData = () => {
    return (
      <div className="col-span-3 flex gap-2 space-between pt-10 item-center justify-center">
        <div>
          😮 Oops ! There are no request made yet on Papad. Explore export
          groups or archive or annotation other
        </div>
      </div>
    );
  };

  return (
    <div className="container pt-10">
      <div className="font-bold text-3xl pb-5">Your Requests</div>
      {requests.length !== 0 ? <Table /> : <NoData />}
      <Pagination />
    </div>
  );
};

export default Requests;
