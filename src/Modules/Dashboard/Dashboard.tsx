import React, { useEffect, useRef, useState } from "react";
import { MdEdit, MdGroups } from "react-icons/md";
import { BiImport } from "react-icons/bi";
import {BsPlusSquareDotted} from "react-icons/bs"
import { useNavigate } from "react-router-dom";
import axios from "axios";
import Display from "./Display";
import {
  Button,
  DialogModal,
  ImportModal,
  Loader,
  CollectionCard,
} from "../../Components";
import CreateCollection from "../Collection/CreateCollection";
import { loginState, prodState, tokenState } from "../AuthState/AuthState";
import { useRecoilState, useRecoilValue } from "recoil";
import { getBaseURL, getData } from "../../api";
import TagButton from "../../Components/Button/TagButton";
import { Card, Col, Dropdown, MenuProps, message, Row, Select, Space } from "antd";
import CardLayout from "../../Components/Cards/CardLayout";
import { DownOutlined } from '@ant-design/icons';
import AntButton from "antd/es/button";

export interface IPage {
  next?: boolean;
  previous?: boolean;
}

const Dashboard = () => {
  const [recordings, setRecordings] = useState<any[]>([]);
  const [curMedia, setCurMedia] = useState<string>("");
  const [tags, setTags] = useState<any>([]);
  const [display, setDisplay] = useState(true);
  const [mediaVal, setMediaVal] = useState("search");
  const [searchMedia, setSearchMedia] = useState<any[]>([]);
  const [page, setPage] = useState({ previous: null, next: null });
  const [paginate, setPaginate] = useState<IPage>({
    previous: false,
    next: false,
  });
  const [isShown, setIsShown] = useState(false);
  const [me, setMe] = useState<any[]>([]);
  const searchInput = useRef<HTMLInputElement>(null);
  const [emptyState, setEmptyState] = useState(false);
  const [error, setError] = useState<any>([]);
  const [multiGrpVal, setMultiGrpVal] = useState("");
  const [multiGrpData, setMultiGrpData] = useState("");
  const [groupToggle, setGroupToggle] = useState(false);
  const [multi, setMulti] = useState<string>("");
  const [multiSearchModal, setMultiSearchModal] = useState(false);
  const [group, setGroup] = useState("");
  const [privateGroups, setPrivateGroups] = useState<any[]>([]);
  const [loading, setLoading] = useState(true);
  const [createColModal, setCreateColModal] = useState(false);

  const toggle = () => setIsShown(!isShown);
  const navigate = useNavigate();
  const auth = localStorage.getItem("token");
  const baseURL = localStorage.getItem("prod_url");
  const [login, setLogin] = useRecoilState(loginState);
  const [token, setToken] = useRecoilState(tokenState);

  useEffect(() => {
    getRecordings();
    getAllTags();
    auth && getUser();
  }, []);

  useEffect(() => {
    mediaVal === "annotations" ? searchAnnotation() : searchBy();
  }, [curMedia]);

  const getRecordings = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/group/`,
      headers: {
        "Content-Type": "form-data",
      },
    };

    const AuthOptions = {
      method: "GET",
      url: `${baseURL}api/v1/group/`,
      headers: {
        "Content-Type": "form-data",
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(auth ? AuthOptions : options)
      .then((response) => {
        setRecordings(response.data.results);
        response.data.count === 0 && setEmptyState(true);
        setPage({ next: response.data.next, previous: response.data.previous });
        response.data.next === null && setPaginate({ next: true });
        response.data.previous === null && setPaginate({ previous: true });
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getAllTags = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/tags/?page_size=20`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };

    axios
      .request(options)
      .then((response) => {
        setTags(response.data);
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const searchBy = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/archive/?${mediaVal}=${curMedia}${multiGrpVal}${multiGrpData}`,
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      data: {},
    };

    const AuthOptions = {
      method: "GET",
      url: `${baseURL}api/v1/archive/?${mediaVal}=${curMedia}${multiGrpVal}${multiGrpData}`,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        Authorization: `Token ${auth}`,
      },
      data: {},
    };

    axios
      .request(auth ? AuthOptions : options)
      .then((response) => {
        setSearchMedia(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const searchAnnotation = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/annotate/?name=${curMedia}`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };

    axios
      .request(options)
      .then((response) => {
        setSearchMedia(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getPage = (id) => {
    const options = {
      method: "GET",
      url: id,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };

    axios
      .request(options)
      .then((response) => {
        setRecordings(response.data.results);
        setPage({ next: response.data.next, previous: response.data.previous });
        response.data.previous === null && setPaginate({ previous: true });
        response.data.next === null
          ? setPaginate({ next: true })
          : setPaginate({ next: false });
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getUser = () => {
    const options = {
      method: "GET",
      url: `${baseURL}auth/users/me/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setMe(response.data);
        setPrivateGroups(response.data.groups);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const GetTags = () => {
    return (
      <div>
        {tags.map((tag, i) => {
          if (tag.name !== "" && tag.count !== 0) {
            return (
              <div className="inline-flex m-1" 
                   key={i}
                   onClick={() => {
                    navigate(`/search/tag=${tag.name}`);
                  }}
              >
                <TagButton
                  color="rgb(85, 172, 238)"
                  tagName={tag.name + " " + tag.count}
                />
              </div>
            );
          }
          return false;
        })}
      </div>
    );
  };

  const updateGroups = () => {
    getRecordings();
  };

  const handleDisplay = (id) => {
    if (id === "next") {
      page.next !== null && getPage(page.next);
    } else if (id === "prev") {
      page.previous === null
        ? setPaginate({ previous: true })
        : getPage(page.previous);
    }
  };

  const Pagination = () => {
    return (
      <div className="flex justify-center">
        {page.next === null && page.previous === null ? null : (
          <>
            <Button
              size="sm"
              type="tertiary"
              className="m-5 font-semibold border-2 w-fit rounded-lg shadow-lg border-blue-600 -blue-600 disabled:opacity-25 focus:outline-none"
              onClick={(e) => {
                handleDisplay("prev");
              }}
              disabled={paginate.previous}
            >
              Previous
            </Button>
            <Button
              size="sm"
              type="tertiary"
              className="m-5 font-semibold border-2 w-min rounded-lg shadow-lg border-blue-600 -blue-600 disabled:opacity-25 focus:outline-none"
              onClick={(e) => {
                handleDisplay("next");
              }}
              disabled={paginate.next}
            >
              Next
            </Button>
          </>
        )}
      </div>
    );
  };

  const NoData = () => {
    return (
      <div className="font-normal text-lg">
        😮 Oops ! There are no media part of this collection on Papad. Explore
        other public {auth && "/ private "}
        collections or <AntButton type="text" className="border-blue-500 border-2 rounded-full" onClick={()=> auth ? setCreateColModal(true) : navigate("/auth/login")}> create your own </AntButton>
      </div>
    );
  };
  const CollectionHead = () => {
    const [importModal, setImportModal] = useState(false);
    return (
      <div className="flex flex-col gap-10">
        <div className="font-bold text-3xl">Start with a Collection</div>
        <div className="flex flex-row w-full pb-10 gap-10">
          <Button
            size="sm"
            onClick={() => setCreateColModal(true)}
            className="flex flex-row lg:gap-3 md:gap-2 sm:gap-0 items-center px-2 py-6 mr-0 shadow-black shadow-2xl"
            type="primary"
          >
            <MdGroups size={25} fontStyle="bold" />
            Create a collection
          </Button>
          <Button
            size="sm"
            onClick={() => setImportModal(true)}
            className="flex flex-row  lg:gap-3 md:gap-2 sm:gap-0 items-center px-2 py-6 mr-0 shadow-black shadow-2xl"
            type="primary"
          >
            <BiImport size={25} fontStyle="bold" />
            Import a collection
          </Button>
          {importModal && (
            <DialogModal
              open={importModal}
              onClose={() => setImportModal(false)}
              title={"Import a Collection"}
            >
              <ImportModal
                type="group"
                onClose={() => setImportModal(false)}
                updateGroups={updateGroups}
              />
            </DialogModal>
          )}
          
        </div>
      </div>
    );
  };
  
  const Cards = () => {
    const [title, setTitle] = useState<any>("All Collections");

    const AntDropdown = () => {
      return (
        <>
        <Space direction="vertical">
            <Select
              defaultValue={title}
              style={{ width: 200 }}
              className="w-min"
              onChange={(e) => {
                setTitle(e);
              }}
              options={[
                    { label: "All Collection", value: "All Collections" },
                    { label: "Public Collections", value: "Public Collections" },
                    auth && { label: "My Collections", value: "My Collections" },
              ]}
            />
        </Space>
        </>
      )
    }

    const cardData = {
      "title" : title,
      "dropDown" : <AntDropdown/>
    }

    const CollectionsCard = () =>{
      return (
        <Row gutter={[24, 16]}>
                      {(
                        <>
                        {auth && privateGroups?.length === 0 ? (
                          <Col span={24} >
                            <NoData />
                          </Col>
                        ) : (
                          <>
                            <Col xs={{ span: 24 }} lg={{ span: 8}}  className="cursor-pointer">
                              <Card style={{ borderColor: "rgb(179, 222, 236)" }} onClick={()=> auth ? setCreateColModal(true) : navigate("/auth/login")}>
                                <div className="grid place-items-center justify-center h-[13.2rem]" >
                                    <BsPlusSquareDotted size={60} style={{textAlign: "center"}}/> 
                                    <span>Create a Collection</span>
                                </div>
                              </Card>
                            </Col>
                            {( auth && (title === "My Collections" || title === "All Collections")) && privateGroups?.map((grp, i) => (
                              <Col xs={{ span: 24 }} lg={{ span: 8}} key={i}>
                                  <CardLayout collection={grp}/>
                              </Col>
                            ))}
                          </>
                        )}
                        </>
                      )}

                      {( (title === "Public Collections" || title === "All Collections")) && recordings?.map((grp, i) => (
                        <Col xs={{ span: 24 }} lg={{ span: 8}} key={i}>
                            <CardLayout collection={grp}/>
                        </Col>
                      ))}

                      {createColModal && (
                        <DialogModal
                          open={createColModal}
                          onClose={() => setCreateColModal(false)}
                          title={"Create a Collection"}
                        >
                          <CreateCollection
                            onClose={() => setCreateColModal(false)}
                            path="add"
                          />
                        </DialogModal>
                      )}
                    </Row>
                    
      )
    }
    return (
      <>
        <div className="mx-4">
          <Row>
            <Col span={24}>
              
              {auth ? 
                  <Card title={cardData?.title} extra={cardData?.dropDown}>
                    <CollectionsCard/>
                    {recordings.length !== 0 && <Pagination />}
                  </Card>
                 : <Card >
                    <CollectionsCard/>
                    {recordings.length !== 0 && <Pagination />}
                  </Card>}

            </Col>
          </Row>
          <div className="mt-4">
            <Row>
              <Col>
                  <Card>
                    {tags && <GetTags />}
                  </Card>
              </Col>
            </Row>
          </div>
        </div>
        
        {/* <div className="grid sm:space-y-0 sm:grid sm:grid-cols-1 lg:grid-cols-8 gap-2 w-full h-auto">
          <div className="col-span-6 h-1/4 rounded-lg place-items-center justify-center">
            {display ? (
              <div className="p-5 gap-y-5">
                
              </div>
            ) : (
              <Display data={searchMedia} id={mediaVal} />
            )}
          </div>
        </div> */}
      </>
    );
  };

  if (loading) {
    return <Loader />;
  }

  return (
    <div className="relative container w-full p-2 gap-2 max-h-fit">
      <Cards />
    </div>
  );
};

export default Dashboard;
