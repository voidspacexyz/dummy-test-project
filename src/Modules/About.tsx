import React from "react";

const About: React.FC = (props) => {
  const Home = () => {
    return (
      <div className="flex flex-col w-full text-gray-700 container ">
        <div className="w-auto px-5 py-2 pt-20 text-align-center text-center">
          <p className="font-medium text-5xl text-center text-brand-dark leading-snug pb-5">
            PAPAD
            <p className="font-light text-3xl text-center text-brand-dark leading-snug">
              Media Fragment Annotator
            </p>
          </p>
          <div className="w-auto m-5 p-5">
            <p className="font-thin text-2xl text-center text-brand-dark leading-snug flex-wrap gap-2">
              Papad is a project of Janastu collective – who explore inclusive
              open source technology solutions. Using Papad, communities can
              archive media, annotate fragments, discover relationships and
              share their knowledge.
            </p>
            <p className="font-thin text-2xl text-center text-brand-dark leading-snug flex-wrap gap-2">
              Follow Papad development & other projects at
              <span className="mx-1 ">
                <a href="open.janastu.org/projects" className="underline">
                  open.janastu.org/projects
                </a>
                .
              </span>
            </p>
          </div>

          <div className="w-auto m-5 p-5">
            <p className="font-thin text-2xl text-center text-brand-dark leading-snug gap-2">
              Janastu’s online instance is at
              <span className="mx-1 underline">
                <a href="stories.janastu.org">stories.janastu.org</a>
              </span>
              and offline one at
              <span className="mx-1 underline">
                <a href="papad.iruway.in">papad.iruway.in</a>
              </span>
              (reachable from COWMesh).
            </p>
            <p className="font-thin text-2xl text-center text-brand-dark leading-snug gap-2">
              Typical use cases are: offline archives, oral histories and media
              libraries.
            </p>
          </div>
        </div>
      </div>
    );
  };

  return (
    <>
      <Home />
    </>
  );
};

export default About;
