import { Tooltip } from "antd";
import axios from "axios";
import React, { useCallback, useEffect, useRef, useState } from "react";
import { MdClose } from "react-icons/md";
import { useNavigate, useParams } from "react-router-dom";
import { Button, IconButton, Toggle } from "../../Components";

interface userData {
  id: string;
  first_name: string;
  username: string;
}

const CreateCollection = ({ onClose, path }: { onClose?; path?: string }) => {
  const [name, setName] = useState(undefined);
  const [description, setDescription] = useState("");
  const [user, setUser] = useState("");
  const [waitDelete, setWaitDelete] = useState("");
  const [curMedia, setCurMedia] = useState<string>("");
  const [searchMedia, setSearchMedia] = useState<any[]>([]);
  const [me, setMe] = useState<any[]>([]);
  const [userData, setUserData] = useState<userData[]>([]);
  const [users, setUsers] = useState<string>();
  const [grpUsers, setGrpUsers] = useState<any[]>([]);
  const [extra, setExtra] = useState<any>([]);
  const [error, setError] = useState<any>({});
  const [moreError, setMoreError] = useState(false);

  const [privateGrp, setPrivateGrp] = useState(false);
  const [loading, setLoading] = useState(false);
  const [defaultField, setDefaultField] = useState(false);
  const [defaultText, setDefaultText] = useState("");
  const [customText, setCustomText] = useState("");
  const [customType, setCustomType] = useState("Text");
  const [customMandatory, setCustomMandatory] = useState(false);
  const [checked, setChecked] = useState(true);

  const auth = localStorage.getItem("token");
  const navigate = useNavigate();
  const { id } = useParams();
  const fieldInput = useRef<HTMLInputElement>(null);
  const baseURL = localStorage.getItem("prod_url");
  const maxWords = 250;
  const [{ content, wordCount }, setContent] = React.useState({
    content: description,
    wordCount: 0,
  });

  useEffect(() => {
    getUser();
    id !== "" && getGroup();
  }, []);

  useEffect(() => {
    curMedia !== "" && searchBy();
  }, [curMedia]);

  useEffect(() => {
    users && setUser(users);
  }, [users]);

  const searchBy = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/users/search/`,
      params: { search: curMedia },
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setSearchMedia(response.data);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getUser = () => {
    const options = {
      method: "GET",
      url: `${baseURL}auth/users/me/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setMe((prev) => [...prev, response.data]);
        setUser(response.data.id);
        setUsers(response.data.id);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const postRecord = (e) => {
    e.preventDefault();

    const formData = new FormData();
    name !== undefined && formData.append("name", name);
    formData.append("description", description);
    formData.append("users", user);
    formData.append("is_public", privateGrp ? "False" : "True");
    extra.length !== 0 &&
      formData.append("group_extra_questions", `{"extra":[${extra}]}`);
    path && id && formData.append("group", id);
    path &&
      path === "keep-data" &&
      formData.append("delete_wait_for", waitDelete);
    path === "add-user" && userData.filter((val) => val.id);

    const form = new FormData();
    form.append("question", customText);
    form.append("mandatory", customMandatory ? "True" : "False");
    form.append("type", customType);
    form.append("default_value", defaultText);
    form.append("add_default_value", defaultField ? "True" : "False");

    const options = {
      method: "POST",
      url: `${baseURL}api/v1/group/`,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Token ${auth}`,
      },
      data: formData,
    };

    const editOptions = {
      method: "PUT",
      url: `${baseURL}api/v1/group/${id}/`,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Token ${auth}`,
      },
      data: formData,
    };

    const customOptions = {
      method: "PUT",
      url: `${baseURL}api/v1/group/add_question/${id}/`,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Token ${auth}`,
      },
      data: form,
    };

    axios
      .request(
        path === "custom-question"
          ? customOptions
          : path !== "add"
          ? editOptions
          : options
      )
      .then((response) => {
        console.log(response);
        id
          ? navigate(`/collection/${id}`)
          : navigate(`/collection/${response?.data?.id}`);
        setLoading(false);
        onClose();
      })
      .catch((error) => {
        console.error(error);
        setError(error);
        setLoading(false);
      });
  };

  const handleSearch = ({
    target,
  }: React.ChangeEvent<HTMLInputElement>): void => {
    const searchWord: string = target.value;
    setCurMedia(searchWord);
  };

  const getGroup = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/group/${id}/`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setGrpUsers(response.data.users);
        setName(response.data.name);
        setDescription(response.data.description);
        setFormattedContent(response.data.description);
        setPrivateGrp(response.data?.is_public === false ? true : false);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const setFormattedContent = useCallback(
    (text) => {
      let words = text.split(" ").filter(Boolean);
      if (words.length > maxWords) {
        setContent({
          content: words.slice(0, maxWords).join(" "),
          wordCount: maxWords,
        });
      } else {
        setContent({ content: text, wordCount: words.length });
      }
    },
    [maxWords, setContent]
  );

  const DisplayUsers = ({ data }: { data: any }) => {
    return (
      <div className="grid lg:grid-cols-4 sm:grid-cols-2 gap-5 p-3 h-1/5 rounded-lg border overflow-y-scroll">
        {data.results &&
          data.results.map((user, key) => (
            <div
              className="block rounded-lg shadow-xl bg-gray-100 max-w-sm overflow-auto text-white active:bg-gray-200 hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full"
              key={key}
            >
              <div className="p-6 grid grid-cols-4">
                <h5 className="col-span-3 text-gray-900 text-xl font-medium mb-2">
                  {user.first_name} {user.last_name}
                </h5>
                <p className="col-span-4 text-gray-700 text-base mb-5">
                  {user.username}
                </p>
              </div>
              <div className="px-4 pb-1">
                <Button
                  size="sm"
                  type="secondary"
                  onClick={() => {
                    setUserData((prev) => [
                      ...prev,
                      {
                        id: user.id,
                        first_name: user.first_name,
                        username: user.username,
                      },
                    ]);
                    setUsers(`${users},${user.id}`);
                  }}
                >
                  ADD
                </Button>
              </div>
            </div>
          ))}
      </div>
    );
  };

  const DisplayAddedUsers = ({
    data,
    admin,
    color,
  }: {
    data: any;
    admin: boolean;
    color?: string;
  }) => {
    return (
      <div className="inline-flex">
        {data &&
          data.map((user, key) => (
            <div className="grid grid-cols-4 gap-5 w-fit" key={key}>
              <div
                className={`group col-span-4 inline-flex w-10 h-10 border-4 ${
                  admin ? "border-blue-500" : "border-emerald-500"
                } rounded-full gap-2`}
              >
                <div className="w-10 h-10 items-center justify-center p-1 pl-2.5 font-bold uppercase mr-1 mb-1">
                  {user.first_name.slice(0, 1)}
                </div>
              </div>
            </div>
          ))}
      </div>
    );
  };

  const AddUsers = () => {
    return (
      <div className="lg:p-5 sm:p-1 rounded-t">
        <div className="inline-flex">
          <DisplayAddedUsers data={grpUsers} admin={true} />
          <DisplayAddedUsers data={userData} admin={false} />
        </div>
        <div className="md:flex md:items-center m-5 mb-6">
          <div className="md:w-1/3">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
              Search Users
            </label>
          </div>
          <div className="md:w-2/3">
            <input
              className="bg-gray-200 border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
              type="text"
              placeholder={`Search users here   ...`}
              value={curMedia}
              ref={fieldInput}
              autoFocus={true}
              onChange={(e) => {
                handleSearch(e);
              }}
            />
          </div>
        </div>
        {searchMedia && <DisplayUsers data={searchMedia} />}
      </div>
    );
  };

  const CustomQuestion = () => {
    return (
      <div className="lg:p-5 sm:p-1 rounded-t">
        <div className="md:flex md:items-center m-5 mb-6">
          <div className="md:w-1/3">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
              Field
            </label>
          </div>
          <div className="md:w-2/3">
            <input
              className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
              type="text"
              ref={fieldInput}
              maxLength={300}
              value={customText}
              autoFocus={true}
              onChange={(e) => {
                setCustomText(e.target.value);
              }}
            />
          </div>
        </div>
        <div className="md:flex md:items-center m-5 mb-6">
          <div className="md:w-1/3">
            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
              Type
            </label>
          </div>
          <div className="md:w-2/3">
            <div className="flex justify-center gap-5">
              <input
                className="h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                type="checkbox"
                value=""
                id="text"
                checked={checked}
                onClick={() => {
                  setChecked(!checked);
                  setCustomType("Text");
                }}
                readOnly
              />
              <label className="form-check-label inline-block text-gray-800">
                Text
              </label>
              <input
                className="h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                type="checkbox"
                value=""
                id="number"
                checked={!checked}
                onClick={() => {
                  setChecked(!checked);
                  setCustomType("Number");
                }}
                readOnly
              />
              <label className="form-check-label inline-block text-gray-800">
                Number
              </label>
            </div>
          </div>
        </div>
        <div className="flex gap-5 justify-evenly">
          <Toggle
            onClick={() => {
              setCustomMandatory(!customMandatory);
            }}
            checked={customMandatory}
            title={"Mandatory"}
          />

          <Toggle
            onClick={() => {
              setDefaultField(!defaultField);
            }}
            checked={defaultField}
            title={"Default value"}
          />
        </div>
        {defaultField && (
          <div className="md:flex md:items-center m-5 mb-6">
            <div className="md:w-1/3">
              <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                Default Value
              </label>
            </div>
            <div className="md:w-2/3">
              <input
                className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                type={customType}
                maxLength={300}
                value={defaultText}
                ref={fieldInput}
                autoFocus={true}
                onChange={(e) => {
                  setDefaultText(e.target.value);
                }}
              />
            </div>
          </div>
        )}
      </div>
    );
  };

  const ErrorFeedback = () => {
    return (
      <>
        {Object.keys(error).length !== 0 && (
          <div className="flex flex-col items-center justify-center p-5">
            {error?.response?.data?.detail !== undefined ? (
              <h5 className="text-xl font-medium text-red-600">
                {error.response.data.detail}
              </h5>
            ) : (
              <h5 className="text-xl font-medium text-red-600">
                {error?.message}
              </h5>
            )}
            <span>
              <Button
                size="sm"
                type="tertiary"
                className="cursor-pointer lowercase"
                onClick={(e) => {
                  setError(JSON.stringify(error, null, 2));
                  setMoreError(!moreError);
                }}
                disabled={moreError}
              >
                ...view more
              </Button>
              {/* <Tooltip title={"Close detail error"}>
                <IconButton
                  type="tertiary"
                  size="xs"
                  className="float-right font-extrabold"
                  onClick={onClose}
                  color="black"
                >
                  <MdClose size={20} />
                </IconButton>
              </Tooltip> */}
            </span>

            {moreError && <pre className="w-full ">{error}</pre>}
          </div>
        )}
      </>
    );
  };

  const FormFooter = () => {
    return (
      <div className="text-center mt-6 p-5 border-t">
        <Button
          size="sm"
          className="w-full"
          type="secondary"
          disabled={loading}
          onClick={(e) => {
            setMoreError(false);
            postRecord(e);
            setLoading(true);
          }}
        >
          Submit
        </Button>
        <Button
          size="sm"
          type="tertiary"
          color="red"
          className="uppercase"
          onClick={() => onClose()}
        >
          Cancel
        </Button>
      </div>
    );
  };

  return (
    <div className="item-align-center">
      <div className="flex justify-center">
        <div className="">
          <form onSubmit={(e) => postRecord(e)}>
            {path === "custom-question" && <CustomQuestion />}
            {path === "add-user" && <AddUsers />}
            {path === "keep-data" && (
              <div className="p-1 pt-2">
                <div className="md:flex md:items-center m-5 mb-6">
                  <div className="md:w-1/3">
                    <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                      Keep deleted data
                      <p className="block text-gray-500 md:text-left">
                        (in days, set to 0 for deleting immediately)
                      </p>
                    </label>
                  </div>
                  <div className="md:w-2/3">
                    <input
                      className="bg-gray-200 border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
                      type="number"
                      placeholder={`how long you wanna keep deleted data ...`}
                      value={waitDelete}
                      minLength={1}
                      ref={fieldInput}
                      autoFocus={true}
                      onChange={(e) => {
                        setWaitDelete(e.target.value);
                      }}
                    />
                  </div>
                </div>
              </div>
            )}
            {path === "add" && (
              <div className="p-5 rounded-t">
                <div className="md:flex md:items-center m-5 mb-6">
                  <div className="md:w-1/3">
                    <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                      Name
                    </label>
                  </div>
                  <div className="md:w-2/3 h-auto">
                    <input
                      className="bg-gray-200 border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
                      maxLength={300}
                      type="text"
                      value={name}
                      ref={fieldInput}
                      autoFocus={true}
                      onChange={(e) => {
                        setName(e.target.value);
                      }}
                      required
                    />
                  </div>
                </div>
                <div className="md:flex md:items-center m-5 mb-6">
                  <div className="md:w-1/3">
                    <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                      Description
                    </label>
                  </div>
                  <div className="md:w-2/3 relative">
                    <textarea
                      className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                      rows={5}
                      value={content}
                      onChange={(e) => {
                        setFormattedContent(e.target.value);
                        setDescription(e.target.value);
                      }}
                      required
                    />
                    <span className="font-normal text-xs text-blueGray-400 absolute bottom-0 right-0 p-2">
                      {wordCount}/{maxWords}
                    </span>
                  </div>
                </div>
                <Toggle
                  onClick={() => {
                    setPrivateGrp(!privateGrp);
                  }}
                  checked={privateGrp}
                  title={"Private"}
                />
              </div>
            )}
            {path === "edit" && (
              <div className="p-5 rounded-t">
                <div className="md:flex md:items-center m-5 mb-6">
                  <div className="md:w-1/3">
                    <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                      Name
                    </label>
                  </div>
                  <div className="md:w-2/3 h-auto">
                    <input
                      className="bg-gray-200 border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
                      maxLength={300}
                      type="text"
                      value={name}
                      ref={fieldInput}
                      autoFocus={true}
                      onChange={(e) => {
                        setName(e.target.value);
                      }}
                      required
                    />
                  </div>
                </div>
                <div className="md:flex md:items-center m-5 mb-6">
                  <div className="md:w-1/3">
                    <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                      Description
                    </label>
                  </div>
                  <div className="md:w-2/3 relative">
                    <textarea
                      className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                      rows={5}
                      value={content}
                      onChange={(e) => {
                        setFormattedContent(e.target.value);
                        setDescription(e.target.value);
                      }}
                      required
                    />
                    <span className="font-normal text-xs text-blueGray-400 absolute bottom-0 right-0 p-2">
                      {wordCount}/{maxWords}
                    </span>
                  </div>
                </div>
                <Toggle
                  onClick={() => {
                    setPrivateGrp(!privateGrp);
                  }}
                  checked={privateGrp}
                  title={"Private"}
                />
              </div>
            )}

            <ErrorFeedback />
            <FormFooter />
          </form>
        </div>
      </div>
    </div>
  );
};

export default CreateCollection;
