import axios from "axios";
import React, { useCallback, useEffect, useState } from "react";
import {
  MdAdd,
  MdArrowBack,
  MdClose,
  MdOutlineFileUpload,
} from "react-icons/md";
import { useNavigate, useParams } from "react-router-dom";
import { Button, IconButton, Loader, Player } from "../../Components";
import Dropzone from "react-dropzone";

const UploadForm = ({
  onClose,
  mediaId,
  archiveName,
  notify,
}: {
  onClose?;
  mediaId?: string;
  archiveName?: string;
  notify?: Function;
}) => {
  const [name, setName] = useState(archiveName);
  const [description, setDescription] = useState("");
  const [tags, setTags] = useState("");
  const [allTags, setAllTags] = useState<any>([]);
  const [group, setGroup] = useState("");
  const [upload, setUpload] = useState<any>({});
  const [uploadType, setUploadType] = useState<string>(null);
  const [uploaded, setUploaded] = useState(false);
  const [groupData, setGroupData] = useState<any[]>([]);
  const [extra, setExtra] = useState<any>([]);
  const [extraField, setExtraField] = useState<any>([]);
  const [error, setError] = useState<any[]>([]);
  const [path, setPath] = useState("");
  const [addTags, setAddTags] = useState(false);
  const [disabled, setDisabled] = useState(false);
  const [loading, setLoading] = useState(false);
  const maxWords = 250;
  const [{ content, wordCount }, setContent] = React.useState({
    content: description,
    wordCount: 0,
  });

  const navigate = useNavigate();
  const { id } = useParams();
  const auth = localStorage.getItem("token");
  const baseURL = localStorage.getItem("prod_url");

  useEffect(() => {
    if (window.location.pathname !== "undefined") {
      const val = window.location.pathname.split("/").splice(1, 2).pop();
      val && setPath(val);
    }
    id && setGroup(id);
    mediaId && getMedia();
    getGroupData();
  }, []);

  const newRecord = () => {
    setName("");
    setDescription("");
    setTags("");
    setGroup("");
    setUpload("");
    setUploaded(false);
  };

  const handleUpload = (file) => {
    setUpload(file[0]);
    setUploadType(file[0].type);

    console.log(file[0]);

    Object.entries(extraField).map(([key, value], index) =>
      setExtra((prev) => [
        ...prev,
        `{"question_id": "${key}", "response": "${value}"}`,
      ])
    );
  };

  const getGroupData = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/group/${id}/`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setGroupData(response.data.extra_group_questions);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const getMedia = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/archive/${mediaId}/`,
    };

    axios
      .request(options)
      .then((response) => {
        setName(response.data.name);
        setDescription(response.data.description);
        setAllTags(response.data.tags);
        setGroup(response.data.group.id);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  const postRecord = (e) => {
    e.preventDefault();

    const formData = new FormData();
    formData.append("name", name);
    formData.append("description", description);
    if (!mediaId) {
      formData.append("tags", tags);
      formData.append("group", group);
      formData.append("upload", upload);
      extra.length !== 0 &&
        formData.append("extra_group_response", `{"answers":[${extra}]}`);
    }

    const options = {
      method: "POST",
      url: `${baseURL}api/v1/archive/`,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Token ${auth}`,
      },
      data: formData,
    };

    const editOptions = {
      method: "PUT",
      url: `${baseURL}api/v1/archive/${mediaId}/`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${auth}`,
      },
      data: formData,
    };

    axios
      .request(mediaId ? editOptions : options)
      .then((response) => {
        console.log(response);
        notify("sucess");
        navigate(`/collection/${group}/media/${response.data.uuid}`);
        onClose();
      })
      .catch((error) => {
        console.error(error);
        notify("error");
        setError((prev) => [...prev, error.response.data]);
        setDisabled(false);
      });
  };

  const handleTag = (e) => {
    const addOptions = {
      method: "PUT",
      url: `${baseURL}api/v1/archive/add_tag/${mediaId}}/`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${auth}`,
      },
      data: { tags: [tags] },
    };

    axios
      .request(addOptions)
      .then((response) => {
        console.log(response.data);
        setTags("");
        getMedia();
        setDisabled(false);
      })
      .catch((error) => {
        console.error(error);
        setDisabled(false);
      });
  };

  const handleDelTag = (tag) => {
    const deleteOptions = {
      method: "PUT",
      url: `${baseURL}api/v1/archive/remove_tag/${mediaId}}/`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${auth}`,
      },
      data: { tags: [tag] },
    };

    axios
      .request(deleteOptions)
      .then((response) => {
        console.log(response.data);
        getMedia();
        setLoading(false);
      })
      .catch((error) => {
        console.error(error);
        setLoading(false);
      });
  };

  const setFormattedContent = useCallback(
    (text) => {
      let words = text.split(" ").filter(Boolean);
      if (words.length > maxWords) {
        setContent({
          content: words.slice(0, maxWords).join(" "),
          wordCount: maxWords,
        });
      } else {
        setContent({ content: text, wordCount: words.length });
      }
    },
    [maxWords, setContent]
  );

  const Upload = () => {
    return (
      <div className="flex items-center justify-center w-full mb-5">
        {!uploaded ? (
          <Dropzone
            onDrop={(e) => {
              handleUpload(e);
              setUploaded(true);
            }}
          >
            {({ getRootProps, getInputProps, isDragActive, isDragReject }) => (
              <div
                className="flex flex-col w-full h-full mx-auto border-4 border-dashed hover:bg-blue-100 hover:border-blue-300"
                {...getRootProps()}
              >
                <div className="text-center items-center place-items-center justify-center mt-5 mb-5">
                  <MdOutlineFileUpload size={30} />
                  <input
                    type="file"
                    placeholder="Select a media"
                    className="form-control block w-full px-5 py-5 text-xl font-normal text-gray-700 m-0 focus:text-gray-700 fous:bg-white focus:bg-none focus:b-none focus:outline-none"
                    onChange={(e) => {
                      handleUpload(e);
                      setUploaded(true);
                      console.log(e);
                    }}
                    {...getInputProps()}
                    required
                  />
                  {!isDragActive &&
                    "Click me or drag a file to upload your media"}
                  {isDragActive &&
                    !isDragReject &&
                    "Drop it here to upload your media"}
                  {isDragReject && "File type not accepted, sorry!"}
                </div>
              </div>
            )}
          </Dropzone>
        ) : (
          upload && (
            <div className="flex w-full">
              <div className="flex flex-col w-full h-full mx-auto border-4 item-center justify-center place-items-center border-dashed hover:bg-blue-100 hover:border-blue-300">
                <Player
                  upload={URL.createObjectURL(upload)}
                  type={uploadType}
                />
                {upload.name}
              </div>
              <div className="-ml-10 -mb-2">
                <IconButton
                  type="tertiary"
                  size="xs"
                  onClick={() => {
                    setUpload("");
                    setUploaded(false);
                  }}
                  color="black"
                >
                  <MdClose size={25} />
                </IconButton>
              </div>
            </div>
          )
        )}
      </div>
    );
  };

  const handleInput = (e) => {
    const { name, value } = e.target;
    extraField[name] = value;
    setExtraField({ ...extraField });
  };

  if (loading) {
    return <Loader />;
  }

  return (
    <div>
      <div className="item-align-center min-w-full max-w-screen">
        <div className="flex justify-center">
          <div className="m-4">
            <form onSubmit={(e) => postRecord(e)}>
              <div className="md:flex md:items-center m-5 mb-6">
                <div className="md:w-1/3">
                  <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                    Name
                  </label>
                </div>
                <div className="md:w-2/3">
                  <input
                    required
                    className="bg-gray-200 border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
                    type="text"
                    maxLength={300}
                    value={name}
                    onChange={(e) => {
                      setName(e.target.value);
                    }}
                  />
                </div>
              </div>

              <div className="md:flex md:items-center m-5 mb-6">
                <div className="md:w-1/3">
                  <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                    Desciption
                  </label>
                </div>
                <div className="md:w-2/3 relative">
                  <textarea
                    className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                    rows={5}
                    value={content}
                    onChange={(e) => {
                      setFormattedContent(e.target.value);
                      setDescription(e.target.value);
                    }}
                    required
                  />
                  <span className="font-normal text-xs text-blueGray-400 absolute bottom-0 right-0 p-2">
                    {wordCount}/{maxWords}
                  </span>
                </div>
              </div>
              {mediaId ? (
                <>
                  <div className="md:flex md:items-center m-5 mb-6">
                    <div className="md:w-1/3">
                      <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                        Tags
                      </label>
                    </div>
                    <div className="grid lg:grid-cols-5 sm:grid-cols-3 md:grid-cols-3 gap-1 md:w-2/3">
                      {allTags?.map((tag, key) => (
                        <div className="inline-flex gap-5" key={key}>
                          <div className="inline-flex bg-gray-200 rounded-full py-1 px-2 text-sm font-semibold text-gray-700 mr-4 mb-2">
                            {tag.name}
                            <IconButton
                              size="xs"
                              type="tertiary"
                              color="red"
                              className="p-0 px-0 -mt-4 -mr-3"
                              disabled={loading}
                              onClick={(e) => {
                                setLoading(true);
                                handleDelTag(tag.id);
                              }}
                            >
                              <MdClose size={20} />
                            </IconButton>
                          </div>
                        </div>
                      ))}
                      <IconButton
                        size="xs"
                        type="primary"
                        onClick={() => setAddTags(true)}
                        disabled={addTags}
                      >
                        <MdAdd size={20} />
                      </IconButton>
                    </div>
                  </div>
                  {addTags && (
                    <div className="md:flex md:items-center m-5 mb-6">
                      <div className="md:w-1/3">
                        <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                          New Tag
                        </label>
                      </div>
                      <div className="flex md:w-2/3 gap-2">
                        <input
                          className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                          type="text"
                          value={tags}
                          onChange={(e) => {
                            setTags(e.target.value);
                          }}
                        />
                        <Button
                          id="addTag"
                          size="sm"
                          type="primary"
                          className="float-right"
                          onClick={(e) => {
                            setDisabled(true);
                            handleTag(e);
                          }}
                          disabled={disabled}
                        >
                          Add
                        </Button>
                      </div>
                    </div>
                  )}
                </>
              ) : (
                <>
                  <div className="md:flex md:items-center m-5 mb-6">
                    <div className="md:w-1/3">
                      <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                        Tags
                      </label>
                    </div>
                    <div className="md:w-2/3">
                      <input
                        className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                        type="text"
                        value={tags}
                        onChange={(e) => {
                          setTags(e.target.value);
                        }}
                      />
                    </div>
                  </div>
                  {groupData &&
                    groupData.map((group, key) => (
                      <div
                        className="md:flex md:items-center m-5 mb-6"
                        key={key}
                      >
                        <div className="md:w-1/3">
                          <label
                            className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4"
                            key={key}
                          >
                            {group.question}
                            {group.question_mandatory && "*"}
                          </label>
                        </div>
                        <div className="md:w-2/3">
                          <input
                            className="bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-purple-500"
                            type="text"
                            key={key}
                            maxLength={300}
                            name={group.id}
                            onChange={(e) => {
                              handleInput(e);
                            }}
                          />
                        </div>
                      </div>
                    ))}
                  <Upload />
                </>
              )}

              {error?.map((err, i) => (
                <div
                  className="flex items-center justify-center p-5 border-b border-solid border-slate-200 rounded-t"
                  key={i}
                >
                  <h5 className="text-xl font-medium text-red-600">
                    {err.detail}
                  </h5>
                </div>
              ))}
              <div className="text-center mt-6 p-5">
                <Button
                  size="sm"
                  className="w-full"
                  type="secondary"
                  onClick={(e) => {
                    setDisabled(true);
                    postRecord(e);
                  }}
                  disabled={disabled}
                >
                  Upload
                </Button>
                <Button
                  size="sm"
                  type="tertiary"
                  className="uppercase"
                  color="red"
                  onClick={newRecord}
                >
                  Cancel
                </Button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default UploadForm;
