import React, { Fragment, useEffect, useState } from "react";
import { AiOutlineUpload } from "react-icons/ai";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import {
  MdContentCopy,
  MdDataSaverOn,
  MdEdit,
  MdLock,
  MdModeEdit,
  MdOutlineArrowBack,
  MdOutlineDelete,
  MdPermIdentity,
  MdPermMedia,
  MdPersonAdd,
  MdSettings,
  MdCheckCircle,
  MdOutlinePermMedia,
} from "react-icons/md";
import {
  Button,
  IconButton,
  DeleteModal,
  CopyModal,
  ImportModal,
  Loader,
  CustomModal,
  DialogModal,
  Line,
  Graph,
  Dropdown,
  Disclosures,
  Float,
} from "../../Components";
import { BiExport, BiImport } from "react-icons/bi";
import { IoAddOutline } from "react-icons/io5";
import { ImStatsDots } from "react-icons/im";
import UploadForm from "./UploadForm";
import CreateCollection from "./CreateCollection";
import { useRecoilValue } from "recoil";
import { prodState, tokenState } from "../AuthState/AuthState";
import { Menu, Transition } from "@headlessui/react";
import { Collapse, Divider, Tabs, notification, Space, Badge, Card, Popover, Row, Col } from "antd";
import MediaHeader from "../Media/MediaHeader";
import TagButton from "../../Components/Button/TagButton";
import  AntButton  from "antd/lib/button";
import CardLayout from "../../Components/Cards/CardLayout";

interface IPage {
  next?: boolean;
  previous?: boolean;
}

type NotificationType = "success" | "info" | "warning" | "error";

const Collection = ({
  groupId,
  groupName,
  groupDesc,
}: {
  groupId?: string;
  groupName?: string;
  groupDesc?: string;
}) => {
  const [recordings, setRecordings] = useState<any[]>([]);
  const [groupData, setGroupData] = useState<any>([]);
  const [curMedia, setCurMedia] = useState<string>("");
  const [tags, setTags] = useState<any[]>([]);
  const [display, setDisplay] = useState(true);
  const [mediaVal, setMediaVal] = useState("search");
  const [searchMedia, setSearchMedia] = useState<any[]>([]);
  const [page, setPage] = useState({ previous: "", next: "" });
  const [paginate, setPaginate] = useState<IPage>({
    previous: false,
    next: false,
  });
  const [showModal, setShowModal] = useState(false);
  const [count, setCount] = useState(0);
  const [delUser, setDelUser] = useState("");
  const [deleteUserName, setDeleteUserName] = useState("");
  const [me, setMe] = useState<any>([]);
  const [grpUsers, setGrpUsers] = useState<any[]>([]);
  const [grpCustom, setGrpCustom] = useState<any[]>([]);
  const [error, setError] = useState<string>("");
  const [copyModal, setCopyModal] = useState(false);
  const [mediaId, setMediaId] = useState("");
  const [mediaValue, setMediaValue] = useState("");
  const [copyExtra, setCopyExtra] = useState<any[]>([]);
  const [loading, setLoading] = useState(true);
  const [openTab, setOpenTab] = useState(1);
  const [openRadio, setOpenRadio] = useState(1);
  const [importModal, setImportModal] = useState(false);
  const [deleteCustomModal, setDeleteCustomModal] = useState(false);
  const [deleteCustomQuestion, setDeleteCustomQuestion] = useState(false);
  const [deleteMediaModal, setDeleteMediaModal] = useState(false);
  const [deleteUserModal, setDeleteUserModal] = useState(false);
  const [superUser, setSuperUser] = useState<any>(null);
  const [mediaStatistics, setMediaStatistics] = useState<any>([]);
  const [annoStatistics, setAnnoStatistics] = useState<any>([]);
  const [uploadModal, setUploadModal] = useState(false);
  const [editModal, setEditModal] = useState(false);
  const [groupKnowledge, setGroupKnowledge] = useState<any>([]);
  const [tag, setTag] = useState<any>([]);
  const [editColModal, setEditColModal] = useState(false);
  const [addUserColModal, setAddUserColModal] = useState(false);
  const [customColModal, setCustomColModal] = useState(false);
  const [keepColModal, setKeepColModal] = useState(false);
  const [showUpload, setShowUpload] = useState(false);
  const [relations, setRelations] = useState(false);
  const [api, contextHolder] = notification.useNotification();

  const navigate = useNavigate();
  const { id } = useParams();
  const auth = localStorage.getItem("token");
  const baseURL = localStorage.getItem("prod_url");

  const openNotificationWithIcon = (type: NotificationType, msg) => {
    api[type]({
      message: msg,
      placement: "topRight",
    });
  };

  useEffect(() => {
    getGroup();
    getRecordings();
    if (auth) {
      getUser();
    }
  }, []);

  useEffect(() => {
    if (openRadio === 1) {
      getGroupKnowledgeGraph();
    }
    if (openRadio === 3) {
      getGroupKnowledgeGraph();
    }
    if (auth) {
      getUser();
      if (openRadio === 2) {
        getMediaStatistics();
        getAnnoStatistics();
      }
    }
  }, [openRadio]);

  const getRecordings = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/archive/?group=${id}`,
      headers: { "Content-Type": "application/json" },
    };

    axios
      .request(options)
      .then((response) => {
        setRecordings(response.data.results);
        setCount(response.data.count);
        setPage({ next: response.data.next, previous: response.data.previous });
        response.data.next === null && setPaginate({ next: true });
        response.data.previous === null && setPaginate({ previous: true });
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setError(e?.message);
      });
  };

  const getGroup = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/group/${id}/`,
      headers: {
        "Content-Type": "application/json",
      },
    };

    const authOptions = {
      method: "GET",
      url: `${baseURL}api/v1/group/${id}/`,
      headers: {
        "Content-Type": "application/json",
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(auth ? authOptions : options)
      .then((response) => {
        setGroupData(response.data);
        setGrpUsers(response.data.users);
        setGrpCustom(response.data.extra_group_questions);
        setTag(response.data.tags);
      })
      .catch((e) => {
        console.log(e);
        setLoading(true);
        setError(e.message);
      });
  };

  const getPage = (id) => {
    const options = {
      method: "GET",
      url: id,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };

    axios
      .request(options)
      .then((response) => {
        setRecordings(response.data.results);
        setPage({ next: response.data.next, previous: response.data.previous });
        response.data.next === null
          ? setPaginate({ next: true })
          : setPaginate({ next: false });
        response.data.previous === null && setPaginate({ previous: true });
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setError(e.message);
      });
  };

  const getUser = () => {
    const options = {
      method: "GET",
      url: `${baseURL}auth/users/me/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setMe(response.data);
        setSuperUser(response.data.is_superuser);
      })
      .catch((e) => {
        console.log(e);
        setError(e.message);
      });
  };

  const getMediaStatistics = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/groupmediastats/${id}/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setMediaStatistics(response.data);
      })
      .catch((error) => {
        console.error(error.message);
      });
  };

  const getAnnoStatistics = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/groupannotationstats/${id}/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        setAnnoStatistics(response.data);
      })
      .catch((error) => {
        console.error(error.message);
      });
  };

  const getGroupKnowledgeGraph = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/group/taggraph/${id}/`,
    };

    const authOptions = {
      method: "GET",
      url: `${baseURL}api/v1/group/taggraph/${id}/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(auth ? authOptions : options)
      .then((response) => {
        setGroupKnowledge(response.data);
      })
      .catch((error) => {
        console.error(error.message);
      });
  };

  const handleDisplay = (id) => {
    if (id === "next") {
      page.next !== null && getPage(page.next);
    } else if (id === "prev") {
      page.previous === null
        ? setPaginate({ previous: true })
        : getPage(page.previous);
    }
  };

  const Pagination = () => {
    return (
      <>
        {page.next === null && page.previous === null ? null : (
          <div className="flex justify-center">
            <Button
              size="sm"
              type="tertiary"
              className="m-5 font-semibold border-2 w-fit rounded-lg shadow-lg border-blue-600 -blue-600 disabled:opacity-25 focus:outline-none"
              onClick={(e) => {
                handleDisplay("prev");
                setLoading(true);
              }}
              disabled={paginate.previous}
            >
              Previous
            </Button>
            <Button
              size="sm"
              type="tertiary"
              className="p-2 m-5 font-semibold border-2 w-min rounded-lg shadow-lg border-blue-600 -blue-600 disabled:opacity-25 focus:outline-none"
              onClick={(e) => {
                handleDisplay("next");
                setLoading(true);
              }}
              disabled={paginate.next}
            >
              Next
            </Button>
          </div>
        )}
      </>
    );
  };

  const handleDeleteUser = (user) => {
    const form = new FormData();
    form.append("user", user);

    const options = {
      method: "PUT",
      url: `${baseURL}api/v1/group/remove_user/${id}/`,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Token ${auth}`,
      },
      data: form,
    };

    axios
      .request(options)
      .then((response) => {
        console.log(response);
        setDeleteUserModal(false);
        getGroup();
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setError(e.response.data.detail);
        setLoading(false);
      });
  };

  const handleMedia = (mediaId) => {
    const options = {
      method: "DELETE",
      url: `${baseURL}api/v1/archive/${mediaId}/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        console.log(response);
        getGroup();
        getRecordings();
        setDeleteMediaModal(false);
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setError(e.response.data.detail);
        setLoading(false);
      });
  };

  const handleExport = () => {
    const options = {
      method: "POST",
      url: `${baseURL}api/v1/export/`,
      headers: {
        Authorization: `Token ${auth}`,
        "Content-Type": "application/json",
      },
      data: { type: "group", id: id },
    };

    axios
      .request(options)
      .then((response) => {
        console.log(response.data);
        navigate("/requests");
      })
      .catch((error) => {
        console.error(error);
        setError(error.response.data);
      });
  };

  const notifyToast = (id) => {
    switch (id) {
      case "success":
        openNotificationWithIcon("success", "Successfully Uploaded Media");
        break;
      case "error":
        openNotificationWithIcon("error", "Failed to Uploaded Media");
        break;
    }
  };

  const MediaDropDown = ({media}) => {
    return (
      <>
      {/* {media?.map((y, i) => ( */}
        <div className="">
          {grpUsers?.map( (id, key) => me?.id === id?.id && (
              <Dropdown>
                <div
                  className="pt-5 p-2 flex flex-col gap-3 overflow-x-auto z-0"
                  key={key}
                >
                  <Button
                    size="xs"
                    className="w-fit p-1 flex gap-2 bg-white text-emerald-500 "
                    type="tertiary"
                    color="emerald"
                    onClick={() => {
                      // navigate(`/media/edit/${media?.uuid}`)
                      setMediaId(media?.uuid);
                      setMediaValue(media?.name);
                      setEditModal(true);
                    }}
                  >
                    <MdEdit size={15} />
                    Edit
                  </Button>

                  <Button
                    size="xs"
                    className="w-fit p-1 flex gap-2 bg-white text-red-500 "
                    type="tertiary"
                    color="red"
                    onClick={() => {
                      setMediaId(media?.uuid);
                      setMediaValue(media?.name);
                      setDeleteMediaModal(true);
                    }}
                  >
                    <MdOutlineDelete size={15} />
                    Delete
                  </Button>

                  <Button
                    size="xs"
                    className="w-fit p-1 flex gap-2 bg-white text-emerald-500 "
                    type="tertiary"
                    color="emerald"
                    onClick={() => {
                      setMediaId(media?.uuid);
                      setCopyExtra(media?.extra_group_response);
                      setCopyModal(true);
                    }}
                  >
                    <MdContentCopy size={15} />
                    Copy
                  </Button>
                </div>
              </Dropdown>
          ))}
          {deleteMediaModal ? (
          <DialogModal
            open={deleteMediaModal}
            onClose={() => setDeleteMediaModal(false)}
            title={`Are you sure you want to delete this media ${mediaValue}?`}
          >
            <DeleteModal
              onClose={() => setDeleteMediaModal(false)}
              onClick={() => {
                handleMedia(mediaId);
              }}
            />
          </DialogModal>
        ) : (
          copyModal && (
            <DialogModal
              open={copyModal}
              onClose={() => setCopyModal(false)}
              title={"Copy Media to another group"}
            >
              <CopyModal
                onClose={() => setCopyModal(false)}
                mediaId={mediaId}
                groupId={id}
                groups={me.groups}
                extra={copyExtra}
              />
            </DialogModal>
          )
        )}
        {editModal && (
          <DialogModal
            open={editModal}
            onClose={() => setEditModal(false)}
            title={`Edit media ${mediaValue}`}
          >
            {error.length !== 0 && (
              <div className="flex items-center justify-center p-5 border-b border-solid border-slate-200 rounded-t">
                <h5 className="text-xl font-medium text-red-600">{error}</h5>
              </div>
            )}
            <UploadForm onClose={() => setEditModal(false)} mediaId={mediaId} />
          </DialogModal>
        )}
        </div>
          {/* ))} */}
      </>
    )
  }

  const DisplayCards = ({ media }: { media: any }) => {
    const navigate = useNavigate();

    return (
      <div className="grid lg:grid-cols-3 py-5 px-5 gap-10 h-auto overflow-hidden">
        {media?.map((y, i) => (
          <>
            <div
              className="grid grid-cols-6 rounded-lg bg-white max-w-sm cursor-pointer h-full shadow-xl shadow-gray-500"
              key={i}
            >
              <div className="col-span-5">
                <div className="relative flex flex-col min-w-0 break-words bg-white rounded mb-10 xl:mb-0 ">
                  <div className="flex-auto p-4">
                    <span className="font-semibold text-xl text-blueGray-700">
                      {y.name}
                    </span>

                    <p className="text-blueGray-400 uppercase font-bold text-xs h-auto min-h-24 max-h-28 line-clamp-5 hover:line-clamp-none mb-2">
                      {y.description}
                    </p>

                    <div className="pt-3 gap-2">
                      {y.tags &&
                        y.tags?.map(
                          (tag, i) =>
                            tag.name !== "" &&
                            tag.count !== 0 && (
                              <div className="inline-flex p-1" key={i}>
                                <TagButton
                                  color="rgb(85, 172, 238)"
                                  tagName={tag.name}
                                />
                              </div>
                            )
                        )}
                    </div>
                  </div>
                </div>
              </div>

              

              <div
                className="col-span-6 p-3 w-max-content items-center px-2 text-sm font-bold min-h-fit max-h-16 h-12 mt-auto justify-center text-center bg-emerald-500 text-white hover:bg-opacity-70 focus:outline-none focus-visible:ring-2 focus-visible:ring-white focus-visible:ring-opacity-75"
                onClick={() => {
                  navigate(`/collection/${id}/media/${y.uuid}`);
                }}
              >
                View
              </div>
            </div>
          </>
        ))}
        
      </div>
    );
  };

  const DashboardButton = () => {
    return (
      <div className="flex flex-row pb-5">
        <Button
          size="md"
          type="tertiary"
          onClick={() => {
            navigate(`/`);
          }}
          className="flex flex-row px-0 py-0 absolute top-0 left-0"
        >
          <MdOutlineArrowBack className="p-1" size={25} />
          Back to Dashboard
        </Button>
      </div>
    );
  };

  const GroupInfo = () => {

    const UserDelete = ({userData}) => {
      const [open, setOpen] = useState(false);
      const [clicked, setClicked] = useState(false);
      const [hovered, setHovered] = useState(false);
  
      const hide = () => {
        setClicked(false);
        setHovered(false);
      };
  
      const handleHoverChange = (open: boolean) => {
        setHovered(open);
        setClicked(false);
      };
  
      const handleClickChange = (open: boolean) => {
        setHovered(false);
        setClicked(open);
      };
  
      const showContent = () => {
        return (
          <div className="flex gap-2 justify-between place-content-center items-center">
            <div>{userData.first_name}</div>
            <div className="border-2 border-red-500 p-1 rounded-full align-middle items-center text-center self-center" onClick={()=>{setDelUser(userData.id);setDeleteUserModal(true);setDeleteUserName(userData.first_name)}}><MdOutlineDelete size={15} /></div>
          </div>
        )
      }
      return (
        <>
          <Popover
            content={showContent}
            trigger="hover"
            open={hovered}
            onOpenChange={handleHoverChange}
          >
            <Popover
              content={showContent}
              trigger="click"
              open={clicked}
              onOpenChange={handleClickChange}
            >
              <AntButton type="text" shape="round" size="middle" className="border-green-500 border-2 font-semibold">{userData.first_name}</AntButton>
            </Popover>
          </Popover>
          
        </>
      )
  }

    return (
      <>
        <div className="mt-3">
          <Space direction="vertical" size="middle" style={{ width: '100%' }}>
              <Card title={<>
                <div className="flex justify-between h-[3rem] mt-4">
                  <div className="flex gap-2 w-[80%]">
                    <Float
                      tooltip={"Back to Dashboard"}
                      icon={<MdOutlineArrowBack />}
                      href="/"
                    />
                  </div>

                  {auth &&
                      groupData.users?.map((user, i) => {
                        return (
                          user.id.includes(me.id) && (
                            <>
                              <div className="flex gap-2">
                                {relations ? (
                                  <AntButton type="text" className="px-2 rounded-lg border-2 border-sky-600 cursor-pointer text-sm font-semibold" onClick={() => setRelations(!relations)}>
                                    <div className="flex gap-2  items-center">
                                      <MdOutlinePermMedia className="h-[1.2rem] w-[1.2rem]"/>{"Media"} 
                                    </div>
                                  </AntButton>
                                ) : (
                                  <AntButton type="text" className="px-2 rounded-lg border-2 border-sky-600 cursor-pointer text-sm font-semibold" onClick={() => setRelations(!relations)}>
                                    <div className="flex gap-2 items-center">
                                      <ImStatsDots className="h-[1rem] w-[1rem]"/>{"Relations"} 
                                    </div>
                                  </AntButton>
                                )}
                                <AntButton type="text" className="px-2 mr-[3rem] rounded-lg border-2 border-sky-600 cursor-pointer text-sm font-semibold" onClick={() => setUploadModal(true)}>
                                  <div className="flex gap-2 items-center">
                                    <AiOutlineUpload className="h-[1.2rem] w-[1.2rem]"/>{"Upload Media"} 
                                  </div>
                                </AntButton>
                              </div>
                              <div className="flex gap-2 absolute top-2 right-2">
                                <Menu as="div" className="flex">
                                  <div className="items-center flex rounded-xl hover:bg-[#d6ebfd] p-2 focus:outline-none ">
                                    <Menu.Button className="flex text-sm focus:outline-none focus:ring-none focus:ring-none">
                                      <MdSettings
                                        className="h-[1.5rem] w-[1.5rem]"
                                        size={50}
                                      />
                                    </Menu.Button>
                                  </div>
                                  <Transition
                                    as={Fragment}
                                    enter="transition ease-out duration-100"
                                    enterFrom="transform opacity-0 scale-95"
                                    enterTo="transform opacity-100 scale-100"
                                    leave="transition ease-in duration-75"
                                    leaveFrom="transform opacity-100 scale-100"
                                    leaveTo="transform opacity-0 scale-95"
                                  >
                                    <div className="text-md font-semibold absolute right-0 px-2 z-10 mt-[3.6rem] min-w-[16rem] max-w-[100%] origin-top-right rounded-md text-black bg-white py-1 shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none ">
                                      <Menu.Items className="divide-y divide-[#e5e7eb] w-full">
                                        <Menu.Item>
                                          <div className="my-2">
                                            <div className="flex flex-row w-full my-2 text-sm font-normal">
                                              <div className="flex flex-col w-full">
                                                <div
                                                  className="p-2 hover:bg-[#acd3ee] cursor-pointer flex  justify-between"
                                                  onClick={() =>
                                                    setEditColModal(true)
                                                  }
                                                >
                                                  <span className="flex place-items-center">
                                                    <MdModeEdit
                                                      className="pr-2"
                                                      size={25}
                                                    />
                                                    <span> Edit Group </span>
                                                  </span>
                                                </div>

                                                <div
                                                  className="p-2 hover:bg-[#acd3ee] cursor-pointer flex  justify-between"
                                                  onClick={() =>
                                                    setAddUserColModal(true)
                                                  }
                                                >
                                                  <span className="flex place-items-center">
                                                    <MdPersonAdd
                                                      className="pr-2"
                                                      size={25}
                                                    />
                                                    <span> Add User</span>
                                                  </span>
                                                </div>

                                                <div
                                                  className="p-2 hover:bg-[#acd3ee] cursor-pointer flex  justify-between"
                                                  onClick={() =>
                                                    setCustomColModal(true)
                                                  }
                                                >
                                                  <span className="flex place-items-center">
                                                    <IoAddOutline
                                                      className="pr-2"
                                                      size={25}
                                                    />
                                                    <span> Add Custom Question</span>
                                                  </span>
                                                </div>
                                                {grpCustom.length !== 0 && (
                                                  <>
                                                    <div
                                                      className="p-2 hover:bg-[#acd3ee] cursor-pointer flex  justify-between"
                                                      onClick={() => {
                                                        setDeleteCustomModal(true);
                                                        setDeleteCustomQuestion(
                                                          false
                                                        );
                                                      }}
                                                    >
                                                      <span className="flex place-items-center">
                                                        <MdModeEdit
                                                          className="pr-2"
                                                          size={25}
                                                        />
                                                        <span>
                                                          {" "}
                                                          Edit Custom Question
                                                        </span>
                                                      </span>
                                                    </div>
                                                    <div
                                                      className="p-2 hover:bg-[#acd3ee] cursor-pointer flex  justify-between"
                                                      onClick={() => {
                                                        setDeleteCustomModal(true);
                                                        setDeleteCustomQuestion(true);
                                                      }}
                                                    >
                                                      <span className="flex place-items-center">
                                                        <MdOutlineDelete
                                                          className="pr-2"
                                                          size={25}
                                                        />
                                                        <span>
                                                          {" "}
                                                          Delete Custom Question
                                                        </span>
                                                      </span>
                                                    </div>
                                                  </>
                                                )}

                                                <div
                                                  className="p-2 hover:bg-[#acd3ee] cursor-pointer flex  justify-between"
                                                  onClick={() =>
                                                    setKeepColModal(true)
                                                  }
                                                >
                                                  <span className="flex place-items-center">
                                                    <MdDataSaverOn
                                                      className="pr-2"
                                                      size={25}
                                                    />
                                                    <span> Keep deleted data</span>
                                                  </span>
                                                </div>
                                                <div
                                                  className="p-2 hover:bg-[#acd3ee] cursor-pointer flex  justify-between"
                                                  onClick={() => handleExport()}
                                                >
                                                  <span className="flex place-items-center">
                                                    <BiExport
                                                      className="pr-2"
                                                      size={25}
                                                    />
                                                    <span> Export Group</span>
                                                  </span>
                                                </div>
                                                <div
                                                  className="p-2 hover:bg-[#acd3ee] cursor-pointer flex  justify-between"
                                                  onClick={() => setImportModal(true)}
                                                >
                                                  <span className="flex place-items-center">
                                                    <BiImport
                                                      className="pr-2"
                                                      size={25}
                                                    />
                                                    <span> Import a Archive</span>
                                                  </span>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </Menu.Item>
                                      </Menu.Items>
                                    </div>
                                  </Transition>
                                </Menu>
                              </div>
                            </>
                          )
                        );
                      })
                    }
                </div>
              </>} size="small">
                    <div className="flex justify-between self-center items-center">
                      <span className="text-lg font-semibold">{groupData?.name}</span>
                      <Badge count={`${groupData?.is_public ? "Public" : "Private"}`} showZero color={`${groupData?.is_public ? "#52c41a" : "#faad14"}`} className="ml-4"/>
                    </div>
                    <p className="mt-2 text-sm h-auto min-h-24 max-h-28 line-clamp-5 hover:line-clamp-none ">
                      {groupData?.description}
                    </p>
                      <div className="relative w-auto mt-4">
                        <div className="col-span-4">
                        <div
                                  className="group inline-flex gap-2 py-2 cursor-pointer"
                                >
                          {groupData.users &&
                            groupData.users.map((user, key) => {
                              return (
                                <div key={key}>
                                  <UserDelete userData={user}/>
                                </div>
                              );
                            })}
                            </div>
                        </div>
                      </div>
                      <div className="pt-3 gap-2">
                        {tag?.map(
                          (tag, i) =>
                            tag.name !== "" &&
                            tag.count !== 0 && (
                              <div className="inline-flex p-1" key={i}>
                                <TagButton
                                  color="rgb(85, 172, 238)"
                                  tagName={tag.name}
                                />
                              </div>
                            )
                        )}
                      </div>
                      {deleteUserModal && (
                                    <DialogModal
                                      open={deleteUserModal}
                                      onClose={() => setDeleteUserModal(false)}
                                      title={`Are you sure you want to delete this ${deleteUserName} user?`}
                                    >
                                      {error.length !== 0 && (
                                        <div className="flex items-center justify-center p-5 border-b border-solid border-slate-200 rounded-t">
                                          <h5 className="text-xl font-medium text-red-600">
                                            {error}
                                          </h5>
                                        </div>
                                      )}
                                      <DeleteModal
                                        onClick={() => {
                                          setLoading(true);
                                          handleDeleteUser(delUser);
                                        }}
                                        onClose={() => setDeleteUserModal(false)}
                                        disabled={loading}
                                      />
                                    </DialogModal>
                                  )}
              </Card>
          </Space>
          <hr className="border border-t-[1px] border-solid border-slate-200 my-[2rem]"/>
          {uploadModal && (
            <DialogModal
              open={uploadModal}
              onClose={() => setUploadModal(false)}
              title={`Upload a Media`}
            >
              {error.length !== 0 && (
                <div className="flex items-center justify-center p-5 border-b border-solid border-slate-200 rounded-t">
                  <h5 className="text-xl font-medium text-red-600">{error}</h5>
                </div>
              )}
              <UploadForm
                onClose={() => setUploadModal(false)}
                archiveName={`${localStorage.getItem("username")} ${
                  groupData.name
                } ${new Date()}`}
                notify={notifyToast}
              />
            </DialogModal>
          )}
          {importModal && (
            <DialogModal
              open={importModal}
              onClose={() => setImportModal(false)}
              title={"Import a Archive"}
            >
              <ImportModal
                type={"archive"}
                groupId={id}
                onClose={() => setImportModal(false)}
              />
            </DialogModal>
          )}
          {deleteCustomModal && (
            <DialogModal
              open={deleteCustomModal}
              onClose={() => {
                setDeleteCustomModal(false);
                setDeleteCustomQuestion(false);
              }}
              title={`${
                deleteCustomQuestion ? "Delete" : "Edit"
              } a Custom Question`}
            >
              <CustomModal
                groupId={id}
                onClose={() => {
                  setDeleteCustomModal(false);
                  getGroup();
                }}
                groups={grpCustom}
                deleteButton={deleteCustomQuestion}
              />
            </DialogModal>
          )}
          {editColModal && (
            <DialogModal
              open={editColModal}
              onClose={() => setEditColModal(false)}
              title={`Edit ${groupData.name}`}
            >
              <CreateCollection
                onClose={() => {
                  setEditColModal(false);
                  getGroup();
                }}
                path="edit"
              />
            </DialogModal>
          )}
          {addUserColModal && (
            <DialogModal
              open={addUserColModal}
              onClose={() => setAddUserColModal(false)}
              title={`Add users to ${groupData.name}`}
            >
              <CreateCollection
                onClose={() => {
                  setAddUserColModal(false);
                  getGroup();
                }}
                path="add-user"
              />
            </DialogModal>
          )}
          {customColModal && (
            <DialogModal
              open={customColModal}
              onClose={() => setCustomColModal(false)}
              title={`Add Custom Question to ${groupData.name}`}
            >
              <CreateCollection
                onClose={() => {
                  setCustomColModal(false);
                  getGroup();
                }}
                path="custom-question"
              />
            </DialogModal>
          )}
          {keepColModal && (
            <DialogModal
              open={keepColModal}
              onClose={() => setKeepColModal(false)}
              title={`Add Custom Question to ${groupData.name}`}
            >
              <CreateCollection
                onClose={() => {
                  setKeepColModal(false);
                  getGroup();
                }}
                path="keep-data"
              />
            </DialogModal>
          )}
        </div>
      </>
    );
  };

  const NoData = () => {
    return (
      <div className="text-center justify-center font-semibold text-lg">
        😮 Oops ! There are no media part of this collection on Papad. Explore
        other public {auth && "/ private "}
        collections or <AntButton type="text" className="border-blue-500 border-2 rounded-full" onClick={()=> auth ? setUploadModal(true) : navigate("/auth/login")}> create your own </AntButton>
      </div>
    );
  };

  const Tabs = () => {
    return (
      <>
        <div className="flex flex-wrap">
          {count === 0 ? (
            <div className="w-full">
              <CardLayout noDataText={<NoData/>}/>
            </div>
          ) : (
            <div className="w-full">
              <Row gutter={[16, 16]}>
                      {recordings?.map((res, i) => (
                      <Col xs={{ span: 24 }} lg={{ span: 8}}>
                          <CardLayout media={res} dropdown={<MediaDropDown media={res}/>} id={i}/>
                        
                      </Col>
                    ))}
              </Row>
              <div>
                <Pagination />
              </div>
            </div>
          )}
          {importModal && (
            <DialogModal
              open={importModal}
              onClose={() => setImportModal(false)}
              title={"Import a Archive"}
            >
              <ImportModal
                type={"archive"}
                groupId={id}
                onClose={() => setImportModal(false)}
              />
            </DialogModal>
          )}
          {deleteCustomModal && (
            <DialogModal
              open={deleteCustomModal}
              onClose={() => {
                setDeleteCustomModal(false);
                setDeleteCustomQuestion(false);
              }}
              title={`${
                deleteCustomQuestion ? "Delete" : "Edit"
              } a Custom Question`}
            >
              <CustomModal
                groupId={id}
                onClose={() => {
                  setDeleteCustomModal(false);
                  getGroup();
                }}
                groups={grpCustom}
                deleteButton={deleteCustomQuestion}
              />
            </DialogModal>
          )}
          {editColModal && (
            <DialogModal
              open={editColModal}
              onClose={() => setEditColModal(false)}
              title={`Edit ${groupData.name}`}
            >
              <CreateCollection
                onClose={() => {
                  setEditColModal(false);
                  getGroup();
                }}
                path="edit"
              />
            </DialogModal>
          )}
          {addUserColModal && (
            <DialogModal
              open={addUserColModal}
              onClose={() => setAddUserColModal(false)}
              title={`Add users to ${groupData.name}`}
            >
              <CreateCollection
                onClose={() => {
                  setAddUserColModal(false);
                  getGroup();
                }}
                path="add-user"
              />
            </DialogModal>
          )}
          {customColModal && (
            <DialogModal
              open={customColModal}
              onClose={() => setCustomColModal(false)}
              title={`Add Custom Question to ${groupData.name}`}
            >
              <CreateCollection
                onClose={() => {
                  setCustomColModal(false);
                  getGroup();
                }}
                path="custom-question"
              />
            </DialogModal>
          )}
          {keepColModal && (
            <DialogModal
              open={keepColModal}
              onClose={() => setKeepColModal(false)}
              title={`Add Custom Question to ${groupData.name}`}
            >
              <CreateCollection
                onClose={() => {
                  setKeepColModal(false);
                  getGroup();
                }}
                path="keep-data"
              />
            </DialogModal>
          )}
        </div>
      </>
    );
  };

  const Stats = () => {
    return (
      <>
        <div className="flex flex-wrap">
          <div className="w-full">
            <ul
              className="flex mb-0 list-none flex-wrap pt-3 pb-4 flex-row px-5"
              role="tablist"
            >
              <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
                <Button
                  size="sm"
                  type="tertiary"
                  className={
                    "w-full flex gap-5 uppercase px-5 py-3 shadow-lg rounded leading-normal " +
                    (openRadio === 1
                      ? "text-blue-500 bg-white"
                      : "text-white bg-blue-500")
                  }
                  onClick={(e) => {
                    e.preventDefault();
                    setOpenRadio(1);
                  }}
                  disabled={openRadio === 1 ? true : false}
                >
                  Sense Making Statistics
                  {openRadio === 1 && <MdCheckCircle size={15} />}
                </Button>
              </li>
              <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
                <Button
                  size="sm"
                  type="tertiary"
                  className={
                    "w-full flex gap-5 uppercase px-5 py-3 shadow-lg rounded leading-normal " +
                    (openRadio === 2
                      ? "text-blue-500 bg-white"
                      : "text-white bg-blue-500")
                  }
                  onClick={(e) => {
                    e.preventDefault();
                    setOpenRadio(2);
                  }}
                  disabled={openRadio === 2 ? true : false}
                >
                  Group Statistics
                  {openRadio === 2 && <MdCheckCircle size={15} />}
                </Button>
              </li>
              <li className="-mb-px mr-2 last:mr-0 flex-auto text-center">
                <Button
                  size="sm"
                  type="tertiary"
                  className={
                    "w-full flex gap-5 uppercase px-5 py-3 shadow-lg rounded leading-normal " +
                    (openRadio === 3
                      ? "text-blue-500 bg-white"
                      : "text-white bg-blue-500")
                  }
                  onClick={(e) => {
                    e.preventDefault();
                    setOpenRadio(3);
                  }}
                  disabled={openRadio === 3 ? true : false}
                >
                  Tag Cloud Statistics
                  {openRadio === 3 && <MdCheckCircle size={15} />}
                </Button>
              </li>
            </ul>

            <div className="tab-content tab-space col-span-5">
              <div className={openRadio === 1 ? "block" : "hidden"} id="link1">
                <div className="items-center justify-center text-center place-items-center">
                  <div className="col-md-12">
                    <div className="font-bold text-3xl">
                      Sense Making Statistics
                    </div>
                  </div>
                </div>
                <div className="items-center justify-center text-center place-items-center">
                  {groupKnowledge.length !== 0 && (
                    <Graph
                      data={groupKnowledge}
                      layout={"circular"}
                      title={"Sense Making Statistics"}
                    />
                  )}
                </div>
              </div>
              <div className={openRadio === 2 ? "block" : "hidden"} id="link1">
                <div className="items-center justify-center text-center place-items-center">
                  <div className="col-md-12">
                    <div className="font-bold text-3xl">Group Statistics</div>
                  </div>
                </div>
                <div className="grid lg:grid-cols-4 sm:col-span-1 md:col-span-1">
                  <div className="col-span-2 items-center justify-center text-center place-items-center">
                    <Line stats={mediaStatistics} label="Group medias" />
                  </div>
                  <div className="col-span-2 items-center justify-center text-center place-items-center">
                    <Line stats={annoStatistics} label="Group annotations" />
                  </div>
                </div>
              </div>
              <div className={openRadio === 3 ? "block" : "hidden"} id="link2">
                <div className="items-center justify-center text-center place-items-center">
                  <div className="col-md-12">
                    <div className="font-bold text-3xl">
                      Tag Cloud Statistics
                    </div>
                  </div>
                </div>
                <div className="col-span-4 items-center justify-center text-center place-items-center">
                  {groupKnowledge.length !== 0 && (
                    <Graph
                      data={groupKnowledge}
                      layout={"none"}
                      title={"Tag Cloud Statistics"}
                    />
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  };

  if (error) {
    return <Loader error={error} />;
  }

  if (loading) {
    return <Loader />;
  }

  return (
    <div className="container relative w-full p-2">
      {contextHolder}

      <div className="w-full">
        <GroupInfo />
        {relations ? <Stats /> : <Tabs />}
      </div>
    </div>
  );
};

export default Collection;
