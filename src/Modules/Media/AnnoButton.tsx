import React, { useState } from "react";
import { Button, DialogModal } from "../../Components";
import { secToTimestamp } from "./Annotation";
import AnnotationForm from "./AnnotationForm";
import videojs from "video.js";
import { tokenState } from "../AuthState/AuthState";
import { useRecoilValue } from "recoil";

const AnnoButton = ({ updateAnnotate, startTime, endTime }) => {
  const [start, setStart] = useState<any>(-1);
  const [end, setEnd] = useState<any>(-1);
  const auth = localStorage.getItem("token");
  const [annoModal, setAnnoModal] = useState(false);

  const handle = (e?: any) => {
    if (videojs.getPlayer("videojs")) {
      const player = videojs.getPlayer("videojs");

      switch (e) {
        case "start":
          startTime("12:00:00")
          setStart(player.currentTime());
          localStorage.setItem("startTime", "12:00:00");
          player.play();
          break;
        case "end":
          endTime("19:00:00")
          setEnd(player.currentTime());
          localStorage.setItem("endTime", player.currentTime().toString());

          if (start === -1) {
            setStart(0);
            startTime("00:00:00");
            localStorage.setItem("startTime", "0");
          }
          player.pause();
          break;
        case "refresh":
          if (player) {
            player.play();
            player.currentTime(end);
          }
          break;
      }
    }
  };

  return (
    <>
      {auth && (
        <div className="text-sm grid lg:grid-cols-3 sm:grid-cols-1 gap-10 pt-[1rem] items-center justify-center">
          <div
            id="start"
            className="text-white w-full bg-blue-600 hover:bg-blue-700 active:bg-blue-800 font-bold rounded shadow hover:shadow-lg py-3 px-10 place-items-center justify-center outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 disabled:opacity-50 cursor-pointer items-center text-center"
            onClick={() => {
              handle("start");
            }}
          >
            {start === -1 ? "Start Time" : secToTimestamp(start)}
          </div>
          <div
            id="end"
            className="text-white w-full bg-blue-600 hover:bg-blue-700 active:bg-blue-800 font-bold rounded shadow hover:shadow-lg py-3 px-10 place-items-center justify-center outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 disabled:opacity-50 cursor-pointer items-center text-center"
            onClick={() => {
              handle("end");
            }}
          >
            {end === -1 ? "End Time" : secToTimestamp(end)}
          </div>
          <Button
            id="annotate"
            disabled={end === -1 || start > end}
            size="md"
            type="secondary"
            onClick={() => setAnnoModal(true)}
            className="disabled:opacity-25 w-full py-3"
          >
            Annotate
          </Button>

          {annoModal && (
            <DialogModal
              open={annoModal}
              onClose={() => setAnnoModal(false)}
              title={"Annotate here"}
            >
              <AnnotationForm
                setUpdateAnnotate={updateAnnotate}
                onClose={() => setAnnoModal(false)}
              />
            </DialogModal>
          )}
        </div>
      )}
    </>
  );
};

export default AnnoButton;
