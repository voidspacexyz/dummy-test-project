import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import { BiExport, BiImport } from "react-icons/bi";
import { MdClose, MdOutlineArrowBack } from "react-icons/md";
import { useNavigate, useParams } from "react-router-dom";
import { useRecoilState, useRecoilValue } from "recoil";
import {
  Button,
  DialogModal,
  IconButton,
  ImportModal,
  Loader,
  Player,
} from "../../Components";
import {
  prodState,
  startTimer,
  endTimer,
  tokenState,
  cardTimeState,
  editAnnoIDState,
} from "../AuthState/AuthState";
import { UploadOutlined, PlusOutlined } from "@ant-design/icons";
import Annotation from "./Annotation";
import MediaHeader from "./MediaHeader";
import { SettingOutlined } from "@ant-design/icons";
import { Collapse, Select } from "antd";
import TimeAgo from "react-timeago";
import { Form, Input, Modal, TimePicker, Checkbox, notification } from "antd";
import AntButton from "antd/es/button";
import { SearchOutlined } from "@ant-design/icons";
import Tags from "./Tags";
import Marquee from "react-fast-marquee";
import dayjs from "dayjs";
import customParseFormat from "dayjs/plugin/customParseFormat";
import { secToTimestamp } from "./Annotation";
import AnnotationForm from "./AnnotationForm";
import videojs from "video.js";
import { InboxOutlined } from "@ant-design/icons";
import type { RcFile, UploadFile, UploadProps } from "antd/es/upload/interface";
import { message, Upload } from "antd";
import TextEditor from "./TextEditor";

const { Dragger } = Upload;
const { Panel } = Collapse;
const { Option } = Select;
dayjs.extend(customParseFormat);
type NotificationType = "success" | "info" | "warning" | "error";

const timestampToSec = (timestamp: string): number => {
  const [hours, minutes, seconds] = timestamp.split(":").map(Number);
  const totalSeconds = hours * 3600 + minutes * 60 + seconds;
  return totalSeconds;
};

const Media = ({ updateMedia }) => {
  const [upload, setUpload] = useState<any>([]);
  const [grpId, setGrpId] = useState("");
  const [error, setError] = useState<any>([]);
  const [importModal, setImportModal] = useState(false);
  const [loading, setLoading] = useState(true);
  const [media, setMedia] = useState<any>([]);
  const [search, setSearch] = useState(false);
  const [start, setStart] = useState<any>(-1);
  const [end, setEnd] = useState<any>(-1);
  const { id } = useParams();
  const [uuid, setUuid] = useState("");
  const navigate = useNavigate();
  const auth = localStorage.getItem("token");
  const baseURL = localStorage.getItem("prod_url");
  const [durationEdit, setDurationEdit] = useState(false);
  const [annotations, setAnnotations] = useState<any>([]);
  const [startTime, setStartTime] = useRecoilState<any>(startTimer);
  const [endTime, setEndTime] = useRecoilState<any>(endTimer);
  const [cardTime, setCardTime] = useRecoilState<any>(cardTimeState);
  const [tagList, setTagList] = useState([]);
  const [fileList, setFileList] = useState<UploadFile[]>([]);
  const [uploading, setUploading] = useState(false);
  const [previewOpen, setPreviewOpen] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  const [previewTitle, setPreviewTitle] = useState("");
  const [annoModal, setAnnoModal] = useState(false);
  const [groupData, setGroupData] = useState<any>([]);
  const userUUID = localStorage.getItem("userUUID");
  const editAnnotation = useRecoilValue(editAnnoIDState);
  const [tags, setTags] = useState<any>([]);
  const [editAnnoText, setEditAnnoText] = useState<string>("");
  const [editAnnoImg, setEditAnnoImg] = useState<any>([]);
  const [isEditAnno, setIsEditAnno] = useState(false);

  const text = `
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;

  const [uploaded, setUploaded] = useState(false);
  const [form] = Form.useForm<{
    text: string;
    tags: [];
    startTime: string;
    endTime: string;
    uploadImage: [];
  }>();
  const [api, contextHolder] = notification.useNotification();

  const openNotificationWithIcon = (type: NotificationType, msg) => {
    api[type]({
      message: msg,
      placement: "topRight",
    });
  };

  const onChange = (key: string | string[]) => {
    console.log(key);
  };

  const onDurationChange = () => {
    setDurationEdit(!durationEdit);
  };

  const genExtra = () => (
    <SettingOutlined
      onClick={(event) => {
        // If you don't want click extra trigger collapse, you can prevent this:
        event.stopPropagation();
      }}
    />
  );

  const onReset = () => {
    //
  };

  useEffect(() => {
    if (id !== undefined) {
      setUuid(id);
    }
    getUpload();
    getAnnotations();
  }, []);

  useEffect(() => {
    setFileList([editAnnoImg]);
    form.setFieldValue("uploadImage", [editAnnoImg]);
  }, [editAnnoImg]);

  useEffect(() => {
    if (cardTime !== "") {
      const seconds: number = timestampToSec(cardTime);
      handleVideoPlayer("cardTime", seconds);
    }
  }, [cardTime]);

  useEffect(() => {
    form.setFieldValue("startTime", dayjs(startTime, "HH:mm:ss"));
    form.setFieldValue("endTime", dayjs(endTime, "HH:mm:ss"));
  }, [startTimer, startTime, endTime]);

  useEffect(() => {
    if (editAnnotation !== "") {
      setTagList([]);
      setTags([]);
      getAnnotationByID(editAnnotation);
      setAnnoModal(true);
    }
  }, [editAnnotation]);

  useEffect(() => {
    let updatedTag = [];
    if (tags.length !== 0) {
      tags
        .filter((tag) => tag.name !== "")
        .map((tag) => {
          updatedTag.push(tag.name);
        });
      setTagList(updatedTag);
      form.setFieldValue("tags", updatedTag);
    }
  }, [tags]);

  // useEffect(()=>{
  //   form.setFieldValue("text",editAnnoText)
  // },[editAnnoText])

  // useEffect(() => {
  //   if (editAnnoImg !== "") {
  //     setPreviewImage(editAnnoImg);
  //     setPreviewTitle(editAnnoText);
  //   }
  // });

  const getUpload = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/archive/${id}/`,
    };

    axios
      .request(options)
      .then((response) => {
        setUpload(response.data.upload);
        setGrpId(response.data.group.id);
        setMedia(response.data);
        setGroupData(response.data.group);
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setLoading(false);
      });
  };

  const getAnnotations = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/archive/${id}/`,
    };

    axios
      .request(options)
      .then((response) => {
        setGrpId(response.data.group.id);
        setAnnotations(response.data.annotations);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const getAnnotationByID = (id) => {
    setIsEditAnno(true);
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/annotate/${id}/`,
    };

    axios
      .request(options)
      .then((response) => {
        setAnnotations(response?.data);
        setStartTime(
          secToTimestamp(
            response?.data.media_target.split("=")[1].split(",")[0]
          )
        );
        setEndTime(
          secToTimestamp(
            response?.data.media_target.split("=")[1].split(",")[1]
          )
        );
        setTags(response.data.tags);
        form.setFieldValue("text", response?.data.annotation_text);
        setEditAnnoText(response?.data.annotation_text);
        const updateFileList = {
          uid: uuid,
          name: "image.png",
          status: "done",
          url: response?.data.annotation_image,
        };
        setEditAnnoImg(updateFileList);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleExport = (id, exporting) => {
    const options = {
      method: "POST",
      url: `${baseURL}api/v1/export/`,
      headers: {
        Authorization: `Token ${auth}`,
        "Content-Type": "application/json",
      },
      data: { type: exporting, id: id },
    };

    axios
      .request(options)
      .then((response) => {
        navigate("/requests");
      })
      .catch((error) => {
        console.error(error);
        setError(error.response.data);
      });
  };

  const updateAnnotate = () => {
    getAnnotations();
  };

  const onFinish = (values: any) => {
    let newList = values.uploadImage.reverse();
    console.log("Success:", values);
    const payload = new FormData();
    !isEditAnno && payload.append("media_reference_id", uuid);
    payload.append("annotation_text", values.text);
    payload.append(
      "media_target",
      `t=${
        values.startTime["$H"] * 60 * 60 +
        values.startTime["$m"] * 60 +
        values.startTime["$s"]
      },${
        values.endTime["$H"] * 60 * 60 +
        values.endTime["$m"] * 60 +
        values.endTime["$s"]
      }`
    );
    payload.append("tags", tagList.join(","));
    payload.append("annotation_image", newList[0]);

    const options = {
      method: `${isEditAnno ? "PUT" : "POST"}`,
      url: `${baseURL}api/v1/annotate/${isEditAnno && editAnnotation}/`,
      headers: {
        "Content-Type": "multipart/form-data",
        Authorization: `Token ${auth}`,
      },
      data: payload,
    };

    axios
      .request(options)
      .then((data) => {
        updateMedia(data);
        openNotificationWithIcon("success", "Successfully Uploaded Annotation");
        localStorage.removeItem("startTime");
        localStorage.removeItem("endTime");
        setAnnoModal(false);
        setStart(-1);
        setEnd(-1);
        videojs.getPlayer("videojs").currentTime(0);
        form.resetFields();
        setFileList([]);
        setTagList([]);
      })
      .catch((error) => {
        // updateMedia(payload);
        openNotificationWithIcon("error", "Failed to upload Annotation");
        setAnnoModal(false);
        console.error(error);
        // setLoading(false);
      });
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };
  const updateTagList = (inputValue: any) => {
    setTagList(inputValue);
  };

  const handleVideoPlayer = (e?: any, specificTime?: number) => {
    if (videojs.getPlayer("videojs")) {
      const player = videojs.getPlayer("videojs");
      switch (e) {
        case "start":
          setStart(player.currentTime());
          localStorage.setItem("startTime", player.currentTime().toString());
          form.setFieldValue(
            "startTime",
            dayjs(secToTimestamp(player.currentTime().toString()), "HH:mm:ss")
          );
          player.play();
          break;
        case "end":
          setEnd(player.currentTime());
          form.setFieldValue(
            "endTime",
            dayjs(secToTimestamp(player.currentTime().toString()), "HH:mm:ss")
          );
          localStorage.setItem("endTime", player.currentTime().toString());

          if (start === -1) {
            setStart(0);
            form.setFieldValue("startTime", dayjs("00:00:00", "HH:mm:ss"));
            localStorage.setItem("startTime", "0");
          }
          player.pause();
          break;
        case "refresh":
          if (player) {
            player.play();
            player.currentTime(end);
          }
          break;
        case "cardTime":
          if (player) {
            player.currentTime(specificTime);
            player.play();
          }
      }
    }
  };

  const AnnoButton = ({ updateAnnotate }) => {
    return (
      <>
        {auth && (
          <div className="flex sm:gap-10 pt-[1rem] items-center justify-center">
            <div
              id="start"
              className="text-white sm:w-[10rem] w-full p-1 bg-[#55acee] hover:bg-[#108ee9] font-bold rounded shadow hover:shadow-lg py-[0.4rem] place-items-center justify-center outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 disabled:opacity-50 cursor-pointer items-center text-center"
              onClick={() => {
                handleVideoPlayer("start");
              }}
            >
              {start === -1 ? "Start Time" : secToTimestamp(start)}
            </div>
            <div
              id="end"
              className="text-white sm:w-[10rem] w-full p-1 bg-[#55acee] hover:bg-[#108ee9] font-bold rounded shadow hover:shadow-lg py-[0.4rem] place-items-center justify-center outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150 disabled:opacity-50 cursor-pointer items-center text-center"
              onClick={() => {
                handleVideoPlayer("end");
              }}
            >
              {end === -1 ? "End Time" : secToTimestamp(end)}
            </div>
            <Button
              id="annotate"
              disabled={end === -1 || start > end}
              size="sm"
              type="secondary"
              onClick={() => setAnnoModal(true)}
              className="disabled:opacity-25 w-[10rem] py-2"
            >
              Annotate
            </Button>
          </div>
        )}
      </>
    );
  };

  const handleUpload = (file) => {
    setUpload(file[0]);
    form.setFieldValue("uploadImage", file[0]);
  };

  const getBase64 = (file: RcFile): Promise<string> =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result as string);
      reader.onerror = (error) => reject(error);
    });

  const props: UploadProps = {
    onRemove: (file) => {
      const index = fileList.indexOf(file);
      const newFileList = fileList.slice();
      newFileList.splice(index, 1);
      form.setFieldValue("", newFileList);
      setFileList(newFileList);
      form.setFieldValue("uploadImage", newFileList);
    },
    beforeUpload: (file) => {
      setFileList([...fileList, file]);
      form.setFieldValue("uploadImage", [...fileList, file]);
      return false;
    },
    fileList,
  };

  const handleCancel = () => setPreviewOpen(false);
  const handlePreview = async (file) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj);
    }
    setPreviewImage(file.url || file.preview);
    setPreviewOpen(true);
    setPreviewTitle(
      file.name || file.url.substring(file.url.lastIndexOf("/") + 1)
    );
  };
  const handleChange = ({ fileList: newFileList }) => {
    setFileList(newFileList);
  };
  const updateTextArea = (value) => {
    if (value === "<p><br></p>") {
      form.setFieldValue("text", null);
    } else {
      form.setFieldValue("text", value);
    }
  };

  if (loading) {
    return <Loader />;
  }

  return (
    <>
      {contextHolder}
      <div className="">
        <div className="text-md text-center py-4 px-2 border-b-[#d9d9d9] border-solid border-b-[1px]">
          <h3 className="pl-4">Annotate the Media</h3>
        </div>
        <Form
          form={form}
          initialValues={{
            startTime: dayjs(startTime, "HH:mm:ss"),
            endTime: dayjs(endTime, "HH:mm:ss"),
            tags: tagList,
            uploadImage: fileList,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
          layout="vertical"
        >
          <div className="p-2 overflow-hidden hover:overflow-y-scroll items-center max-h-[47.5rem]">
            <div
              className={`sm:flex md:flex md:gap-2 sm:gap-2  ${
                annoModal ? "place-content-start" : "place-content-center"
              }`}
            >
              <Player upload={upload} />
              {annoModal && (
                <div className="max-w-[22rem]">
                  <div className="flex gap-8 sm:mt-0 mt-2">
                    <Form.Item
                      name="startTime"
                      label="Start time"
                      rules={[
                        {
                          required: true,
                          message: "Please select start time.",
                        },
                      ]}
                    >
                      <TimePicker
                        defaultValue={dayjs(
                          startTime ? startTime : "00:00:00",
                          "HH:mm:ss"
                        )}
                        format={"HH:mm:ss"}
                        showNow={true}
                        popupClassName="bg-none"
                        disabled={durationEdit}
                      />
                    </Form.Item>
                    <Form.Item
                      name="endTime"
                      label="End time"
                      rules={[
                        { required: true, message: "Please select send time." },
                      ]}
                    >
                      <TimePicker
                        defaultValue={dayjs(endTime, "HH:mm:ss")}
                        showNow={false}
                        value={endTime}
                        popupClassName="bg-none"
                        disabled={durationEdit}
                      />
                    </Form.Item>
                  </div>
                  <Form.Item name="tags" label={`Tags`}>
                    <Tags updateTags={updateTagList} tagList={tagList} />
                  </Form.Item>
                  {/* <Checkbox checked={durationEdit === true ? false : true } onChange={onDurationChange} className="w-[6rem]">Edit Time</Checkbox> */}
                </div>
              )}
            </div>

            {auth &&
              !annoModal &&
              groupData.users?.map((user, i) => {
                return (
                  user.id.includes(userUUID) && (
                    <AnnoButton updateAnnotate={updateAnnotate} />
                  )
                );
              })}
          </div>
          {annoModal && (
            <>
              {/* <hr className="border border-t-[1px] border-solid border-slate-200 my-2"/> */}

              <div className="p-2">
                <div className="">
                  <Form.Item
                    name="text"
                    label="Annotate Text"
                    rules={[
                      {
                        required: true,
                        message: "Please input your text content.",
                      },
                    ]}
                  >
                    {/* <Input.TextArea rows={10} /> */}
                    <TextEditor
                      updateTextFeild={updateTextArea}
                      editValue={editAnnoText ? editAnnoText : ""}
                    />
                  </Form.Item>
                  <div className="sm:flex justify-between">
                    <Form.Item name="uploadImage" label="Upload Image" noStyle>
                      <div className="h-fit">
                        <Upload.Dragger
                          multiple={false}
                          maxCount={1}
                          name="files"
                          {...props}
                          listType="picture"
                          fileList={fileList}
                          defaultFileList={fileList}
                          onPreview={handlePreview}
                          onChange={handleChange}
                        >
                          <div className="flex gap-2 px-1 sm:w-[30rem]">
                            <p className="ant-upload-drag-icon">
                              <UploadOutlined />
                            </p>
                            <p className="ant-upload-text self-center">
                              Click or drag image to upload (Only one image
                              allowed)
                            </p>
                          </div>
                        </Upload.Dragger>
                      </div>
                      <Modal
                        open={previewOpen}
                        title={previewTitle}
                        footer={null}
                        onCancel={handleCancel}
                      >
                        <img
                          alt=""
                          style={{ width: "100%" }}
                          src={previewImage}
                        />
                      </Modal>
                    </Form.Item>

                    <div className="self-end mt-6 sm:mt-0">
                      <Form.Item>
                        <div className="flex gap-2">
                          <AntButton
                            className="bg-[#108ee9]"
                            type="primary"
                            size="middle"
                            htmlType="submit"
                          >
                            Create
                          </AntButton>
                          <AntButton
                            type="text"
                            size="middle"
                            onClick={() => {
                              setAnnoModal(false);
                              setStart(-1);
                              setEnd(-1);
                              videojs.getPlayer("videojs").currentTime(0);
                            }}
                          >
                            Cancel
                          </AntButton>
                        </div>
                      </Form.Item>
                      <div></div>
                    </div>
                  </div>
                </div>
              </div>
            </>
          )}
        </Form>
      </div>
    </>
  );
};

export default Media;
