import React, { useState, useEffect, useCallback } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { MdClose, MdOutlineShare, MdOutlineDelete } from "react-icons/md";
import copy from "copy-to-clipboard";
import axios from "axios";
import {
  Button,
  DeleteModal,
  IconButton,
  DialogModal,
  AnnotationCard,
} from "../../Components";
import { BiExport } from "react-icons/bi";
import videojs from "video.js";
import { useRecoilValue } from "recoil";
import { prodState, tokenState } from "../AuthState/AuthState";

export const secToTimestamp = (sec) => {
  const date = new Date(sec * 1000);
  var hh = date.getUTCHours();
  var mm = date.getUTCMinutes();
  var ss = date.getUTCSeconds();

  if (hh < 10) {
    hh = 0 + hh;
  }
  if (mm < 10) {
    mm = 0 + mm;
  }
  if (ss < 10) {
    ss = 0 + ss;
  }
  var time = hh + ":" + mm + ":" + ss;

  return time;
};

const Annotation = ({ data, updateAnnotate, isSearch, mediaName }) => {
  const [recordings, setRecordings] = useState<any>([]);
  const [annotations, setAnnotations] = useState<any>([]);
  const [updateTime, setUpdateTime] = useState<number>();
  const [curMedia, setCurMedia] = useState<string>("");
  const [searchMedia, setSearchMedia] = useState<any[]>([]);
  const [display, setDisplay] = useState(true);
  const [grpId, setGrpId] = useState("");
  const [showModal, setShowModal] = useState(false);
  const [deleteModal, setDeleteModal] = useState(false);
  const [error, setError] = useState<any>([]);
  const [annoId, setAnnoId] = useState(null);
  const [exporting, setExporting] = useState("");
  const [annoModal, setAnnoModal] = useState(false);

  const { id } = useParams();
  const auth = localStorage.getItem("token");
  const navigate = useNavigate();
  const baseURL = localStorage.getItem("prod_url");

  useEffect(() => {
    searchAnno();
  }, [curMedia]);

  const searchAnno = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/annotate/?name=${curMedia}`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
    };

    axios
      .request(options)
      .then((response) => {
        setSearchMedia(response.data.results);
      })
      .catch((e) => {
        console.log(e);
      });
  };

  const handleAnnotation = (annoId) => {
    const options = {
      method: "DELETE",
      url: `${baseURL}api/v1/annotate/id/${annoId}/`,
      headers: {
        Authorization: `Token ${auth}`,
      },
    };

    axios
      .request(options)
      .then((response) => {
        console.log(response);
        updateAnnotate();
      })
      .catch((e) => {
        console.log(e);
        setError(e.response.data.detail);
      });
  };

  const handleExport = (id, exporting) => {
    const options = {
      method: "POST",
      url: `${baseURL}api/v1/export/`,
      headers: {
        Authorization: `Token ${auth}`,
        "Content-Type": "application/json",
      },
      data: { type: exporting, id: id },
    };

    axios
      .request(options)
      .then((response) => {
        console.log(response.data);
        navigate("/requests");
      })
      .catch((error) => {
        console.error(error);
        setError(error.response.data);
      });
  };

  const handleCopyText = (value) => {
    copy(value);
    alert(`Copied to clipboard : "${value}"`);
  };

  const shareModal = useCallback(
    (val) => {
      setShowModal(true);
    },
    [showModal]
  );

  const deleteAnnoModal = useCallback(
    (val) => {
      setAnnoId(val);
      console.log(val);
      setDeleteModal(true);
    },
    [deleteModal]
  );

  const exportAnnotation = useCallback(
    (val) => {
      setAnnoId(val);
      setExporting("annotation");
      handleExport(val, "annotation");
    },
    [exporting]
  );

  const SearchAnno = () => {
    return (
      <>
        <div>
          <ul className="w-full flex flex-col gap-5">
            {searchMedia &&
              searchMedia.map((anno, i) => (
                <AnnotationCard
                  annotationId={anno.uuid}
                  annoText={anno.annotation_text}
                  tags={anno.tags}
                  startTime={secToTimestamp(
                    anno.media_target.split("=").join().split(",")[1]
                  )}
                  endTime={secToTimestamp(
                    anno.media_target.split("=").join().split(",")[2]
                  )}
                  annoImage={anno.annotation_image}
                  createdDate={anno.created_at}
                  createdBy={anno.created_by}
                  updatedDate={anno.updated_at}
                  shareModal={shareModal}
                  deleteModal={deleteAnnoModal}
                  exportAnno={exportAnnotation}
                  mediaTitle={mediaName}
                />
              ))}
          </ul>
        </div>
      </>
    );
  };

  const ClickToCopy = () => {
    return (
      <div className="grid grid-cols-4 w-full h-5/6">
        <div className="col-span-3 items-start gap-2">
          <p className="p-2 font-bold mb-2 text-gray-700 text-lg text-clip overflow-hidden">
            {window.location.href}
          </p>
        </div>
        <div className="col-span-1 items-center justify-center">
          <Button
            className="overflow-hidden col-span-1 w-fit bg-emerald-500 text-white active:bg-emerald-600 font-bold uppercase text-sm px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 ease-linear transition-all duration-150"
            type="secondary"
            size="sm"
            onClick={() => {
              handleCopyText(window.location.href);
              setShowModal(false);
            }}
          >
            Click to Copy
          </Button>
        </div>
      </div>
    );
  };

  const DisplayAnno = () => {
    return (
      <div>
        <ul className="w-full flex flex-col gap-5">
          {data.map((anno, key) => (
            <AnnotationCard
              annotationId={anno.uuid}
              annoText={anno.annotation_text}
              tags={anno.tags}
              startTime={secToTimestamp(
                anno.media_target.split("=").join().split(",")[1]
              )}
              endTime={secToTimestamp(
                anno.media_target.split("=").join().split(",")[2]
              )}
              annoImage={anno.annotation_image}
              createdDate={anno.created_at}
              createdBy={anno.created_by}
              updatedDate={anno.updated_at}
              shareModal={shareModal}
              deleteModal={deleteAnnoModal}
              exportAnno={exportAnnotation}
              mediaTitle={mediaName}
            />
          ))}
        </ul>
        {showModal && (
          <DialogModal
            open={showModal}
            onClose={() => setShowModal(false)}
            title={"Share your annotation"}
          >
            <ClickToCopy />
          </DialogModal>
        )}
        {deleteModal && (
          <DialogModal
            open={deleteModal}
            onClose={() => setDeleteModal(false)}
            title={`Are you sure you want to delete this annotation ?`}
          >
            <DeleteModal
              onClose={() => {
                setDeleteModal(false);
              }}
              onClick={() => {
                handleAnnotation(annoId);
                setDeleteModal(false);
              }}
            />
          </DialogModal>
        )}
      </div>
    );
  };

  const handleCurTime = (annoTime) => {
    if (videojs.getPlayer("videojs")) {
      const player = videojs.getPlayer("videojs");
      player.currentTime(annoTime);
      player.play();
    }
  };

  const handleSearch = ({
    target,
  }: React.ChangeEvent<HTMLInputElement>): void => {
    const searchWord: string = target.value;
    setCurMedia(searchWord);
    setDisplay(false);
  };

  return (
    <div className="">
      {data?.length !== 0 && (
        <div className={`col-start-2 col-end-6 w-full relative`}>
          {isSearch && (
            <div className="flex flex-col-2 items-center mb-3 gap-2">
              <div className="md:w-full w-full grid items-center shadow-lg  rounded-lg">
                <div className="flex ">
                  <input
                    type="text"
                    placeholder={`Search Annotation's here...`}
                    value={curMedia}
                    onChange={(e) => {
                      handleSearch(e);
                    }}
                    className="bg-white border-2 border-gray-200 rounded-lg w-full py-2 px-4 text-gray-700 focus:outline-none focus:bg-white focus:border-purple-500"
                  />
                </div>
              </div>
              <MdClose
                size={26}
                className="p-1 cursor-pointer -ml-10"
                onClick={() => {
                  setCurMedia("");
                  setDisplay(true);
                }}
              />
            </div>
          )}
          {display ? (
            <div className="w-full relative h-3/4 rounded-lg">
              <DisplayAnno />
            </div>
          ) : (
            <div className="w-full relative h-3/4 rounded-lg">
              <SearchAnno />
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default Annotation;
