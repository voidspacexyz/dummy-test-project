import React, { useState, useEffect } from "react";
import RichTextEditor from "react-rte";

const TextEditor = ({
  updateTextFeild,
  editValue,
}: {
  updateTextFeild;
  editValue: string;
}) => {
  const [value, setValue] = useState(RichTextEditor.createValueFromString(editValue, 'html'));

 
  const onChange = (value) => {
    setValue(value);
    updateTextFeild(value.toString("html"));
  };
  useEffect(()=>{
    setValue(RichTextEditor.createValueFromString(editValue, 'html'))
    console.log(editValue)
  },[editValue])

  return (
    <RichTextEditor
      autoFocus={true}
      // placeholder={"Please enter your annotation contents here...."}
      enableSoftNewLineByDefault={true}
      value={value}
      onChange={onChange}
    />
  );
};

export default TextEditor;
