import {
  Button,
  Collapse,
  Space,
  Tooltip,
  Badge,
  Card,
  Select,
  SelectProps,
} from "antd";
import React, { useState, useEffect, Fragment } from "react";
import Annotation from "./Annotation";
import Media from "./Media";
import { SearchOutlined } from "@ant-design/icons";
import TimeAgo from "react-timeago";
import { useNavigate, useParams } from "react-router-dom";
import axios from "axios";
import TagButton from "../../Components/Button/TagButton";
import Pragraphs from "../../Components/Paragraph/Pragraphs";
import { Float, Loader, Toggle } from "../../Components";
import { MdOutlineArrowBack } from "react-icons/md";

const AnnotationBlock = () => {
  const { Panel } = Collapse;
  const navigate = useNavigate();
  const { id } = useParams();
  const [annotations, setAnnotations] = useState<any>([]);
  const [media, setMedia] = useState<any>([]);
  const [upload, setUpload] = useState<any>([]);
  const [loading, setLoading] = useState(true);
  const [search, setSearch] = useState(false);
  const auth = localStorage.getItem("token");
  const baseURL = localStorage.getItem("prod_url");
  const [grp, setGrp] = useState<any>({});
  const [sortAnno, setSortAnno] = useState<string>("last");
  const annoSort = localStorage.getItem("annoSort");
  const userUUID = localStorage.getItem("userUUID");
  const [viewMyAnno, setViewMyAnno] = useState(false);
  const [collectionUsers, setCollectionUsers] = useState<any>([]);
  const [selectedItems, setSelectedItems] = useState<any[]>([]);
  const [USERS, setUSERS] = useState<any[]>([]);
  const [error, setError] = useState<string>("");

  useEffect(() => {
    getUpload();
    getAnnotations();
    if (sortAnno !== "") {
      getAnnotations();
    }
    getUsersInArray();
  }, []);

  useEffect(() => {
    collectionUsers.length !== 0 && getUsersInArray();
  }, [collectionUsers]);

  const getUsersInArray = () => {
    collectionUsers.map((user) => USERS.push(user.username));
  };

  const filteredOptions = USERS.filter(
    (user) => !selectedItems.includes(user.username)
  );

  // console.log(selectedItems);
  // console.log(annotations);

  useEffect(() => {
    localStorage.setItem("annoSort", sortAnno);
    getAnnotations();
  }, [sortAnno, viewMyAnno, selectedItems]);

  const getUpload = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/archive/${id}/`,
    };

    axios
      .request(options)
      .then((response) => {
        setUpload(response.data.upload);
        setGrp(response.data.group);
        setCollectionUsers(response.data.group.users);
        setMedia(response.data);
        setLoading(false);
      })
      .catch((e) => {
        console.log(e);
        setError(e?.message);
        setLoading(false);
      });
  };

  const getAnnotations = () => {
    const options = {
      method: "GET",
      url: `${baseURL}api/v1/archive/${id}/`,
    };

    axios
      .request(options)
      .then((response) => {
        switch (sortAnno) {
          case "last":
            setAnnotations(response.data.annotations.reverse());
            break;

          case "time":
            setAnnotations(
              response.data.annotations.sort((val1, val2) => {
                if (
                  parseInt(val1.media_target.split("=")[1].split(",")[0]) <
                  parseInt(val2.media_target.split("=")[1].split(",")[0])
                ) {
                  return -1;
                } else if (
                  parseInt(val1.media_target.split("=")[1].split(",")[0]) >
                  parseInt(val2.media_target.split("=")[1].split(",")[0])
                ) {
                  return 1;
                }
                return 0;
              })
            );
            break;

          default:
            setAnnotations(response.data.annotations.reverse());
            break;
        }

        viewMyAnno &&
          setAnnotations(
            response.data?.annotations.filter(
              (x) => x.created_by && x.created_by.id === userUUID
            )
          );

        selectedItems.length !== 0 &&
          console.log(
            response.data?.annotations.filter(
              (x) =>
                x.created_by && x.created_by.username === USERS.map((y) => y)
            )
          );

        // selectedItems.length !== 0 &&
        //   setAnnotations(
        //     USERS.some((y) =>
        //       response.data?.annotations.some(
        //         (x) => x.created_by && x.created_by.username === y
        //       )
        //     )
        //   );
      })
      .catch((e) => {
        console.log(e);
        setError(e?.message);
      });
  };

  const updateAnnotate = () => {
    getAnnotations();
  };

  if (error) {
    return <Loader error={error} />;
  }

  return (
    <div className="container relative p-5">
      <div className="mb-4">
        <Collapse defaultActiveKey={["0"]} expandIconPosition={"end"}>
          <Panel
            header={
              <>
                <Float
                  tooltip={`Back to Collection ${grp.name}`}
                  icon={<MdOutlineArrowBack />}
                  href={`/collection/${grp.id}`}
                />
                <div className="pl-6 ml-6 pb-1.5 mb-1.5 ">
                  <Tooltip title="Click to view more media data">
                    {media?.name && media?.name}
                  </Tooltip>
                </div>
              </>
            }
            key="1"
          >
            <div className="">
              {media?.description && (
                <Pragraphs isExpand={true} rows={6} text={media?.description} />
              )}
              {media?.tags && (
                <div className="flex w-full">
                  <div className="flex overflow-hidden hover:overflow-x-scroll h-12 ">
                    {media?.tags?.map(
                      (tag, i) =>
                        tag.name !== "" &&
                        tag.count !== 0 && (
                          <div
                            className="inline-flex p-1 cursor-pointer"
                            key={i}
                            onClick={() => {
                              navigate(`/search/tag=${tag.name}`);
                            }}
                          >
                            <TagButton color="#55acee" tagName={tag.name} />
                          </div>
                        )
                    )}
                  </div>
                </div>
              )}
              <hr className=" border border-t-[1px] border-solid border-slate-200 my-2" />
              <div className="flex justify-between">
                {media?.created_by?.first_name && (
                  <span className="">
                    <Tooltip title="Media Owner" placement="right">
                      Created By : {media?.created_by?.first_name}
                    </Tooltip>
                  </span>
                )}
                {media?.created_at && (
                  <div className="cursor-pointer">
                    <TagButton
                      color="#108ee9"
                      tagName={
                        <TimeAgo
                          date={media?.created_at?.split("T").reverse().pop()}
                        />
                      }
                    />
                  </div>
                )}
              </div>
            </div>
          </Panel>
        </Collapse>
      </div>
      <div className="grid grid-cols-1 sm:gap-2 sm:mb-2 sm:grid-cols-12">
        <div className="col-start-1 col-end-9">
          <div className="border-[#d9d9d9] border-solid border-[1px] bg-[#eff1f2] rounded-md">
            <Media updateMedia={updateAnnotate} />
          </div>
        </div>
        <div className="sm:col-start-9 sm:col-span-12 mt-2 sm:mt-0">
          <Collapse defaultActiveKey={["0"]} expandIconPosition={"end"}>
            <Panel
              header={
                <>
                  <div className="text-md flex justify-between items-center">
                    <h3>Annotations</h3>
                    <span>
                      <Space direction="vertical">
                        <Space wrap>
                          <Tooltip title="Search annotations">
                            <Button
                              type="dashed"
                              shape="circle"
                              icon={<SearchOutlined />}
                              onClick={() => {
                                setSearch(!search);
                              }}
                            />
                          </Tooltip>
                        </Space>
                      </Space>
                    </span>
                  </div>
                </>
              }
              key="1"
            >
              <Space direction="vertical">
                <Space wrap>
                  <Select
                    defaultValue={annoSort}
                    style={{ width: 200 }}
                    className="w-min"
                    onChange={(e) => {
                      setSortAnno(e);
                    }}
                    options={[
                      {
                        label: "Sort",
                        options: [
                          { label: "Last Modified", value: "last" },
                          { label: "Time Stamp", value: "time" },
                        ],
                      },
                    ]}
                  />
                  <Toggle
                    onClick={() => {
                      setViewMyAnno(!viewMyAnno);
                    }}
                    checked={viewMyAnno}
                    title={"My Annotations"}
                  />
                </Space>
              </Space>
            </Panel>
          </Collapse>
          <div className="p-2 overflow-hidden hover:overflow-y-scroll pb-56 h-[43rem]">
            <Annotation
              data={annotations}
              updateAnnotate={updateAnnotate}
              isSearch={search}
              mediaName={media?.name && media?.name}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default AnnotationBlock;
