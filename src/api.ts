import { useRecoilState } from 'recoil';
import axios from 'axios';
import { prodState } from './Modules/AuthState/AuthState';

let baseURL = '';

export const getBaseURL = () => {

    return axios.get(`${process.env.PUBLIC_URL}/config.json`)
        .then(response => {
            baseURL = response.data.API_URL;
        });
};

export const getData = (url: string) => {
    return axios.get(`${baseURL}/${url}`);
};