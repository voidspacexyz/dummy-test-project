import React, { useEffect, useState } from "react";
import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";
import "./tailwind.css";
import Nav from "./Components/Navbar/Nav";
import Dashboard from "./Modules/Dashboard/Dashboard";
import UploadForm from "./Modules/Collection/UploadForm";
import { Login, Register } from "./Modules/Auth";
import Collection from "./Modules/Collection/Collection";
import Home from "./Modules/About";
import Media from "./Modules/Media/Media";
import Requests from "./Modules/Dashboard/Requests";
import Search from "./Modules/Dashboard/Search";
import AuthStats from "./Modules/Auth/AuthStats";
import axios from "axios";
import { RecoilRoot, useRecoilState, useRecoilValue } from "recoil";
import { prodState, tokenState } from "./Modules/AuthState/AuthState";
import { Footer } from "./Components";
import AnnotationBlock from "./Modules/Media/AnnotationBlock";

const App = () => {
  const [prod, setProd] = useRecoilState(prodState);

  useEffect(() => {
    baseURL();
  }, []);

  const baseURL = () => {
    const options = {
      method: "GET",
      url: process.env.PUBLIC_URL + `/config.json`,
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };

    axios
      .request(options)
      .then((response) => {
        localStorage.setItem("prod_url", response.data.API_URL);
        setProd(response.data.API_URL);
        console.log(response.data.API_URL);
      })
      .catch((error) => {
        console.error(error);
      });
  };

  return (
    <>
      <RecoilRoot>
        {prod ? (
          <>
            <Nav />
            <main>
              <section className="bg-gray-100 h-full min-h-screen">
                <BrowserRouter>
                  <Routes>
                    <Route path="/" element={<Dashboard />} />
                    <Route path="/home" element={<Dashboard />} />
                    <Route path="/about" element={<Home />} />
                    <Route path="/collection/:id" element={<Collection />} />
                    <Route
                      path="collection/:id/media/:id"
                      element={<AnnotationBlock />}
                    />
                    <Route path="/media/upload/:id" element={<UploadForm />} />
                    <Route path="/media/edit/:id" element={<UploadForm />} />
                    <Route path="/requests" element={<Requests />} />
                    <Route path="/requests/:id" element={<Requests />} />
                    <Route path="/search" element={<Search />} />
                    <Route path="/search/:val" element={<Search />} />

                    <Route path="/auth/">
                      <Route path="/auth/login" element={<Login />} />
                      <Route path="/auth/register" element={<Register />} />
                      <Route path="/auth/stats" element={<AuthStats />} />
                      <Route
                        path="/auth/*"
                        element={<Navigate replace to="/auth/login" />}
                      />
                    </Route>

                    <Route path="*" element={<Home />} />
                  </Routes>
                </BrowserRouter>
              </section>
            </main>
            <Footer />
          </>
        ) : null}
      </RecoilRoot>
    </>
  );
};

export default App;
