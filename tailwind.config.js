module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      display: ["group-hover"],
      lineClamp: {
        7: '7',
        8: '8',
        9: '9',
        10: '10',
      }
    },
    container: {
      center: true,
    },
  },
  variants: {
    opacity: ({ after }) => after(['disabled']),
    lineClamp: ['responsive', 'hover']

  },
  plugins: [
    require('@tailwindcss/line-clamp'),
  ],
}